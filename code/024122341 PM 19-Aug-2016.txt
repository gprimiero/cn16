model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	383
total timesteps	52
turtle that knows p	90
turtle that knows not p	111
turtle that discovers p (id,ranking,skeptic?)	56	1	true
turtle that discovers not p (id,ranking,skeptic?)	99	1	true
