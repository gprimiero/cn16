model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	101
total timesteps	19
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	21	1	false
turtle that discovers not p (id,ranking,skeptic?)	35	0.5	true
