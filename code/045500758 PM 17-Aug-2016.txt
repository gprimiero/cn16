model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1103
total timesteps	186
turtle that knows p	90
turtle that knows not p	210
turtle that discovers p (id,ranking,skeptic?)	247	84	true
turtle that discovers not p (id,ranking,skeptic?)	243	113	false
