model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	531
total timesteps	77
turtle that knows p	168
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	102	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.125	true
