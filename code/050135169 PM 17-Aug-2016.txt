model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	1244
total timesteps	10
turtle that knows p	125
turtle that knows not p	175
turtle that discovers p (id,ranking,skeptic?)	275	0	false
turtle that discovers not p (id,ranking,skeptic?)	192	0	false
