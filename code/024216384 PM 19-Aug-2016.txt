model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	275
total timesteps	61
turtle that knows p	47
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	50	1	true
turtle that discovers not p (id,ranking,skeptic?)	88	1	true
model parameters
nodes	100
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2071
total timesteps	118
turtle that knows p	153
turtle that knows not p	148
turtle that discovers p (id,ranking,skeptic?)	211	1	true
turtle that discovers not p (id,ranking,skeptic?)	173	1	true
