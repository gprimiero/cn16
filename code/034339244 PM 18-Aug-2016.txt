model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	745
total timesteps	6
turtle that knows p	217
turtle that knows not p	228
turtle that discovers p (id,ranking,skeptic?)	160	0	false
turtle that discovers not p (id,ranking,skeptic?)	253	0	true
