model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	334
total timesteps	48
turtle that knows p	62
turtle that knows not p	239
turtle that discovers p (id,ranking,skeptic?)	169	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	95	1	false
