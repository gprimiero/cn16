model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	538
total timesteps	51
turtle that knows p	172
turtle that knows not p	29
turtle that discovers p (id,ranking,skeptic?)	9	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	39	0.3333333333333333	true
