model parameters
nodes	300
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	862
total timesteps	157
turtle that knows p	224
turtle that knows not p	77
turtle that discovers p (id,ranking,skeptic?)	130	1	true
turtle that discovers not p (id,ranking,skeptic?)	199	1	false
