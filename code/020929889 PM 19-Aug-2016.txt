model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	916
total timesteps	71
turtle that knows p	276
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	159	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	277	1	true
