model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	42
total timesteps	22
turtle that knows p	41
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	8	1	true
turtle that discovers not p (id,ranking,skeptic?)	6	0.25	true
model parameters
nodes	40
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	95
total timesteps	15
turtle that knows p	24
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	22	1	true
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
