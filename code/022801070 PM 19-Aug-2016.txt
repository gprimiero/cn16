model parameters
nodes	50
skeptics	5
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	64
total timesteps	22
turtle that knows p	45
turtle that knows not p	6
turtle that discovers p (id,ranking,skeptic?)	32	1	false
turtle that discovers not p (id,ranking,skeptic?)	3	1	false
