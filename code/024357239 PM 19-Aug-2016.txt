model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	178
total timesteps	35
turtle that knows p	14
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	10	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	28	1	true
