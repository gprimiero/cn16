model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	927
total timesteps	82
turtle that knows p	215
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	118	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	133	0.3333333333333333	true
