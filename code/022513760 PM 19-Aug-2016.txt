model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1377
total timesteps	92
turtle that knows p	189
turtle that knows not p	112
turtle that discovers p (id,ranking,skeptic?)	55	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	253	1	false
model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	702
total timesteps	68
turtle that knows p	170
turtle that knows not p	131
turtle that discovers p (id,ranking,skeptic?)	299	1	false
turtle that discovers not p (id,ranking,skeptic?)	122	1	true
