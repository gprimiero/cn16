model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1710
total timesteps	92
turtle that knows p	23
turtle that knows not p	278
turtle that discovers p (id,ranking,skeptic?)	265	1	false
turtle that discovers not p (id,ranking,skeptic?)	37	1	true
