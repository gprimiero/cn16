model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	555
total timesteps	74
turtle that knows p	102
turtle that knows not p	199
turtle that discovers p (id,ranking,skeptic?)	249	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	67	1	true
