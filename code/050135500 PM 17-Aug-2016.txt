model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	194
total timesteps	33
turtle that knows p	35
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	8	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	13	1	true
