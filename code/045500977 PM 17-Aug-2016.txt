model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	140
total timesteps	94
turtle that knows p	178
turtle that knows not p	122
turtle that discovers p (id,ranking,skeptic?)	196	215	false
turtle that discovers not p (id,ranking,skeptic?)	239	28	true
