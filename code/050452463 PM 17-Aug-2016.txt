model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	47
total timesteps	16
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	45	1	false
turtle that discovers not p (id,ranking,skeptic?)	41	1	true
