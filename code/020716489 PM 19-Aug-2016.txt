model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	722
total timesteps	97
turtle that knows p	204
turtle that knows not p	97
turtle that discovers p (id,ranking,skeptic?)	27	1	false
turtle that discovers not p (id,ranking,skeptic?)	133	0.5	false
