model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	270
total timesteps	29
turtle that knows p	192
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	73	1	true
turtle that discovers not p (id,ranking,skeptic?)	16	0.2	true
