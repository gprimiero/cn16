model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	661
total timesteps	76
turtle that knows p	66
turtle that knows not p	235
turtle that discovers p (id,ranking,skeptic?)	65	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	17	0.25	false
