model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	251
total timesteps	39
turtle that knows p	15
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	99	1	true
turtle that discovers not p (id,ranking,skeptic?)	38	0.3333333333333333	true
