model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	139
total timesteps	19
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	23	1	true
turtle that discovers not p (id,ranking,skeptic?)	39	1	true
