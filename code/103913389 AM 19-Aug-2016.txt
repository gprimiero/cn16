model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	310
total timesteps	60
turtle that knows p	251
turtle that knows not p	50
turtle that discovers p (id,ranking,skeptic?)	159	1	true
turtle that discovers not p (id,ranking,skeptic?)	139	0.25	true
