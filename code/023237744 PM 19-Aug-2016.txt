model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	655
total timesteps	70
turtle that knows p	191
turtle that knows not p	110
turtle that discovers p (id,ranking,skeptic?)	242	1	true
turtle that discovers not p (id,ranking,skeptic?)	211	1	true
