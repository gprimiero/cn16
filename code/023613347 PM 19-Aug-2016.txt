model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	98
total timesteps	25
turtle that knows p	51
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	19	1	false
turtle that discovers not p (id,ranking,skeptic?)	33	0.3333333333333333	false
