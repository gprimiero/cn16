model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	330
total timesteps	63
turtle that knows p	200
turtle that knows not p	101
turtle that discovers p (id,ranking,skeptic?)	254	1	false
turtle that discovers not p (id,ranking,skeptic?)	228	1	false
