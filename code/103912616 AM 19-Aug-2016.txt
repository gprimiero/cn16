model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	370
total timesteps	76
turtle that knows p	230
turtle that knows not p	71
turtle that discovers p (id,ranking,skeptic?)	211	1	true
turtle that discovers not p (id,ranking,skeptic?)	137	0.5	true
