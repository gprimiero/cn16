model parameters
nodes	300
skeptics	144
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	527
total timesteps	74
turtle that knows p	236
turtle that knows not p	65
turtle that discovers p (id,ranking,skeptic?)	126	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	173	1	true
