model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	422
total timesteps	67
turtle that knows p	40
turtle that knows not p	261
turtle that discovers p (id,ranking,skeptic?)	88	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	83	1	false
