model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	415
total timesteps	60
turtle that knows p	37
turtle that knows not p	264
turtle that discovers p (id,ranking,skeptic?)	84	1	false
turtle that discovers not p (id,ranking,skeptic?)	60	0.3333333333333333	false
