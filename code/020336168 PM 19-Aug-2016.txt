model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	160
total timesteps	42
turtle that knows p	121
turtle that knows not p	180
turtle that discovers p (id,ranking,skeptic?)	175	1	false
turtle that discovers not p (id,ranking,skeptic?)	130	0.5	false
