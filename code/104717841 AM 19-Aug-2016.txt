model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	651
total timesteps	75
turtle that knows p	226
turtle that knows not p	75
turtle that discovers p (id,ranking,skeptic?)	62	1	true
turtle that discovers not p (id,ranking,skeptic?)	214	1	true
