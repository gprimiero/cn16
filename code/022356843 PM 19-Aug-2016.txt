model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	468
total timesteps	73
turtle that knows p	225
turtle that knows not p	76
turtle that discovers p (id,ranking,skeptic?)	117	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	129	1	true
