model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	123
total timesteps	25
turtle that knows p	13
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	7	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
