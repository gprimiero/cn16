model parameters
nodes	100
skeptics	54
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	516
total timesteps	95
turtle that knows p	75
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	36	1	false
turtle that discovers not p (id,ranking,skeptic?)	100	1	true
