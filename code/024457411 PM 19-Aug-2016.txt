model parameters
nodes	30
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	224
total timesteps	35
turtle that knows p	14
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	28	1	true
turtle that discovers not p (id,ranking,skeptic?)	19	1	true
