model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	841
total timesteps	9
turtle that knows p	184
turtle that knows not p	116
turtle that discovers p (id,ranking,skeptic?)	154	0	false
turtle that discovers not p (id,ranking,skeptic?)	269	0	true
