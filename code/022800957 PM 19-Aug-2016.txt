model parameters
nodes	50
skeptics	3
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	67
total timesteps	15
turtle that knows p	44
turtle that knows not p	7
turtle that discovers p (id,ranking,skeptic?)	36	1	false
turtle that discovers not p (id,ranking,skeptic?)	18	1	false
