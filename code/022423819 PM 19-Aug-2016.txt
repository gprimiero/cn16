model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	670
total timesteps	128
turtle that knows p	84
turtle that knows not p	217
turtle that discovers p (id,ranking,skeptic?)	125	1	false
turtle that discovers not p (id,ranking,skeptic?)	52	1	false
