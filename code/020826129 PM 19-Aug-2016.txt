model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	690
total timesteps	76
turtle that knows p	301
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	224	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	226	1	false
