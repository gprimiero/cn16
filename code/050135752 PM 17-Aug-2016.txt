model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	66
total timesteps	20
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	48	1	false
turtle that discovers not p (id,ranking,skeptic?)	23	1	false
