model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	631
total timesteps	88
turtle that knows p	148
turtle that knows not p	153
turtle that discovers p (id,ranking,skeptic?)	226	1	true
turtle that discovers not p (id,ranking,skeptic?)	290	1	true
