model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	131
total timesteps	26
turtle that knows p	22
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	24	1	true
turtle that discovers not p (id,ranking,skeptic?)	1	0.25	true
