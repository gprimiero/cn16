model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	984
total timesteps	70
turtle that knows p	106
turtle that knows not p	195
turtle that discovers p (id,ranking,skeptic?)	95	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	10	0.5	true
