model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	129
total timesteps	21
turtle that knows p	9
turtle that knows not p	32
turtle that discovers p (id,ranking,skeptic?)	6	1	false
turtle that discovers not p (id,ranking,skeptic?)	37	1	true
