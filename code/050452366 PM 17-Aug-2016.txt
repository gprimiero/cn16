model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1023
total timesteps	111
turtle that knows p	182
turtle that knows not p	119
turtle that discovers p (id,ranking,skeptic?)	222	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	7	0.06666666666666667	true
