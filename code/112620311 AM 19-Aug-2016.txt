model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	422
total timesteps	65
turtle that knows p	111
turtle that knows not p	190
turtle that discovers p (id,ranking,skeptic?)	296	1	false
turtle that discovers not p (id,ranking,skeptic?)	236	1	true
