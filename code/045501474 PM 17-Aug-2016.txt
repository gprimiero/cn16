model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	163
total timesteps	27
turtle that knows p	47
turtle that knows not p	4
turtle that discovers p (id,ranking,skeptic?)	42	1	false
turtle that discovers not p (id,ranking,skeptic?)	43	1	true
