model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1945
total timesteps	221
turtle that knows p	84
turtle that knows not p	216
turtle that discovers p (id,ranking,skeptic?)	89	34	true
turtle that discovers not p (id,ranking,skeptic?)	181	78	true
