model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	379
total timesteps	33
turtle that knows p	64
turtle that knows not p	237
turtle that discovers p (id,ranking,skeptic?)	80	1	true
turtle that discovers not p (id,ranking,skeptic?)	101	1	true
