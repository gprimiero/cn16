model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	251
total timesteps	49
turtle that knows p	31
turtle that knows not p	70
turtle that discovers p (id,ranking,skeptic?)	70	1	false
turtle that discovers not p (id,ranking,skeptic?)	43	0.5	false
