model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	801
total timesteps	79
turtle that knows p	185
turtle that knows not p	116
turtle that discovers p (id,ranking,skeptic?)	153	1	true
turtle that discovers not p (id,ranking,skeptic?)	246	1	true
