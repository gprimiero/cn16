model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	824
total timesteps	95
turtle that knows p	178
turtle that knows not p	123
turtle that discovers p (id,ranking,skeptic?)	203	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	47	0.5	true
