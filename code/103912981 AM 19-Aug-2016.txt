model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	335
total timesteps	61
turtle that knows p	249
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	27	0.14285714285714285	false
turtle that discovers not p (id,ranking,skeptic?)	204	1	false
