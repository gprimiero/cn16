model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	179
total timesteps	32
turtle that knows p	43
turtle that knows not p	8
turtle that discovers p (id,ranking,skeptic?)	34	1	true
turtle that discovers not p (id,ranking,skeptic?)	45	1	true
