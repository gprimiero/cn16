model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	244
total timesteps	45
turtle that knows p	86
turtle that knows not p	215
turtle that discovers p (id,ranking,skeptic?)	27	1	false
turtle that discovers not p (id,ranking,skeptic?)	213	1	true
