model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1374
total timesteps	109
turtle that knows p	141
turtle that knows not p	160
turtle that discovers p (id,ranking,skeptic?)	135	1	true
turtle that discovers not p (id,ranking,skeptic?)	188	1	false
