model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	132
total timesteps	26
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	13	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	17	0.25	false
model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	157
total timesteps	39
turtle that knows p	17
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	15	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	4	0.14285714285714285	false
