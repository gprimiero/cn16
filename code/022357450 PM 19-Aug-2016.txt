model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	284
total timesteps	44
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	64	1	true
turtle that discovers not p (id,ranking,skeptic?)	250	1	true
