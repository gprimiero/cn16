model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	392
total timesteps	77
turtle that knows p	170
turtle that knows not p	131
turtle that discovers p (id,ranking,skeptic?)	12	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	257	1	false
