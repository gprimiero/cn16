model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	188
total timesteps	48
turtle that knows p	61
turtle that knows not p	140
turtle that discovers p (id,ranking,skeptic?)	199	1	true
turtle that discovers not p (id,ranking,skeptic?)	61	0.3333333333333333	false
