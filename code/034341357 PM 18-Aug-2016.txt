model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1390
total timesteps	10
turtle that knows p	151
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	37	0	false
turtle that discovers not p (id,ranking,skeptic?)	214	0	true
