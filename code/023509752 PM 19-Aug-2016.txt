model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	230
total timesteps	38
turtle that knows p	34
turtle that knows not p	67
turtle that discovers p (id,ranking,skeptic?)	82	1	true
turtle that discovers not p (id,ranking,skeptic?)	80	0.5	false
