model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	256
total timesteps	32
turtle that knows p	268
turtle that knows not p	33
turtle that discovers p (id,ranking,skeptic?)	32	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	157	1	true
model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	385
total timesteps	72
turtle that knows p	90
turtle that knows not p	211
turtle that discovers p (id,ranking,skeptic?)	68	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	233	1	true
