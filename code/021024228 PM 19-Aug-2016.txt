model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1159
total timesteps	68
turtle that knows p	231
turtle that knows not p	70
turtle that discovers p (id,ranking,skeptic?)	111	1	true
turtle that discovers not p (id,ranking,skeptic?)	72	1	true
