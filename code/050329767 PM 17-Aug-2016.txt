model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	93
total timesteps	19
turtle that knows p	24
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	31	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	50	1	true
