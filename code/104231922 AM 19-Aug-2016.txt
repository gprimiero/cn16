model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	809
total timesteps	46
turtle that knows p	18
turtle that knows not p	283
turtle that discovers p (id,ranking,skeptic?)	194	1	false
turtle that discovers not p (id,ranking,skeptic?)	299	1	true
