model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	68
total timesteps	25
turtle that knows p	0
turtle that knows not p	51
turtle that discovers p (id,ranking,skeptic?)	18	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
