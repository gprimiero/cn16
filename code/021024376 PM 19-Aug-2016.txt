model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	920
total timesteps	106
turtle that knows p	114
turtle that knows not p	187
turtle that discovers p (id,ranking,skeptic?)	158	1	false
turtle that discovers not p (id,ranking,skeptic?)	281	1	false
