model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	96
total timesteps	21
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	36	1	false
turtle that discovers not p (id,ranking,skeptic?)	48	1	false
