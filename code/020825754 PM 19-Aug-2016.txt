model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	594
total timesteps	80
turtle that knows p	151
turtle that knows not p	150
turtle that discovers p (id,ranking,skeptic?)	59	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	281	1	false
model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	531
total timesteps	82
turtle that knows p	8
turtle that knows not p	293
turtle that discovers p (id,ranking,skeptic?)	98	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	121	1	true
