model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	229
total timesteps	43
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	162	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	205	1	false
