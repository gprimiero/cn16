model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	134
total timesteps	23
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	27	1	false
turtle that discovers not p (id,ranking,skeptic?)	42	1	false
model parameters
nodes	300
skeptics	157
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	369
total timesteps	61
turtle that knows p	180
turtle that knows not p	121
turtle that discovers p (id,ranking,skeptic?)	1	0.1111111111111111	false
turtle that discovers not p (id,ranking,skeptic?)	84	1	false
