model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	786
total timesteps	283
turtle that knows p	300
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	226	16	false
turtle that discovers not p (id,ranking,skeptic?)	184	1	false
