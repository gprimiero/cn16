model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	123
total timesteps	38
turtle that knows p	15
turtle that knows not p	186
turtle that discovers p (id,ranking,skeptic?)	177	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	0.3333333333333333	false
