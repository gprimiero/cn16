model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	526
total timesteps	57
turtle that knows p	31
turtle that knows not p	270
turtle that discovers p (id,ranking,skeptic?)	259	1	true
turtle that discovers not p (id,ranking,skeptic?)	109	1	true
