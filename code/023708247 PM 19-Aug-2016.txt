model parameters
nodes	40
skeptics	18
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	86
total timesteps	19
turtle that knows p	25
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	33	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	false
