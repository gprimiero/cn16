model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	377
total timesteps	66
turtle that knows p	276
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	164	1	false
turtle that discovers not p (id,ranking,skeptic?)	218	1	true
