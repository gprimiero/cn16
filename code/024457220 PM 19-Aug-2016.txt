model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	79
total timesteps	22
turtle that knows p	8
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	16	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	3	0.16666666666666666	true
