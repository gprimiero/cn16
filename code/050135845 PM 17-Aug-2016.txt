model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	830
total timesteps	9
turtle that knows p	173
turtle that knows not p	127
turtle that discovers p (id,ranking,skeptic?)	215	0	true
turtle that discovers not p (id,ranking,skeptic?)	28	0	true
