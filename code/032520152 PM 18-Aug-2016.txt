model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	864
total timesteps	9
turtle that knows p	173
turtle that knows not p	127
turtle that discovers p (id,ranking,skeptic?)	146	0	false
turtle that discovers not p (id,ranking,skeptic?)	189	0	true
