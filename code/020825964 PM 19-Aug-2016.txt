model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	654
total timesteps	72
turtle that knows p	247
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	33	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	270	1	false
