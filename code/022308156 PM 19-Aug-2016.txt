model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	583
total timesteps	52
turtle that knows p	55
turtle that knows not p	246
turtle that discovers p (id,ranking,skeptic?)	260	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.25	false
