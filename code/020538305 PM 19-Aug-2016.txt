model parameters
nodes	300
skeptics	32
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	128
total timesteps	45
turtle that knows p	140
turtle that knows not p	161
turtle that discovers p (id,ranking,skeptic?)	185	1	false
turtle that discovers not p (id,ranking,skeptic?)	132	1	true
