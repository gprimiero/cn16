model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	69
total timesteps	23
turtle that knows p	36
turtle that knows not p	65
turtle that discovers p (id,ranking,skeptic?)	56	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	0.5	false
