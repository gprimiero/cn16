model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1782
total timesteps	111
turtle that knows p	172
turtle that knows not p	129
turtle that discovers p (id,ranking,skeptic?)	49	1	false
turtle that discovers not p (id,ranking,skeptic?)	237	1	true
