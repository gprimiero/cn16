model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	52
total timesteps	14
turtle that knows p	19
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	1	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	5	0.3333333333333333	false
