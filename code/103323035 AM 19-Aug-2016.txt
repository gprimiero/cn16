model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	437
total timesteps	68
turtle that knows p	28
turtle that knows not p	273
turtle that discovers p (id,ranking,skeptic?)	224	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	209	1	false
