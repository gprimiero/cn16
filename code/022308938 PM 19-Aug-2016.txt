model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	764
total timesteps	72
turtle that knows p	167
turtle that knows not p	134
turtle that discovers p (id,ranking,skeptic?)	170	1	false
turtle that discovers not p (id,ranking,skeptic?)	114	0.3333333333333333	false
