model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	106
total timesteps	23
turtle that knows p	8
turtle that knows not p	43
turtle that discovers p (id,ranking,skeptic?)	34	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
