model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	2084
total timesteps	233
turtle that knows p	33
turtle that knows not p	267
turtle that discovers p (id,ranking,skeptic?)	51	280	false
turtle that discovers not p (id,ranking,skeptic?)	155	233	true
