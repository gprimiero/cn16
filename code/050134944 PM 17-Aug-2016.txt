model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	152
total timesteps	37
turtle that knows p	32
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	41	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
