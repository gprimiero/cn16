model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	121
total timesteps	26
turtle that knows p	18
turtle that knows not p	13
turtle that discovers p (id,ranking,skeptic?)	14	1	true
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
