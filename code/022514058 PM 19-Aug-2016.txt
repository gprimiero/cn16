model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	232
total timesteps	76
turtle that knows p	213
turtle that knows not p	88
turtle that discovers p (id,ranking,skeptic?)	42	1	false
turtle that discovers not p (id,ranking,skeptic?)	112	1	true
