model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	884
total timesteps	9
turtle that knows p	145
turtle that knows not p	155
turtle that discovers p (id,ranking,skeptic?)	50	0	false
turtle that discovers not p (id,ranking,skeptic?)	268	0	true
