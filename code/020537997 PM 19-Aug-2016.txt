model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	305
total timesteps	65
turtle that knows p	260
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	285	1	false
turtle that discovers not p (id,ranking,skeptic?)	254	1	false
