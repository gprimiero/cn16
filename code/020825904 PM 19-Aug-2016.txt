model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	297
total timesteps	50
turtle that knows p	201
turtle that knows not p	100
turtle that discovers p (id,ranking,skeptic?)	46	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	182	1	true
