model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	523
total timesteps	113
turtle that knows p	121
turtle that knows not p	179
turtle that discovers p (id,ranking,skeptic?)	131	64	true
turtle that discovers not p (id,ranking,skeptic?)	299	186	false
