model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	123
total timesteps	25
turtle that knows p	4
turtle that knows not p	47
turtle that discovers p (id,ranking,skeptic?)	49	1	true
turtle that discovers not p (id,ranking,skeptic?)	15	0.5	true
