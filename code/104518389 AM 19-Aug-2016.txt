model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1450
total timesteps	110
turtle that knows p	208
turtle that knows not p	93
turtle that discovers p (id,ranking,skeptic?)	65	1	true
turtle that discovers not p (id,ranking,skeptic?)	37	1	true
