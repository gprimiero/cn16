model parameters
nodes	50
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	442
total timesteps	53
turtle that knows p	180
turtle that knows not p	121
turtle that discovers p (id,ranking,skeptic?)	106	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	141	1	false
