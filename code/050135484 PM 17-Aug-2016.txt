model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	151
total timesteps	30
turtle that knows p	1
turtle that knows not p	50
turtle that discovers p (id,ranking,skeptic?)	34	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
