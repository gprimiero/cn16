model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	373
total timesteps	61
turtle that knows p	250
turtle that knows not p	51
turtle that discovers p (id,ranking,skeptic?)	127	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	34	1	true
