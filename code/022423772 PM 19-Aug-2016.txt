model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	324
total timesteps	55
turtle that knows p	197
turtle that knows not p	104
turtle that discovers p (id,ranking,skeptic?)	278	1	false
turtle that discovers not p (id,ranking,skeptic?)	247	1	false
