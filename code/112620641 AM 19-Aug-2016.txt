model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	524
total timesteps	61
turtle that knows p	132
turtle that knows not p	169
turtle that discovers p (id,ranking,skeptic?)	81	1	false
turtle that discovers not p (id,ranking,skeptic?)	145	1	true
