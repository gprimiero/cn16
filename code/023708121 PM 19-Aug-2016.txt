model parameters
nodes	40
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	622
total timesteps	79
turtle that knows p	99
turtle that knows not p	202
turtle that discovers p (id,ranking,skeptic?)	157	1	false
turtle that discovers not p (id,ranking,skeptic?)	141	1	false
