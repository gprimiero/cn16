model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1119
total timesteps	150
turtle that knows p	234
turtle that knows not p	67
turtle that discovers p (id,ranking,skeptic?)	98	1	false
turtle that discovers not p (id,ranking,skeptic?)	278	1	false
