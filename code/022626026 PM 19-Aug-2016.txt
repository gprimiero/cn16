model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	116
total timesteps	31
turtle that knows p	66
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	82	1	false
turtle that discovers not p (id,ranking,skeptic?)	48	0.5	false
