model parameters
nodes	300
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	36
total timesteps	71
turtle that knows p	0
turtle that knows not p	301
turtle that discovers p (id,ranking,skeptic?)	100	1	false
turtle that discovers not p (id,ranking,skeptic?)	84	0.5	true
