model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	419
total timesteps	47
turtle that knows p	225
turtle that knows not p	76
turtle that discovers p (id,ranking,skeptic?)	13	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	163	1	true
