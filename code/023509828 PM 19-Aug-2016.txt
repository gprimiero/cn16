model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	286
total timesteps	39
turtle that knows p	82
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	74	1	false
turtle that discovers not p (id,ranking,skeptic?)	25	0.5	true
