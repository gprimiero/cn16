model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	671
total timesteps	56
turtle that knows p	136
turtle that knows not p	165
turtle that discovers p (id,ranking,skeptic?)	159	1	true
turtle that discovers not p (id,ranking,skeptic?)	64	0.5	true
