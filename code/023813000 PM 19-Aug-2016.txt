model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	19
total timesteps	11
turtle that knows p	31
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	27	1	true
turtle that discovers not p (id,ranking,skeptic?)	2	0.09090909090909091	true
