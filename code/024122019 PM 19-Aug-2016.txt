model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	289
total timesteps	39
turtle that knows p	140
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	4	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	107	0.5	true
