model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	153
total timesteps	41
turtle that knows p	79
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	98	1	true
turtle that discovers not p (id,ranking,skeptic?)	76	1	false
