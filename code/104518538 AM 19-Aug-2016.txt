model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1103
total timesteps	127
turtle that knows p	193
turtle that knows not p	108
turtle that discovers p (id,ranking,skeptic?)	174	1	true
turtle that discovers not p (id,ranking,skeptic?)	41	0.5	true
