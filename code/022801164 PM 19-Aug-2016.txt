model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	207
total timesteps	39
turtle that knows p	15
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	25	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
