model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	65
total timesteps	17
turtle that knows p	35
turtle that knows not p	6
turtle that discovers p (id,ranking,skeptic?)	5	1	false
turtle that discovers not p (id,ranking,skeptic?)	24	1	false
model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	175
total timesteps	54
turtle that knows p	14
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	24	1	false
turtle that discovers not p (id,ranking,skeptic?)	8	0.5	false
