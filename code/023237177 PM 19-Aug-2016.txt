model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1493
total timesteps	119
turtle that knows p	152
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	171	1	false
turtle that discovers not p (id,ranking,skeptic?)	62	1	true
