model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1170
total timesteps	100
turtle that knows p	243
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	15	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	220	1	false
