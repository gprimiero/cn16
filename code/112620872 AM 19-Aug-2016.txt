model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	177
total timesteps	39
turtle that knows p	0
turtle that knows not p	301
turtle that discovers p (id,ranking,skeptic?)	292	1	false
turtle that discovers not p (id,ranking,skeptic?)	163	1	true
