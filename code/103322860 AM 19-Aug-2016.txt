model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	358
total timesteps	59
turtle that knows p	291
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	20	0.09090909090909091	false
turtle that discovers not p (id,ranking,skeptic?)	75	0.3333333333333333	false
