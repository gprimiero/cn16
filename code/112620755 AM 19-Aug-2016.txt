model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	350
total timesteps	60
turtle that knows p	242
turtle that knows not p	59
turtle that discovers p (id,ranking,skeptic?)	109	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	149	1	true
model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	483
total timesteps	54
turtle that knows p	118
turtle that knows not p	183
turtle that discovers p (id,ranking,skeptic?)	47	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	1	true
