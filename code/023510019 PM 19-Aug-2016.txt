model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	185
total timesteps	36
turtle that knows p	59
turtle that knows not p	42
turtle that discovers p (id,ranking,skeptic?)	72	1	false
turtle that discovers not p (id,ranking,skeptic?)	52	0.3333333333333333	true
