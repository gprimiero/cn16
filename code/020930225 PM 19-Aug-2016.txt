model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	263
total timesteps	46
turtle that knows p	301
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	32	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	237	1	true
