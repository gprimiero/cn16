model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	306
total timesteps	38
turtle that knows p	81
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	45	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
