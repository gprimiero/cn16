model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1156
total timesteps	91
turtle that knows p	172
turtle that knows not p	129
turtle that discovers p (id,ranking,skeptic?)	50	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	153	1	false
