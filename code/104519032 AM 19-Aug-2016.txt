model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1152
total timesteps	101
turtle that knows p	148
turtle that knows not p	153
turtle that discovers p (id,ranking,skeptic?)	150	1	true
turtle that discovers not p (id,ranking,skeptic?)	88	1	true
