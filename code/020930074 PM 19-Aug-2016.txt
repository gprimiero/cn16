model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	486
total timesteps	44
turtle that knows p	19
turtle that knows not p	282
turtle that discovers p (id,ranking,skeptic?)	291	1	true
turtle that discovers not p (id,ranking,skeptic?)	0	0.3333333333333333	true
