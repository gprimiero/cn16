model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1422
total timesteps	216
turtle that knows p	78
turtle that knows not p	222
turtle that discovers p (id,ranking,skeptic?)	96	28	true
turtle that discovers not p (id,ranking,skeptic?)	109	83	true
