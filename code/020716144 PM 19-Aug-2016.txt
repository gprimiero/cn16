model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1373
total timesteps	82
turtle that knows p	33
turtle that knows not p	268
turtle that discovers p (id,ranking,skeptic?)	53	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	52	0.25	true
