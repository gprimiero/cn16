model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	812
total timesteps	68
turtle that knows p	95
turtle that knows not p	206
turtle that discovers p (id,ranking,skeptic?)	234	1	true
turtle that discovers not p (id,ranking,skeptic?)	73	1	true
