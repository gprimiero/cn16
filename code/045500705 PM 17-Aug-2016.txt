model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1073
total timesteps	191
turtle that knows p	85
turtle that knows not p	215
turtle that discovers p (id,ranking,skeptic?)	226	2	true
turtle that discovers not p (id,ranking,skeptic?)	207	108	false
