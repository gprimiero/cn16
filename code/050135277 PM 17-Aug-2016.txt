model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	121
total timesteps	22
turtle that knows p	9
turtle that knows not p	42
turtle that discovers p (id,ranking,skeptic?)	33	1	false
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	true
