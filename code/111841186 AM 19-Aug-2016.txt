model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	682
total timesteps	111
turtle that knows p	113
turtle that knows not p	188
turtle that discovers p (id,ranking,skeptic?)	58	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	250	1	true
