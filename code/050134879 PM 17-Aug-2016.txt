model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	53
total timesteps	19
turtle that knows p	26
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	36	1	false
turtle that discovers not p (id,ranking,skeptic?)	34	0.5	true
