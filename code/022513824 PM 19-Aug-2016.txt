model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1609
total timesteps	85
turtle that knows p	139
turtle that knows not p	162
turtle that discovers p (id,ranking,skeptic?)	3	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	14	0.08333333333333333	true
