model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	212
total timesteps	42
turtle that knows p	23
turtle that knows not p	78
turtle that discovers p (id,ranking,skeptic?)	73	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	98	1	true
