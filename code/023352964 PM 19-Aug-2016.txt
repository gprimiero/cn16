model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	214
total timesteps	49
turtle that knows p	86
turtle that knows not p	115
turtle that discovers p (id,ranking,skeptic?)	48	1	true
turtle that discovers not p (id,ranking,skeptic?)	82	1	true
