model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1099
total timesteps	8
turtle that knows p	158
turtle that knows not p	142
turtle that discovers p (id,ranking,skeptic?)	259	0	false
turtle that discovers not p (id,ranking,skeptic?)	11	0	true
