model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	61
total timesteps	14
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	6	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	5	1	false
