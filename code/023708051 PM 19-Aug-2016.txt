model parameters
nodes	40
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	165
total timesteps	19
turtle that knows p	32
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	4	0.14285714285714285	false
turtle that discovers not p (id,ranking,skeptic?)	16	0.25	true
