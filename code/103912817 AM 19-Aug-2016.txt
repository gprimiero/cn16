model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	273
total timesteps	54
turtle that knows p	221
turtle that knows not p	80
turtle that discovers p (id,ranking,skeptic?)	258	1	false
turtle that discovers not p (id,ranking,skeptic?)	165	1	true
