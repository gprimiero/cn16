model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	340
total timesteps	48
turtle that knows p	223
turtle that knows not p	78
turtle that discovers p (id,ranking,skeptic?)	232	1	true
turtle that discovers not p (id,ranking,skeptic?)	204	1	true
