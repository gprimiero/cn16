model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	598
total timesteps	72
turtle that knows p	16
turtle that knows not p	285
turtle that discovers p (id,ranking,skeptic?)	180	1	true
turtle that discovers not p (id,ranking,skeptic?)	136	1	true
