model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	302
total timesteps	52
turtle that knows p	221
turtle that knows not p	80
turtle that discovers p (id,ranking,skeptic?)	77	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	34	0.25	true
