model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	910
total timesteps	79
turtle that knows p	81
turtle that knows not p	220
turtle that discovers p (id,ranking,skeptic?)	223	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	8	0.2	true
