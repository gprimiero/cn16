model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	70
total timesteps	14
turtle that knows p	38
turtle that knows not p	13
turtle that discovers p (id,ranking,skeptic?)	1	0.14285714285714285	true
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
