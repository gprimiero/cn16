model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	239
total timesteps	44
turtle that knows p	243
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	19	1	false
turtle that discovers not p (id,ranking,skeptic?)	82	1	false
