model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	73
total timesteps	13
turtle that knows p	1
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	23	1	true
turtle that discovers not p (id,ranking,skeptic?)	7	0.3333333333333333	true
