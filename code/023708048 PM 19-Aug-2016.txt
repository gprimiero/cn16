model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	72
total timesteps	16
turtle that knows p	30
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	28	1	true
turtle that discovers not p (id,ranking,skeptic?)	39	1	false
