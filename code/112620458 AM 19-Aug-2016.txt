model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	1040
total timesteps	61
turtle that knows p	133
turtle that knows not p	168
turtle that discovers p (id,ranking,skeptic?)	274	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	0.5	true
