model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	405
total timesteps	54
turtle that knows p	54
turtle that knows not p	247
turtle that discovers p (id,ranking,skeptic?)	5	0.05555555555555555	false
turtle that discovers not p (id,ranking,skeptic?)	196	1	true
