model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	370
total timesteps	48
turtle that knows p	235
turtle that knows not p	66
turtle that discovers p (id,ranking,skeptic?)	226	1	false
turtle that discovers not p (id,ranking,skeptic?)	67	0.3333333333333333	false
