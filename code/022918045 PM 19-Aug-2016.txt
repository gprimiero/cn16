model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	63
total timesteps	18
turtle that knows p	19
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	16	1	false
turtle that discovers not p (id,ranking,skeptic?)	24	1	false
