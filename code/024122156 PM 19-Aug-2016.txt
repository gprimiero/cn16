model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	436
total timesteps	44
turtle that knows p	144
turtle that knows not p	57
turtle that discovers p (id,ranking,skeptic?)	144	1	true
turtle that discovers not p (id,ranking,skeptic?)	16	0.5	true
