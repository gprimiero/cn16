model parameters
nodes	40
skeptics	18
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	36
total timesteps	12
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	20	1	true
turtle that discovers not p (id,ranking,skeptic?)	22	0.25	false
