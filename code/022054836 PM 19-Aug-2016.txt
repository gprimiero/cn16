model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	142
total timesteps	32
turtle that knows p	27
turtle that knows not p	174
turtle that discovers p (id,ranking,skeptic?)	75	1	true
turtle that discovers not p (id,ranking,skeptic?)	70	0.3333333333333333	false
