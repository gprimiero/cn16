model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	211
total timesteps	38
turtle that knows p	10
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	40	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	1	false
