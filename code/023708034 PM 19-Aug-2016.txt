model parameters
nodes	40
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	399
total timesteps	57
turtle that knows p	58
turtle that knows not p	243
turtle that discovers p (id,ranking,skeptic?)	249	1	true
turtle that discovers not p (id,ranking,skeptic?)	140	1	true
