model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	456
total timesteps	65
turtle that knows p	90
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	88	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
