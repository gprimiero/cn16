model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	371
total timesteps	87
turtle that knows p	187
turtle that knows not p	114
turtle that discovers p (id,ranking,skeptic?)	136	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	172	1	false
