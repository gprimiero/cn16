model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	619
total timesteps	71
turtle that knows p	55
turtle that knows not p	246
turtle that discovers p (id,ranking,skeptic?)	181	1	false
turtle that discovers not p (id,ranking,skeptic?)	164	1	false
