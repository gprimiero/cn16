model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	94
total timesteps	22
turtle that knows p	19
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	17	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	14	1	false
