model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	115
total timesteps	24
turtle that knows p	28
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	15	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	0.5	true
