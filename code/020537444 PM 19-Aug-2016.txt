model parameters
nodes	300
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1751
total timesteps	162
turtle that knows p	95
turtle that knows not p	206
turtle that discovers p (id,ranking,skeptic?)	146	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	0.14285714285714285	false
