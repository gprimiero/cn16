model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	84
total timesteps	37
turtle that knows p	20
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	8	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	25	1	false
model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	45
total timesteps	16
turtle that knows p	34
turtle that knows not p	7
turtle that discovers p (id,ranking,skeptic?)	36	1	false
turtle that discovers not p (id,ranking,skeptic?)	29	1	true
