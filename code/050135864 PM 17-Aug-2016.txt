model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	134
total timesteps	30
turtle that knows p	19
turtle that knows not p	32
turtle that discovers p (id,ranking,skeptic?)	13	1	true
turtle that discovers not p (id,ranking,skeptic?)	46	1	false
