model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1275
total timesteps	252
turtle that knows p	50
turtle that knows not p	250
turtle that discovers p (id,ranking,skeptic?)	169	16	true
turtle that discovers not p (id,ranking,skeptic?)	128	47	false
