model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1259
total timesteps	139
turtle that knows p	231
turtle that knows not p	70
turtle that discovers p (id,ranking,skeptic?)	5	0.05555555555555555	false
turtle that discovers not p (id,ranking,skeptic?)	284	1	false
