model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	150
total timesteps	46
turtle that knows p	104
turtle that knows not p	197
turtle that discovers p (id,ranking,skeptic?)	260	1	false
turtle that discovers not p (id,ranking,skeptic?)	296	1	false
