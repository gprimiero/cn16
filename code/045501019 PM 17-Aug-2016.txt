model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	36
total timesteps	22
turtle that knows p	27
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	42	1	false
turtle that discovers not p (id,ranking,skeptic?)	7	1	true
model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	846
total timesteps	156
turtle that knows p	198
turtle that knows not p	102
turtle that discovers p (id,ranking,skeptic?)	71	144	true
turtle that discovers not p (id,ranking,skeptic?)	200	41	false
