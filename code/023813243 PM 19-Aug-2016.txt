model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	96
total timesteps	22
turtle that knows p	4
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	21	1	false
turtle that discovers not p (id,ranking,skeptic?)	20	0.5	false
