model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	770
total timesteps	78
turtle that knows p	206
turtle that knows not p	95
turtle that discovers p (id,ranking,skeptic?)	129	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	53	1	false
