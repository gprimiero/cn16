model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	644
total timesteps	87
turtle that knows p	81
turtle that knows not p	220
turtle that discovers p (id,ranking,skeptic?)	106	1	true
turtle that discovers not p (id,ranking,skeptic?)	240	1	true
