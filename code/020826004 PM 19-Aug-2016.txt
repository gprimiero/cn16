model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	390
total timesteps	82
turtle that knows p	191
turtle that knows not p	110
turtle that discovers p (id,ranking,skeptic?)	65	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	true
