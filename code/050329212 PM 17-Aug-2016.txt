model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	92
total timesteps	25
turtle that knows p	30
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	41	1	true
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
