model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1398
total timesteps	72
turtle that knows p	150
turtle that knows not p	151
turtle that discovers p (id,ranking,skeptic?)	131	1	false
turtle that discovers not p (id,ranking,skeptic?)	221	1	true
