model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	127
total timesteps	21
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	0	0.08333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.3333333333333333	false
