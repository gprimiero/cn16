model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	70
total timesteps	22
turtle that knows p	14
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	44	1	true
turtle that discovers not p (id,ranking,skeptic?)	24	1	false
