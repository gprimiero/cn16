model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	50
total timesteps	22
turtle that knows p	12
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	10	1	false
turtle that discovers not p (id,ranking,skeptic?)	6	0.125	false
