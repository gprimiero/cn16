model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	526
total timesteps	68
turtle that knows p	45
turtle that knows not p	256
turtle that discovers p (id,ranking,skeptic?)	21	0.058823529411764705	true
turtle that discovers not p (id,ranking,skeptic?)	55	1	true
