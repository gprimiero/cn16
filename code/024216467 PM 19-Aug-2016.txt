model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	299
total timesteps	41
turtle that knows p	30
turtle that knows not p	71
turtle that discovers p (id,ranking,skeptic?)	44	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	0.5	true
