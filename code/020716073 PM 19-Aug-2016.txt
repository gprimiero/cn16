model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	811
total timesteps	119
turtle that knows p	128
turtle that knows not p	173
turtle that discovers p (id,ranking,skeptic?)	7	1	false
turtle that discovers not p (id,ranking,skeptic?)	103	1	false
