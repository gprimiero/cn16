model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	768
total timesteps	108
turtle that knows p	125
turtle that knows not p	176
turtle that discovers p (id,ranking,skeptic?)	85	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	175	0.5	false
