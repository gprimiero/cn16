model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	139
total timesteps	29
turtle that knows p	18
turtle that knows not p	183
turtle that discovers p (id,ranking,skeptic?)	51	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	147	1	false
