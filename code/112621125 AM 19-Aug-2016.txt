model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	573
total timesteps	60
turtle that knows p	244
turtle that knows not p	57
turtle that discovers p (id,ranking,skeptic?)	218	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	166	1	true
