model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	51
total timesteps	16
turtle that knows p	19
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	27	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	76
total timesteps	21
turtle that knows p	25
turtle that knows not p	6
turtle that discovers p (id,ranking,skeptic?)	10	1	true
turtle that discovers not p (id,ranking,skeptic?)	19	1	true
