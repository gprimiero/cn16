model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1325
total timesteps	10
turtle that knows p	167
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	116	0	false
turtle that discovers not p (id,ranking,skeptic?)	254	0	true
