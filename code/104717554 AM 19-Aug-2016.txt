model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	791
total timesteps	101
turtle that knows p	95
turtle that knows not p	206
turtle that discovers p (id,ranking,skeptic?)	244	1	true
turtle that discovers not p (id,ranking,skeptic?)	300	1	true
