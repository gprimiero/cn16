model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	854
total timesteps	180
turtle that knows p	82
turtle that knows not p	218
turtle that discovers p (id,ranking,skeptic?)	296	55	false
turtle that discovers not p (id,ranking,skeptic?)	202	120	false
