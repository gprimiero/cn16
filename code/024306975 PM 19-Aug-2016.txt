model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	54
total timesteps	17
turtle that knows p	51
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	42	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	43	1	true
model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	93
total timesteps	20
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	23	1	true
turtle that discovers not p (id,ranking,skeptic?)	18	1	false
