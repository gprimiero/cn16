model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	162
total timesteps	46
turtle that knows p	191
turtle that knows not p	110
turtle that discovers p (id,ranking,skeptic?)	38	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	35	1	true
