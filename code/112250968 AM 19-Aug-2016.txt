model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	403
total timesteps	58
turtle that knows p	217
turtle that knows not p	84
turtle that discovers p (id,ranking,skeptic?)	59	1	false
turtle that discovers not p (id,ranking,skeptic?)	27	1	false
