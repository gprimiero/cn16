model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	86
total timesteps	22
turtle that knows p	16
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	15	1	false
turtle that discovers not p (id,ranking,skeptic?)	1	1	true
