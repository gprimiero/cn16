model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	304
total timesteps	78
turtle that knows p	170
turtle that knows not p	131
turtle that discovers p (id,ranking,skeptic?)	26	0.1	false
turtle that discovers not p (id,ranking,skeptic?)	104	0.3333333333333333	false
