model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	390
total timesteps	42
turtle that knows p	82
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	52	1	false
turtle that discovers not p (id,ranking,skeptic?)	44	1	false
