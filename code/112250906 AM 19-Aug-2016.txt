model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	1666
total timesteps	165
turtle that knows p	241
turtle that knows not p	60
turtle that discovers p (id,ranking,skeptic?)	85	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	22	0.5	false
