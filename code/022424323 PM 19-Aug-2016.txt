model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	628
total timesteps	68
turtle that knows p	214
turtle that knows not p	87
turtle that discovers p (id,ranking,skeptic?)	36	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	281	1	false
