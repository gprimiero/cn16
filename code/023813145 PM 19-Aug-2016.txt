model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	92
total timesteps	18
turtle that knows p	11
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	12	1	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	61
total timesteps	16
turtle that knows p	25
turtle that knows not p	6
turtle that discovers p (id,ranking,skeptic?)	23	1	true
turtle that discovers not p (id,ranking,skeptic?)	22	1	false
