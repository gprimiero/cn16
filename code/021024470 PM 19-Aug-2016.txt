model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	539
total timesteps	50
turtle that knows p	125
turtle that knows not p	176
turtle that discovers p (id,ranking,skeptic?)	142	1	true
turtle that discovers not p (id,ranking,skeptic?)	146	0.3333333333333333	true
