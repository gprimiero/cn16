model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	719
total timesteps	82
turtle that knows p	148
turtle that knows not p	153
turtle that discovers p (id,ranking,skeptic?)	172	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	184	1	true
