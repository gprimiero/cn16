model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	341
total timesteps	49
turtle that knows p	97
turtle that knows not p	204
turtle that discovers p (id,ranking,skeptic?)	127	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	66	1	false
