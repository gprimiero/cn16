model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	110
total timesteps	26
turtle that knows p	6
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	26	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	23	1	true
