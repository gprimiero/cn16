model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	374
total timesteps	50
turtle that knows p	6
turtle that knows not p	195
turtle that discovers p (id,ranking,skeptic?)	188	1	true
turtle that discovers not p (id,ranking,skeptic?)	115	0.5	true
