model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	554
total timesteps	84
turtle that knows p	96
turtle that knows not p	205
turtle that discovers p (id,ranking,skeptic?)	129	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	34	1	false
