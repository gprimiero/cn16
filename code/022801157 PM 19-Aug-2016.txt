model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	22
total timesteps	14
turtle that knows p	16
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	22	1	true
turtle that discovers not p (id,ranking,skeptic?)	28	1	true
