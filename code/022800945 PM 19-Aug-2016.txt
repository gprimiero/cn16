model parameters
nodes	50
skeptics	5
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	88
total timesteps	26
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	2	0.1	true
turtle that discovers not p (id,ranking,skeptic?)	3	1	false
