model parameters
nodes	100
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	591
total timesteps	66
turtle that knows p	264
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	89	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	274	1	true
