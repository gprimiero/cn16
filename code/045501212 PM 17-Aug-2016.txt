model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	253
total timesteps	31
turtle that knows p	26
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	47	1	true
turtle that discovers not p (id,ranking,skeptic?)	13	1	true
