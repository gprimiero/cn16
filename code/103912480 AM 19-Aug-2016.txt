model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	632
total timesteps	80
turtle that knows p	123
turtle that knows not p	178
turtle that discovers p (id,ranking,skeptic?)	79	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	220	1	false
