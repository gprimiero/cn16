model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	440
total timesteps	67
turtle that knows p	186
turtle that knows not p	115
turtle that discovers p (id,ranking,skeptic?)	254	1	true
turtle that discovers not p (id,ranking,skeptic?)	126	1	true
