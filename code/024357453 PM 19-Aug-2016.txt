model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	89
total timesteps	19
turtle that knows p	3
turtle that knows not p	38
turtle that discovers p (id,ranking,skeptic?)	38	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	true
model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	99
total timesteps	19
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	17	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
