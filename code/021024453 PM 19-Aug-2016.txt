model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1815
total timesteps	88
turtle that knows p	121
turtle that knows not p	180
turtle that discovers p (id,ranking,skeptic?)	187	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	290	1	true
