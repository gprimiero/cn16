model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	146
total timesteps	25
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	9	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
