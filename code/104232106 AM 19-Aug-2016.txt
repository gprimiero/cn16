model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	627
total timesteps	79
turtle that knows p	34
turtle that knows not p	267
turtle that discovers p (id,ranking,skeptic?)	195	1	true
turtle that discovers not p (id,ranking,skeptic?)	215	0.5	false
