model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1035
total timesteps	109
turtle that knows p	222
turtle that knows not p	79
turtle that discovers p (id,ranking,skeptic?)	280	1	false
turtle that discovers not p (id,ranking,skeptic?)	98	1	false
