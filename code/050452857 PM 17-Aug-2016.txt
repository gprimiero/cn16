model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	660
total timesteps	106
turtle that knows p	199
turtle that knows not p	102
turtle that discovers p (id,ranking,skeptic?)	127	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	15	0.2	true
model parameters
nodes	300
skeptics	157
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	400
total timesteps	61
turtle that knows p	29
turtle that knows not p	272
turtle that discovers p (id,ranking,skeptic?)	50	1	true
turtle that discovers not p (id,ranking,skeptic?)	298	1	true
