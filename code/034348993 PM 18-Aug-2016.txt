model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	884
total timesteps	9
turtle that knows p	136
turtle that knows not p	164
turtle that discovers p (id,ranking,skeptic?)	294	0	false
turtle that discovers not p (id,ranking,skeptic?)	46	0	true
