model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	91
total timesteps	20
turtle that knows p	30
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	7	1	true
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	49
total timesteps	13
turtle that knows p	23
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	0	1	true
turtle that discovers not p (id,ranking,skeptic?)	8	1	true
