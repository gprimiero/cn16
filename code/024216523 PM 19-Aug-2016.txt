model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	238
total timesteps	37
turtle that knows p	56
turtle that knows not p	45
turtle that discovers p (id,ranking,skeptic?)	49	1	true
turtle that discovers not p (id,ranking,skeptic?)	39	1	true
