model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	454
total timesteps	54
turtle that knows p	227
turtle that knows not p	74
turtle that discovers p (id,ranking,skeptic?)	102	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	135	1	true
