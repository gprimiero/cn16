model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	12
total timesteps	12
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	0	0.1111111111111111	false
turtle that discovers not p (id,ranking,skeptic?)	4	0.5	false
