model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	151
total timesteps	20
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	1	1	true
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
