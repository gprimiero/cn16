model parameters
nodes	40
skeptics	4
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	108
total timesteps	35
turtle that knows p	18
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	9	1	false
turtle that discovers not p (id,ranking,skeptic?)	19	1	false
