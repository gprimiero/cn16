model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	950
total timesteps	102
turtle that knows p	96
turtle that knows not p	205
turtle that discovers p (id,ranking,skeptic?)	227	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	1	0.09090909090909091	false
