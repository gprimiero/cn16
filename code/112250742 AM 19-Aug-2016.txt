model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	618
total timesteps	66
turtle that knows p	219
turtle that knows not p	82
turtle that discovers p (id,ranking,skeptic?)	28	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	0.16666666666666666	false
