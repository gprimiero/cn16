model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	249
total timesteps	54
turtle that knows p	164
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	101	1	true
turtle that discovers not p (id,ranking,skeptic?)	32	1	true
