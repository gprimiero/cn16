model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	608
total timesteps	87
turtle that knows p	160
turtle that knows not p	141
turtle that discovers p (id,ranking,skeptic?)	210	1	true
turtle that discovers not p (id,ranking,skeptic?)	275	1	false
model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	170
total timesteps	47
turtle that knows p	232
turtle that knows not p	69
turtle that discovers p (id,ranking,skeptic?)	38	1	false
turtle that discovers not p (id,ranking,skeptic?)	183	0.5	false
