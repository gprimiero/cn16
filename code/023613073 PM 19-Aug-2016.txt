model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	68
total timesteps	21
turtle that knows p	28
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	8	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	34	0.5	false
model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	134
total timesteps	25
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	21	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	39	1	false
