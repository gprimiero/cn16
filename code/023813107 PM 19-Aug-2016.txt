model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	35
total timesteps	13
turtle that knows p	9
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	25	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	14	1	true
