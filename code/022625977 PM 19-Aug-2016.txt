model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	57
total timesteps	32
turtle that knows p	48
turtle that knows not p	53
turtle that discovers p (id,ranking,skeptic?)	4	1	false
turtle that discovers not p (id,ranking,skeptic?)	75	0.5	false
