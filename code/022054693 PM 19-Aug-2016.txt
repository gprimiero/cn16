model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	252
total timesteps	53
turtle that knows p	30
turtle that knows not p	171
turtle that discovers p (id,ranking,skeptic?)	139	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	183	1	false
