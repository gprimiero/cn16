model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	214
total timesteps	61
turtle that knows p	100
turtle that knows not p	201
turtle that discovers p (id,ranking,skeptic?)	101	1	false
turtle that discovers not p (id,ranking,skeptic?)	43	0.16666666666666666	true
