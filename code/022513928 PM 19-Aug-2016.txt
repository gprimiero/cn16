model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	742
total timesteps	90
turtle that knows p	247
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	70	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	216	1	true
