model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1264
total timesteps	10
turtle that knows p	118
turtle that knows not p	182
turtle that discovers p (id,ranking,skeptic?)	140	0	false
turtle that discovers not p (id,ranking,skeptic?)	292	0	true
