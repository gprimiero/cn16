model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	194
total timesteps	21
turtle that knows p	12
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	11	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	22	1	false
