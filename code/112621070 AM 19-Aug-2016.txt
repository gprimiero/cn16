model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	483
total timesteps	43
turtle that knows p	106
turtle that knows not p	195
turtle that discovers p (id,ranking,skeptic?)	100	1	false
turtle that discovers not p (id,ranking,skeptic?)	97	1	true
