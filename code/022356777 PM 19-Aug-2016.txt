model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	335
total timesteps	60
turtle that knows p	16
turtle that knows not p	285
turtle that discovers p (id,ranking,skeptic?)	257	1	false
turtle that discovers not p (id,ranking,skeptic?)	300	1	true
