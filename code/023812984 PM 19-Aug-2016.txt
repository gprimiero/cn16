model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	61
total timesteps	21
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	4	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	1	true
