model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	445
total timesteps	60
turtle that knows p	160
turtle that knows not p	141
turtle that discovers p (id,ranking,skeptic?)	133	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	263	1	true
