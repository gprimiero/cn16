model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	488
total timesteps	94
turtle that knows p	60
turtle that knows not p	241
turtle that discovers p (id,ranking,skeptic?)	109	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	247	1	false
