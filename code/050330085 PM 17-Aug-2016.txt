model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	50
total timesteps	18
turtle that knows p	12
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	7	1	true
turtle that discovers not p (id,ranking,skeptic?)	47	1	true
