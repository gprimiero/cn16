model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	76
total timesteps	15
turtle that knows p	2
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	29	1	true
turtle that discovers not p (id,ranking,skeptic?)	5	1	true
