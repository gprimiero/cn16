model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	291
total timesteps	81
turtle that knows p	86
turtle that knows not p	215
turtle that discovers p (id,ranking,skeptic?)	274	1	false
turtle that discovers not p (id,ranking,skeptic?)	170	1	false
