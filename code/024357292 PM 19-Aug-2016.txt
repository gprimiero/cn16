model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	196
total timesteps	40
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	35	1	true
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
