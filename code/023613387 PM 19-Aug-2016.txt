model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	192
total timesteps	37
turtle that knows p	13
turtle that knows not p	38
turtle that discovers p (id,ranking,skeptic?)	20	1	true
turtle that discovers not p (id,ranking,skeptic?)	34	1	false
