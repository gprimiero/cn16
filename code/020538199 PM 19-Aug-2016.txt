model parameters
nodes	300
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	760
total timesteps	146
turtle that knows p	150
turtle that knows not p	151
turtle that discovers p (id,ranking,skeptic?)	1	0.015384615384615385	false
turtle that discovers not p (id,ranking,skeptic?)	257	1	false
