model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1343
total timesteps	10
turtle that knows p	155
turtle that knows not p	145
turtle that discovers p (id,ranking,skeptic?)	56	0	false
turtle that discovers not p (id,ranking,skeptic?)	101	0	true
