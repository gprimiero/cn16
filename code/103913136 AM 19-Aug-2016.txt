model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	450
total timesteps	95
turtle that knows p	285
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	121	1	false
turtle that discovers not p (id,ranking,skeptic?)	162	1	false
