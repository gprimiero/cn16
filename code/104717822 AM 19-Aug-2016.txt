model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	507
total timesteps	63
turtle that knows p	260
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	220	1	false
turtle that discovers not p (id,ranking,skeptic?)	227	1	false
