model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	489
total timesteps	73
turtle that knows p	153
turtle that knows not p	148
turtle that discovers p (id,ranking,skeptic?)	17	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	154	1	true
