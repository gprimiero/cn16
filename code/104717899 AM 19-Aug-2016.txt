model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1001
total timesteps	71
turtle that knows p	123
turtle that knows not p	178
turtle that discovers p (id,ranking,skeptic?)	216	1	true
turtle that discovers not p (id,ranking,skeptic?)	29	0.3333333333333333	true
