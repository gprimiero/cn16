model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	786
total timesteps	56
turtle that knows p	127
turtle that knows not p	74
turtle that discovers p (id,ranking,skeptic?)	100	1	true
turtle that discovers not p (id,ranking,skeptic?)	103	1	true
