model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	164
total timesteps	27
turtle that knows p	13
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	5	1	false
turtle that discovers not p (id,ranking,skeptic?)	4	1	false
