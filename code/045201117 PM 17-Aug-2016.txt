model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	121
total timesteps	24
turtle that knows p	44
turtle that knows not p	7
turtle that discovers p (id,ranking,skeptic?)	20	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	6	0.1111111111111111	true
