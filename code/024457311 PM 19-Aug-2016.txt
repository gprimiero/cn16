model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	123
total timesteps	22
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	10	1	true
turtle that discovers not p (id,ranking,skeptic?)	11	0.3333333333333333	true
