model parameters
nodes	300
skeptics	33
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	528
total timesteps	142
turtle that knows p	132
turtle that knows not p	169
turtle that discovers p (id,ranking,skeptic?)	53	1	false
turtle that discovers not p (id,ranking,skeptic?)	268	1	false
