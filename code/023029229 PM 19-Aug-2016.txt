model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	16
total timesteps	11
turtle that knows p	10
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	4	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	5	0.25	false
