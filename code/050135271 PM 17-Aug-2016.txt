model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	92
total timesteps	20
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	28	1	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.1	false
