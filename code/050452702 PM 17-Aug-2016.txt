model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	658
total timesteps	57
turtle that knows p	60
turtle that knows not p	241
turtle that discovers p (id,ranking,skeptic?)	105	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	174	0.5	false
model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	67
total timesteps	20
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	12	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	49	1	true
