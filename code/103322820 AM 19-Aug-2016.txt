model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	691
total timesteps	80
turtle that knows p	213
turtle that knows not p	88
turtle that discovers p (id,ranking,skeptic?)	167	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	184	1	false
