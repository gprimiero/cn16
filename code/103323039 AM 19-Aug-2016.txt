model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	369
total timesteps	75
turtle that knows p	216
turtle that knows not p	85
turtle that discovers p (id,ranking,skeptic?)	58	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	293	1	false
