model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	431
total timesteps	135
turtle that knows p	192
turtle that knows not p	108
turtle that discovers p (id,ranking,skeptic?)	24	164	false
turtle that discovers not p (id,ranking,skeptic?)	299	28	true
