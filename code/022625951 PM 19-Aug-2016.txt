model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	23
total timesteps	30
turtle that knows p	101
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	84	1	false
turtle that discovers not p (id,ranking,skeptic?)	58	1	false
model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	86
total timesteps	38
turtle that knows p	50
turtle that knows not p	51
turtle that discovers p (id,ranking,skeptic?)	76	1	false
turtle that discovers not p (id,ranking,skeptic?)	74	1	false
