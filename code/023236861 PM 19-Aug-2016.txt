model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	869
total timesteps	75
turtle that knows p	148
turtle that knows not p	153
turtle that discovers p (id,ranking,skeptic?)	24	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	292	1	false
