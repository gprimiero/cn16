model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	245
total timesteps	46
turtle that knows p	168
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	115	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	259	1	false
