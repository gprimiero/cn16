model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	72
total timesteps	27
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	29	1	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	708
total timesteps	72
turtle that knows p	148
turtle that knows not p	153
turtle that discovers p (id,ranking,skeptic?)	77	1	true
turtle that discovers not p (id,ranking,skeptic?)	247	1	true
