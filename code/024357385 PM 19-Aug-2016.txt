model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	71
total timesteps	17
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	24	1	false
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
