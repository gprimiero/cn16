model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	422
total timesteps	78
turtle that knows p	175
turtle that knows not p	126
turtle that discovers p (id,ranking,skeptic?)	200	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	228	1	true
