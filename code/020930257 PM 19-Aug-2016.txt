model parameters
nodes	300
skeptics	240
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2024
total timesteps	203
turtle that knows p	126
turtle that knows not p	175
turtle that discovers p (id,ranking,skeptic?)	25	0.125	false
turtle that discovers not p (id,ranking,skeptic?)	231	1	false
