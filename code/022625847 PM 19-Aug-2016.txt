model parameters
nodes	100
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	527
total timesteps	73
turtle that knows p	156
turtle that knows not p	145
turtle that discovers p (id,ranking,skeptic?)	178	1	false
turtle that discovers not p (id,ranking,skeptic?)	214	1	true
