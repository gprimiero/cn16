model parameters
nodes	300
skeptics	144
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	786
total timesteps	72
turtle that knows p	240
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	114	1	true
turtle that discovers not p (id,ranking,skeptic?)	54	0.3333333333333333	false
