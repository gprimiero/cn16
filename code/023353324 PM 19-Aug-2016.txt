model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	354
total timesteps	41
turtle that knows p	43
turtle that knows not p	158
turtle that discovers p (id,ranking,skeptic?)	93	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	185	1	false
