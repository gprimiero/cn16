model parameters
nodes	100
skeptics	54
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	342
total timesteps	73
turtle that knows p	67
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	84	1	true
turtle that discovers not p (id,ranking,skeptic?)	97	1	true
