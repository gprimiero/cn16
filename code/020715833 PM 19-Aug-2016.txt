model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	393
total timesteps	108
turtle that knows p	118
turtle that knows not p	183
turtle that discovers p (id,ranking,skeptic?)	247	1	false
turtle that discovers not p (id,ranking,skeptic?)	246	1	true
