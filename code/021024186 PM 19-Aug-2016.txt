model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	521
total timesteps	43
turtle that knows p	22
turtle that knows not p	279
turtle that discovers p (id,ranking,skeptic?)	259	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	1	true
