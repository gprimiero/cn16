model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	215
total timesteps	54
turtle that knows p	203
turtle that knows not p	98
turtle that discovers p (id,ranking,skeptic?)	138	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.045454545454545456	false
