model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	331
total timesteps	57
turtle that knows p	118
turtle that knows not p	183
turtle that discovers p (id,ranking,skeptic?)	267	1	true
turtle that discovers not p (id,ranking,skeptic?)	238	0.5	false
