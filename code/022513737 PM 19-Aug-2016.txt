model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	665
total timesteps	154
turtle that knows p	96
turtle that knows not p	205
turtle that discovers p (id,ranking,skeptic?)	161	1	true
turtle that discovers not p (id,ranking,skeptic?)	240	0.5	false
