model parameters
nodes	50
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	202
total timesteps	29
turtle that knows p	31
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	26	1	false
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	116
total timesteps	19
turtle that knows p	35
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	22	1	false
turtle that discovers not p (id,ranking,skeptic?)	33	1	true
