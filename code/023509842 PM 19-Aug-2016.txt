model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	639
total timesteps	48
turtle that knows p	80
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	27	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	98	1	true
