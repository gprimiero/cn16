model parameters
nodes	40
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	64
total timesteps	12
turtle that knows p	29
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	31	1	true
turtle that discovers not p (id,ranking,skeptic?)	30	1	true
