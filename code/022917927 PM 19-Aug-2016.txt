model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	152
total timesteps	50
turtle that knows p	22
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	2	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	32	1	false
