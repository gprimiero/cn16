model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1330
total timesteps	96
turtle that knows p	35
turtle that knows not p	266
turtle that discovers p (id,ranking,skeptic?)	186	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	61	0.5	true
