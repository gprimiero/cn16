model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	705
total timesteps	54
turtle that knows p	30
turtle that knows not p	271
turtle that discovers p (id,ranking,skeptic?)	174	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	17	0.25	false
