model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	57
total timesteps	18
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	29	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.2	true
