model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	67
total timesteps	20
turtle that knows p	17
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	6	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	17	0.3333333333333333	true
