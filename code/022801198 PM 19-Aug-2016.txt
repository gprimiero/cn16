model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	55
total timesteps	15
turtle that knows p	22
turtle that knows not p	29
turtle that discovers p (id,ranking,skeptic?)	40	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
