model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	355
total timesteps	105
turtle that knows p	164
turtle that knows not p	136
turtle that discovers p (id,ranking,skeptic?)	270	105	true
turtle that discovers not p (id,ranking,skeptic?)	201	226	true
