model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	777
total timesteps	9
turtle that knows p	95
turtle that knows not p	205
turtle that discovers p (id,ranking,skeptic?)	286	0	false
turtle that discovers not p (id,ranking,skeptic?)	258	0	true
