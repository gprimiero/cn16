model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	364
total timesteps	42
turtle that knows p	24
turtle that knows not p	277
turtle that discovers p (id,ranking,skeptic?)	17	0.07142857142857142	true
turtle that discovers not p (id,ranking,skeptic?)	174	1	true
