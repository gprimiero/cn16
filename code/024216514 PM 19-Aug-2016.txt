model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1045
total timesteps	70
turtle that knows p	20
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	47	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	42	0.2	true
