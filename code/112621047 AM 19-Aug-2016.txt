model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	588
total timesteps	89
turtle that knows p	249
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	259	1	false
turtle that discovers not p (id,ranking,skeptic?)	256	1	true
model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	600
total timesteps	77
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	59	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	287	1	true
