model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	94
total timesteps	20
turtle that knows p	48
turtle that knows not p	3
turtle that discovers p (id,ranking,skeptic?)	48	1	false
turtle that discovers not p (id,ranking,skeptic?)	38	1	true
