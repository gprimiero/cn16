model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1027
total timesteps	149
turtle that knows p	241
turtle that knows not p	60
turtle that discovers p (id,ranking,skeptic?)	270	1	false
turtle that discovers not p (id,ranking,skeptic?)	163	1	false
