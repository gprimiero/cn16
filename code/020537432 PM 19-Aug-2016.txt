model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	305
total timesteps	61
turtle that knows p	155
turtle that knows not p	146
turtle that discovers p (id,ranking,skeptic?)	116	1	false
turtle that discovers not p (id,ranking,skeptic?)	89	0.5	false
