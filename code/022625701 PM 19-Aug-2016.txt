model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	271
total timesteps	28
turtle that knows p	57
turtle that knows not p	44
turtle that discovers p (id,ranking,skeptic?)	96	1	false
turtle that discovers not p (id,ranking,skeptic?)	56	1	false
