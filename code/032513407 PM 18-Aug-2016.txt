model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	813
total timesteps	9
turtle that knows p	151
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	167	0	false
turtle that discovers not p (id,ranking,skeptic?)	55	0	true
