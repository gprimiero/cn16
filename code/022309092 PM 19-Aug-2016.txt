model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	390
total timesteps	50
turtle that knows p	201
turtle that knows not p	100
turtle that discovers p (id,ranking,skeptic?)	96	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	13	0.14285714285714285	false
