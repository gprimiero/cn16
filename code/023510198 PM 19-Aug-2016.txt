model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	409
total timesteps	64
turtle that knows p	15
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	38	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	45	1	false
