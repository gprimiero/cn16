model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	278
total timesteps	45
turtle that knows p	207
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	125	1	true
turtle that discovers not p (id,ranking,skeptic?)	124	0.5	true
