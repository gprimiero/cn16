model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	689
total timesteps	76
turtle that knows p	138
turtle that knows not p	163
turtle that discovers p (id,ranking,skeptic?)	281	1	false
turtle that discovers not p (id,ranking,skeptic?)	263	1	true
model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	453
total timesteps	58
turtle that knows p	290
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	122	1	true
turtle that discovers not p (id,ranking,skeptic?)	124	1	true
