model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	903
total timesteps	87
turtle that knows p	107
turtle that knows not p	194
turtle that discovers p (id,ranking,skeptic?)	211	1	true
turtle that discovers not p (id,ranking,skeptic?)	7	0.5	false
