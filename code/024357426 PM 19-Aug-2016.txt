model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	277
total timesteps	38
turtle that knows p	23
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	26	1	true
turtle that discovers not p (id,ranking,skeptic?)	36	1	true
