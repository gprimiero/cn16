model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	299
total timesteps	51
turtle that knows p	116
turtle that knows not p	185
turtle that discovers p (id,ranking,skeptic?)	216	1	false
turtle that discovers not p (id,ranking,skeptic?)	192	1	true
