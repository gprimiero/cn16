model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	127
total timesteps	39
turtle that knows p	21
turtle that knows not p	180
turtle that discovers p (id,ranking,skeptic?)	22	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	138	1	false
