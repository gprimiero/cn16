model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	629
total timesteps	85
turtle that knows p	78
turtle that knows not p	123
turtle that discovers p (id,ranking,skeptic?)	156	1	true
turtle that discovers not p (id,ranking,skeptic?)	24	1	true
