model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	756
total timesteps	85
turtle that knows p	145
turtle that knows not p	156
turtle that discovers p (id,ranking,skeptic?)	209	1	true
turtle that discovers not p (id,ranking,skeptic?)	43	1	true
