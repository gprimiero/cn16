model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	581
total timesteps	75
turtle that knows p	243
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	254	1	false
turtle that discovers not p (id,ranking,skeptic?)	283	1	false
