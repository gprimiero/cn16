model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	657
total timesteps	64
turtle that knows p	42
turtle that knows not p	259
turtle that discovers p (id,ranking,skeptic?)	163	1	false
turtle that discovers not p (id,ranking,skeptic?)	182	1	true
