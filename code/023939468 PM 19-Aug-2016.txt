model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1165
total timesteps	60
turtle that knows p	68
turtle that knows not p	233
turtle that discovers p (id,ranking,skeptic?)	127	1	true
turtle that discovers not p (id,ranking,skeptic?)	32	1	true
