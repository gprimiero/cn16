model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	867
total timesteps	63
turtle that knows p	270
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	156	1	true
turtle that discovers not p (id,ranking,skeptic?)	236	1	true
