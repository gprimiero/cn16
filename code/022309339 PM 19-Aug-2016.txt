model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1414
total timesteps	85
turtle that knows p	145
turtle that knows not p	156
turtle that discovers p (id,ranking,skeptic?)	77	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	35	0.25	true
