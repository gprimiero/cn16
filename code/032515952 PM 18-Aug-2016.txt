model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	747
total timesteps	9
turtle that knows p	185
turtle that knows not p	115
turtle that discovers p (id,ranking,skeptic?)	28	0	false
turtle that discovers not p (id,ranking,skeptic?)	140	0	true
