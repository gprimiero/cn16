model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	343
total timesteps	45
turtle that knows p	177
turtle that knows not p	124
turtle that discovers p (id,ranking,skeptic?)	24	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	20	1	true
