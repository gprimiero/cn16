model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	495
total timesteps	95
turtle that knows p	68
turtle that knows not p	233
turtle that discovers p (id,ranking,skeptic?)	112	1	false
turtle that discovers not p (id,ranking,skeptic?)	221	1	false
