model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	347
total timesteps	56
turtle that knows p	181
turtle that knows not p	120
turtle that discovers p (id,ranking,skeptic?)	281	1	false
turtle that discovers not p (id,ranking,skeptic?)	157	1	false
