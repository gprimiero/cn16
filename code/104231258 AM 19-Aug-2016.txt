model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	441
total timesteps	87
turtle that knows p	176
turtle that knows not p	125
turtle that discovers p (id,ranking,skeptic?)	257	1	false
turtle that discovers not p (id,ranking,skeptic?)	296	1	true
