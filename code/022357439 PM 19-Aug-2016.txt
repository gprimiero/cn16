model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	367
total timesteps	87
turtle that knows p	109
turtle that knows not p	192
turtle that discovers p (id,ranking,skeptic?)	140	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	170	1	false
