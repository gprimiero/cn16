model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	252
total timesteps	52
turtle that knows p	90
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	84	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	1	true
