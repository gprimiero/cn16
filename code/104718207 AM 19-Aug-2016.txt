model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	341
total timesteps	50
turtle that knows p	291
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	130	1	true
turtle that discovers not p (id,ranking,skeptic?)	280	1	true
