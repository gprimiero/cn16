model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	8
total timesteps	16
turtle that knows p	0
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	39	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	1	0.2	false
