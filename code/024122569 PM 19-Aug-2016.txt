model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	416
total timesteps	35
turtle that knows p	17
turtle that knows not p	184
turtle that discovers p (id,ranking,skeptic?)	154	1	false
turtle that discovers not p (id,ranking,skeptic?)	40	1	true
