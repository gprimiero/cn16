model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	316
total timesteps	65
turtle that knows p	64
turtle that knows not p	237
turtle that discovers p (id,ranking,skeptic?)	233	1	false
turtle that discovers not p (id,ranking,skeptic?)	55	0.5	false
