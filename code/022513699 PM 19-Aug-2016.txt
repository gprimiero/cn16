model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	344
total timesteps	75
turtle that knows p	252
turtle that knows not p	49
turtle that discovers p (id,ranking,skeptic?)	268	1	false
turtle that discovers not p (id,ranking,skeptic?)	252	1	true
