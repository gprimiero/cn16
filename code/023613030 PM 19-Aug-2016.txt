model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	186
total timesteps	23
turtle that knows p	10
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	44	1	true
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
