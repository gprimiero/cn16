model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	270
total timesteps	109
turtle that knows p	111
turtle that knows not p	189
turtle that discovers p (id,ranking,skeptic?)	209	269	false
turtle that discovers not p (id,ranking,skeptic?)	207	108	false
