model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	1130
total timesteps	128
turtle that knows p	100
turtle that knows not p	201
turtle that discovers p (id,ranking,skeptic?)	209	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	76	0.3333333333333333	false
