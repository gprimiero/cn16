model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	149
total timesteps	23
turtle that knows p	6
turtle that knows not p	45
turtle that discovers p (id,ranking,skeptic?)	22	1	true
turtle that discovers not p (id,ranking,skeptic?)	19	0.3333333333333333	true
