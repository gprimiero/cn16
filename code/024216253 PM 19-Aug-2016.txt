model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	360
total timesteps	51
turtle that knows p	92
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	59	1	true
turtle that discovers not p (id,ranking,skeptic?)	89	1	true
