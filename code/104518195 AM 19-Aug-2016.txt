model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	662
total timesteps	90
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	38	1	true
turtle that discovers not p (id,ranking,skeptic?)	69	0.16666666666666666	false
