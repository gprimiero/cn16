model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	748
total timesteps	9
turtle that knows p	110
turtle that knows not p	190
turtle that discovers p (id,ranking,skeptic?)	7	0	false
turtle that discovers not p (id,ranking,skeptic?)	57	0	true
