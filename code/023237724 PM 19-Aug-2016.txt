model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	663
total timesteps	72
turtle that knows p	179
turtle that knows not p	122
turtle that discovers p (id,ranking,skeptic?)	140	1	true
turtle that discovers not p (id,ranking,skeptic?)	65	0.3333333333333333	true
