model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	55
total timesteps	16
turtle that knows p	21
turtle that knows not p	33
turtle that discovers p (id,ranking,skeptic?)	29	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	11	0.5	true
