model parameters
nodes	180
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyNotP-skepticP

model output
total costs	411
total timesteps	58
turtle that knows p	123
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	8	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	112	0.5	false
