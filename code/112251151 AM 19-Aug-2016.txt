model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	376
total timesteps	57
turtle that knows p	284
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	30	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	249	1	false
