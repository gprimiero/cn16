model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	190
total timesteps	30
turtle that knows p	52
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	149	1	false
turtle that discovers not p (id,ranking,skeptic?)	23	0.5	false
