model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	435
total timesteps	45
turtle that knows p	64
turtle that knows not p	237
turtle that discovers p (id,ranking,skeptic?)	81	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	67	0.08333333333333333	true
