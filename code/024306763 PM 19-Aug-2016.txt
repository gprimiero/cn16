model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	366
total timesteps	38
turtle that knows p	13
turtle that knows not p	38
turtle that discovers p (id,ranking,skeptic?)	39	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	40	1	true
