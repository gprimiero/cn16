model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	227
total timesteps	32
turtle that knows p	107
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	194	1	false
turtle that discovers not p (id,ranking,skeptic?)	193	1	true
