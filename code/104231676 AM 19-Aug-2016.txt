model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	400
total timesteps	73
turtle that knows p	65
turtle that knows not p	236
turtle that discovers p (id,ranking,skeptic?)	66	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	242	1	true
