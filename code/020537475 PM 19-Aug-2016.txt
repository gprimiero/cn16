model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	360
total timesteps	64
turtle that knows p	196
turtle that knows not p	105
turtle that discovers p (id,ranking,skeptic?)	14	1	false
turtle that discovers not p (id,ranking,skeptic?)	173	1	false
