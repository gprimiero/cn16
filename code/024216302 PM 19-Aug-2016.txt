model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	764
total timesteps	47
turtle that knows p	47
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	97	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.3333333333333333	true
