model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1369
total timesteps	10
turtle that knows p	169
turtle that knows not p	131
turtle that discovers p (id,ranking,skeptic?)	255	0	false
turtle that discovers not p (id,ranking,skeptic?)	194	0	true
