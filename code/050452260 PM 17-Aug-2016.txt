model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	73
total timesteps	21
turtle that knows p	21
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	45	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.2	true
