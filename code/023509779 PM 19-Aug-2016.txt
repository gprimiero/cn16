model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	156
total timesteps	24
turtle that knows p	34
turtle that knows not p	67
turtle that discovers p (id,ranking,skeptic?)	22	1	false
turtle that discovers not p (id,ranking,skeptic?)	96	1	true
