model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	580
total timesteps	67
turtle that knows p	268
turtle that knows not p	33
turtle that discovers p (id,ranking,skeptic?)	29	0.16666666666666666	false
turtle that discovers not p (id,ranking,skeptic?)	195	1	false
