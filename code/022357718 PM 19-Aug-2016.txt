model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	305
total timesteps	53
turtle that knows p	249
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	295	1	false
turtle that discovers not p (id,ranking,skeptic?)	293	1	false
