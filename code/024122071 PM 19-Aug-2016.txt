model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	363
total timesteps	55
turtle that knows p	153
turtle that knows not p	48
turtle that discovers p (id,ranking,skeptic?)	95	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	154	1	false
