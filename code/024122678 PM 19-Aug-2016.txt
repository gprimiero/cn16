model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	711
total timesteps	80
turtle that knows p	43
turtle that knows not p	158
turtle that discovers p (id,ranking,skeptic?)	133	1	true
turtle that discovers not p (id,ranking,skeptic?)	191	1	false
