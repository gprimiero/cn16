model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	499
total timesteps	63
turtle that knows p	167
turtle that knows not p	134
turtle that discovers p (id,ranking,skeptic?)	60	1	false
turtle that discovers not p (id,ranking,skeptic?)	100	1	false
