model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1274
total timesteps	8
turtle that knows p	152
turtle that knows not p	148
turtle that discovers p (id,ranking,skeptic?)	264	0	false
turtle that discovers not p (id,ranking,skeptic?)	96	0	true
