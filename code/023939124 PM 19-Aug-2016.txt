model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	305
total timesteps	64
turtle that knows p	0
turtle that knows not p	301
turtle that discovers p (id,ranking,skeptic?)	33	1	true
turtle that discovers not p (id,ranking,skeptic?)	292	1	true
