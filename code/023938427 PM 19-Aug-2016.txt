model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	732
total timesteps	111
turtle that knows p	132
turtle that knows not p	169
turtle that discovers p (id,ranking,skeptic?)	10	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	127	1	true
