model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	681
total timesteps	69
turtle that knows p	259
turtle that knows not p	42
turtle that discovers p (id,ranking,skeptic?)	252	1	true
turtle that discovers not p (id,ranking,skeptic?)	206	1	true
