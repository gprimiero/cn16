model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	277
total timesteps	61
turtle that knows p	84
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	39	1	true
turtle that discovers not p (id,ranking,skeptic?)	73	0.5	false
