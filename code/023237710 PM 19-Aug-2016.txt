model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	293
total timesteps	45
turtle that knows p	87
turtle that knows not p	214
turtle that discovers p (id,ranking,skeptic?)	214	1	false
turtle that discovers not p (id,ranking,skeptic?)	236	0.5	false
