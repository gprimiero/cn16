model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	638
total timesteps	79
turtle that knows p	132
turtle that knows not p	169
turtle that discovers p (id,ranking,skeptic?)	229	1	true
turtle that discovers not p (id,ranking,skeptic?)	196	0.5	true
