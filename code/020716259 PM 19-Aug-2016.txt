model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	604
total timesteps	74
turtle that knows p	72
turtle that knows not p	229
turtle that discovers p (id,ranking,skeptic?)	262	1	false
turtle that discovers not p (id,ranking,skeptic?)	32	0.1111111111111111	false
