model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	109
total timesteps	18
turtle that knows p	19
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	10	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	124
total timesteps	24
turtle that knows p	15
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	20	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	36	1	true
