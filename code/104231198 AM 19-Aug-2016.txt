model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	519
total timesteps	78
turtle that knows p	75
turtle that knows not p	226
turtle that discovers p (id,ranking,skeptic?)	121	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	175	1	true
