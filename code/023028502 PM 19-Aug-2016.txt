model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	51
total timesteps	11
turtle that knows p	19
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	0	0.125	false
turtle that discovers not p (id,ranking,skeptic?)	25	1	false
