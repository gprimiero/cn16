model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	840
total timesteps	9
turtle that knows p	189
turtle that knows not p	111
turtle that discovers p (id,ranking,skeptic?)	195	0	false
turtle that discovers not p (id,ranking,skeptic?)	109	0	true
