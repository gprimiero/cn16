model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	77
total timesteps	18
turtle that knows p	31
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	37	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.1111111111111111	false
