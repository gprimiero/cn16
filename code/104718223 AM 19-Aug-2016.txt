model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	483
total timesteps	59
turtle that knows p	0
turtle that knows not p	301
turtle that discovers p (id,ranking,skeptic?)	208	1	true
turtle that discovers not p (id,ranking,skeptic?)	112	1	true
