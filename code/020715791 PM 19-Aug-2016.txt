model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	451
total timesteps	76
turtle that knows p	196
turtle that knows not p	105
turtle that discovers p (id,ranking,skeptic?)	201	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	143	0.5	true
