model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	427
total timesteps	52
turtle that knows p	124
turtle that knows not p	77
turtle that discovers p (id,ranking,skeptic?)	140	1	false
turtle that discovers not p (id,ranking,skeptic?)	134	1	false
