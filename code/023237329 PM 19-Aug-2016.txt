model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	486
total timesteps	65
turtle that knows p	134
turtle that knows not p	167
turtle that discovers p (id,ranking,skeptic?)	256	1	true
turtle that discovers not p (id,ranking,skeptic?)	161	1	false
