model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	151
total timesteps	36
turtle that knows p	12
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	10	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.5	true
