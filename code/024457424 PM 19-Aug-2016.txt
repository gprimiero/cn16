model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	141
total timesteps	22
turtle that knows p	4
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	18	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
