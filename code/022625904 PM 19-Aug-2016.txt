model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	180
total timesteps	35
turtle that knows p	74
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	24	1	false
turtle that discovers not p (id,ranking,skeptic?)	55	1	false
