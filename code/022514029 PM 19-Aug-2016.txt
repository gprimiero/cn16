model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	441
total timesteps	88
turtle that knows p	193
turtle that knows not p	108
turtle that discovers p (id,ranking,skeptic?)	96	1	false
turtle that discovers not p (id,ranking,skeptic?)	281	1	false
