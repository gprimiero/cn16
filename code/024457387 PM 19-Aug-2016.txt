model parameters
nodes	30
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	258
total timesteps	46
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	13	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.3333333333333333	true
