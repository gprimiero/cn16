model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	585
total timesteps	130
turtle that knows p	84
turtle that knows not p	217
turtle that discovers p (id,ranking,skeptic?)	49	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	25	1	true
