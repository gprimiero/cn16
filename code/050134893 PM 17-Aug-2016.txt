model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	97
total timesteps	21
turtle that knows p	12
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	39	1	true
turtle that discovers not p (id,ranking,skeptic?)	1	1	true
