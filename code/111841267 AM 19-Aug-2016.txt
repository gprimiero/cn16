model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	446
total timesteps	68
turtle that knows p	42
turtle that knows not p	259
turtle that discovers p (id,ranking,skeptic?)	210	1	true
turtle that discovers not p (id,ranking,skeptic?)	51	1	true
