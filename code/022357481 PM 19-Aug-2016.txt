model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1004
total timesteps	82
turtle that knows p	67
turtle that knows not p	234
turtle that discovers p (id,ranking,skeptic?)	2	0.1111111111111111	false
turtle that discovers not p (id,ranking,skeptic?)	87	0.5	false
