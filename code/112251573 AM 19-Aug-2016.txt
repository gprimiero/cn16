model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	963
total timesteps	97
turtle that knows p	171
turtle that knows not p	130
turtle that discovers p (id,ranking,skeptic?)	155	1	false
turtle that discovers not p (id,ranking,skeptic?)	208	1	false
