model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	85
total timesteps	18
turtle that knows p	5
turtle that knows not p	46
turtle that discovers p (id,ranking,skeptic?)	15	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
