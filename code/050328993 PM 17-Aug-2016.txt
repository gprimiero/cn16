model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	39
total timesteps	12
turtle that knows p	8
turtle that knows not p	43
turtle that discovers p (id,ranking,skeptic?)	37	1	true
turtle that discovers not p (id,ranking,skeptic?)	40	1	false
