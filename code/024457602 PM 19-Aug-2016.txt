model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	111
total timesteps	25
turtle that knows p	22
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	29	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.3333333333333333	true
