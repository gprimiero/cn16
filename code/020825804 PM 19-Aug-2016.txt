model parameters
nodes	300
skeptics	144
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	764
total timesteps	81
turtle that knows p	112
turtle that knows not p	189
turtle that discovers p (id,ranking,skeptic?)	62	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	0	0.16666666666666666	true
