model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	246
total timesteps	38
turtle that knows p	298
turtle that knows not p	3
turtle that discovers p (id,ranking,skeptic?)	1	0.07692307692307693	false
turtle that discovers not p (id,ranking,skeptic?)	177	1	false
