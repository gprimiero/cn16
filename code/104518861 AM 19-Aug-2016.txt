model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	340
total timesteps	47
turtle that knows p	155
turtle that knows not p	146
turtle that discovers p (id,ranking,skeptic?)	146	1	true
turtle that discovers not p (id,ranking,skeptic?)	95	0.3333333333333333	true
