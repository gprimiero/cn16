model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	303
total timesteps	55
turtle that knows p	74
turtle that knows not p	227
turtle that discovers p (id,ranking,skeptic?)	161	1	false
turtle that discovers not p (id,ranking,skeptic?)	45	0.3333333333333333	false
