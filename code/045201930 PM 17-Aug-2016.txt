model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	324
total timesteps	35
turtle that knows p	35
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	46	1	false
turtle that discovers not p (id,ranking,skeptic?)	10	0.5	false
