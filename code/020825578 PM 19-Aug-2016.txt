model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	488
total timesteps	67
turtle that knows p	248
turtle that knows not p	53
turtle that discovers p (id,ranking,skeptic?)	16	1	false
turtle that discovers not p (id,ranking,skeptic?)	88	0.25	false
