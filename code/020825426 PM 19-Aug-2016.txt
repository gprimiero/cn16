model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	789
total timesteps	91
turtle that knows p	216
turtle that knows not p	85
turtle that discovers p (id,ranking,skeptic?)	274	1	true
turtle that discovers not p (id,ranking,skeptic?)	276	1	true
