model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	649
total timesteps	66
turtle that knows p	41
turtle that knows not p	160
turtle that discovers p (id,ranking,skeptic?)	57	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	140	1	true
