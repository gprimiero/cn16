model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	874
total timesteps	79
turtle that knows p	130
turtle that knows not p	171
turtle that discovers p (id,ranking,skeptic?)	137	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	49	0.1	false
