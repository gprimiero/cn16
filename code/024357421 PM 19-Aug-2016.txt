model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	118
total timesteps	26
turtle that knows p	15
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	25	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	1	true
