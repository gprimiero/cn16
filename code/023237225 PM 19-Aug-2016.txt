model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	318
total timesteps	34
turtle that knows p	80
turtle that knows not p	221
turtle that discovers p (id,ranking,skeptic?)	184	1	true
turtle that discovers not p (id,ranking,skeptic?)	299	1	true
