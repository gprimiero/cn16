model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	289
total timesteps	33
turtle that knows p	89
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	20	1	false
turtle that discovers not p (id,ranking,skeptic?)	85	1	true
