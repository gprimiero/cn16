model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	95
total timesteps	21
turtle that knows p	11
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	19	1	false
turtle that discovers not p (id,ranking,skeptic?)	8	1	false
