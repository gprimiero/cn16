model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	555
total timesteps	71
turtle that knows p	247
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	111	1	true
turtle that discovers not p (id,ranking,skeptic?)	228	1	true
