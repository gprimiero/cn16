model parameters
nodes	100
skeptics	54
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	380
total timesteps	81
turtle that knows p	59
turtle that knows not p	42
turtle that discovers p (id,ranking,skeptic?)	81	1	false
turtle that discovers not p (id,ranking,skeptic?)	40	1	true
