model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	66
total timesteps	29
turtle that knows p	7
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	87	1	false
turtle that discovers not p (id,ranking,skeptic?)	93	1	false
