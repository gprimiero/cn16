model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	176
total timesteps	27
turtle that knows p	7
turtle that knows not p	44
turtle that discovers p (id,ranking,skeptic?)	31	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	10	0.5	false
