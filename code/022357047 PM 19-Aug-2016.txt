model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	499
total timesteps	63
turtle that knows p	82
turtle that knows not p	219
turtle that discovers p (id,ranking,skeptic?)	231	1	false
turtle that discovers not p (id,ranking,skeptic?)	217	1	false
