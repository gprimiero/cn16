model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	56
total timesteps	37
turtle that knows p	0
turtle that knows not p	101
turtle that discovers p (id,ranking,skeptic?)	25	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	44	1	false
model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	212
total timesteps	28
turtle that knows p	82
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	16	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	77	1	false
