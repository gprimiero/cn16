model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	87
total timesteps	31
turtle that knows p	31
turtle that knows not p	70
turtle that discovers p (id,ranking,skeptic?)	41	1	false
turtle that discovers not p (id,ranking,skeptic?)	38	1	false
