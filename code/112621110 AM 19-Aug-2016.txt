model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	800
total timesteps	59
turtle that knows p	158
turtle that knows not p	143
turtle that discovers p (id,ranking,skeptic?)	191	1	false
turtle that discovers not p (id,ranking,skeptic?)	85	1	true
