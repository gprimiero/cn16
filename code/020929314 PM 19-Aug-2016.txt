model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	430
total timesteps	44
turtle that knows p	289
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	106	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	226	1	false
