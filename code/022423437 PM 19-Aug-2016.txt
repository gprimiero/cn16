model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	275
total timesteps	86
turtle that knows p	114
turtle that knows not p	187
turtle that discovers p (id,ranking,skeptic?)	182	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	169	1	false
