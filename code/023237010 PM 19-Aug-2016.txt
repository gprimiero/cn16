model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	421
total timesteps	48
turtle that knows p	220
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	89	1	false
turtle that discovers not p (id,ranking,skeptic?)	281	1	false
