model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	196
total timesteps	45
turtle that knows p	10
turtle that knows not p	191
turtle that discovers p (id,ranking,skeptic?)	116	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	24	0.5	false
