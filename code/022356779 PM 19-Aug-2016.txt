model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	697
total timesteps	83
turtle that knows p	131
turtle that knows not p	170
turtle that discovers p (id,ranking,skeptic?)	224	1	false
turtle that discovers not p (id,ranking,skeptic?)	181	1	false
