model parameters
nodes	50
skeptics	3
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	51
total timesteps	21
turtle that knows p	9
turtle that knows not p	42
turtle that discovers p (id,ranking,skeptic?)	44	1	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.5	true
