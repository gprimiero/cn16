model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	833
total timesteps	9
turtle that knows p	141
turtle that knows not p	159
turtle that discovers p (id,ranking,skeptic?)	40	0	false
turtle that discovers not p (id,ranking,skeptic?)	146	0	true
