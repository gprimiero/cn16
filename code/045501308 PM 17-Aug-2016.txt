model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	955
total timesteps	175
turtle that knows p	192
turtle that knows not p	108
turtle that discovers p (id,ranking,skeptic?)	255	125	false
turtle that discovers not p (id,ranking,skeptic?)	11	62	true
