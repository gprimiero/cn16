model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	1338
total timesteps	10
turtle that knows p	132
turtle that knows not p	168
turtle that discovers p (id,ranking,skeptic?)	240	0	true
turtle that discovers not p (id,ranking,skeptic?)	206	0	true
