model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1285
total timesteps	10
turtle that knows p	155
turtle that knows not p	145
turtle that discovers p (id,ranking,skeptic?)	133	0	false
turtle that discovers not p (id,ranking,skeptic?)	101	0	true
