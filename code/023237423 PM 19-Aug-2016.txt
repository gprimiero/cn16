model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	371
total timesteps	61
turtle that knows p	181
turtle that knows not p	120
turtle that discovers p (id,ranking,skeptic?)	188	1	false
turtle that discovers not p (id,ranking,skeptic?)	258	1	true
