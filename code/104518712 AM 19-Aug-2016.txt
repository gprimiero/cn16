model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	494
total timesteps	77
turtle that knows p	119
turtle that knows not p	182
turtle that discovers p (id,ranking,skeptic?)	133	1	false
turtle that discovers not p (id,ranking,skeptic?)	183	1	false
