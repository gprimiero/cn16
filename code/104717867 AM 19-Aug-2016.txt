model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	483
total timesteps	51
turtle that knows p	84
turtle that knows not p	217
turtle that discovers p (id,ranking,skeptic?)	268	1	true
turtle that discovers not p (id,ranking,skeptic?)	42	0.3333333333333333	true
