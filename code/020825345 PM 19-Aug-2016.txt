model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	464
total timesteps	53
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	125	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	195	1	true
