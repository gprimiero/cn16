model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	160
total timesteps	29
turtle that knows p	71
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	34	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	100	1	true
