model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	849
total timesteps	9
turtle that knows p	148
turtle that knows not p	152
turtle that discovers p (id,ranking,skeptic?)	293	0	false
turtle that discovers not p (id,ranking,skeptic?)	228	0	true
