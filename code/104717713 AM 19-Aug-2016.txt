model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	907
total timesteps	124
turtle that knows p	197
turtle that knows not p	104
turtle that discovers p (id,ranking,skeptic?)	47	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	224	1	true
