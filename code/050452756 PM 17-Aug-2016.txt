model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	90
total timesteps	25
turtle that knows p	32
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	48	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	1	true
