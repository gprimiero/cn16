model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	857
total timesteps	60
turtle that knows p	131
turtle that knows not p	170
turtle that discovers p (id,ranking,skeptic?)	291	1	false
turtle that discovers not p (id,ranking,skeptic?)	172	1	false
