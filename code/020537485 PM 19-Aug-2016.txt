model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	758
total timesteps	73
turtle that knows p	157
turtle that knows not p	144
turtle that discovers p (id,ranking,skeptic?)	182	1	false
turtle that discovers not p (id,ranking,skeptic?)	237	1	false
