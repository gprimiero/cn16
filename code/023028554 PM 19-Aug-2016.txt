model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	41
total timesteps	13
turtle that knows p	0
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	15	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	30	1	false
