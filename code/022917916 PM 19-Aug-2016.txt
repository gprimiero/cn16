model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	47
total timesteps	21
turtle that knows p	29
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	31	1	false
turtle that discovers not p (id,ranking,skeptic?)	32	1	false
