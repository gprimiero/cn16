model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1173
total timesteps	124
turtle that knows p	166
turtle that knows not p	135
turtle that discovers p (id,ranking,skeptic?)	291	1	true
turtle that discovers not p (id,ranking,skeptic?)	228	1	true
