model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	126
total timesteps	43
turtle that knows p	134
turtle that knows not p	167
turtle that discovers p (id,ranking,skeptic?)	8	1	false
turtle that discovers not p (id,ranking,skeptic?)	10	0.0625	false
