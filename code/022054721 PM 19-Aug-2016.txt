model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	111
total timesteps	37
turtle that knows p	50
turtle that knows not p	151
turtle that discovers p (id,ranking,skeptic?)	35	1	false
turtle that discovers not p (id,ranking,skeptic?)	105	1	false
