model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	494
total timesteps	74
turtle that knows p	291
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	164	1	true
turtle that discovers not p (id,ranking,skeptic?)	281	1	true
