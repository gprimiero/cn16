model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	724
total timesteps	76
turtle that knows p	189
turtle that knows not p	112
turtle that discovers p (id,ranking,skeptic?)	57	1	false
turtle that discovers not p (id,ranking,skeptic?)	142	1	false
