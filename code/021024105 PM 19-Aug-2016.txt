model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	505
total timesteps	53
turtle that knows p	15
turtle that knows not p	286
turtle that discovers p (id,ranking,skeptic?)	166	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	290	1	true
