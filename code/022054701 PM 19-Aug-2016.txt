model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	214
total timesteps	44
turtle that knows p	113
turtle that knows not p	88
turtle that discovers p (id,ranking,skeptic?)	24	1	false
turtle that discovers not p (id,ranking,skeptic?)	46	1	true
