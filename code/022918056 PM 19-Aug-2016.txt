model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	25
total timesteps	15
turtle that knows p	14
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	37	1	true
turtle that discovers not p (id,ranking,skeptic?)	31	1	false
