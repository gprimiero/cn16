model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1385
total timesteps	73
turtle that knows p	36
turtle that knows not p	265
turtle that discovers p (id,ranking,skeptic?)	190	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	210	1	false
