model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	134
total timesteps	23
turtle that knows p	22
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	6	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
model parameters
nodes	30
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	277
total timesteps	40
turtle that knows p	10
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	12	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
