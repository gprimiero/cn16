model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	440
total timesteps	48
turtle that knows p	265
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	117	1	true
turtle that discovers not p (id,ranking,skeptic?)	146	0.5	true
