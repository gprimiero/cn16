model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	273
total timesteps	34
turtle that knows p	43
turtle that knows not p	8
turtle that discovers p (id,ranking,skeptic?)	46	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.2	true
