model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	338
total timesteps	59
turtle that knows p	86
turtle that knows not p	215
turtle that discovers p (id,ranking,skeptic?)	16	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	202	1	false
