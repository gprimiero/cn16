model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	286
total timesteps	49
turtle that knows p	6
turtle that knows not p	95
turtle that discovers p (id,ranking,skeptic?)	4	0.1	false
turtle that discovers not p (id,ranking,skeptic?)	8	0.3333333333333333	false
