model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	295
total timesteps	42
turtle that knows p	15
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	28	1	true
turtle that discovers not p (id,ranking,skeptic?)	23	1	true
