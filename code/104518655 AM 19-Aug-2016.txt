model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	485
total timesteps	86
turtle that knows p	261
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	266	1	true
turtle that discovers not p (id,ranking,skeptic?)	241	1	true
