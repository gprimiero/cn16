model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	224
total timesteps	53
turtle that knows p	2
turtle that knows not p	299
turtle that discovers p (id,ranking,skeptic?)	268	1	false
turtle that discovers not p (id,ranking,skeptic?)	45	1	false
