model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	700
total timesteps	77
turtle that knows p	106
turtle that knows not p	195
turtle that discovers p (id,ranking,skeptic?)	181	1	true
turtle that discovers not p (id,ranking,skeptic?)	230	1	true
