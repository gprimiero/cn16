model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	961
total timesteps	93
turtle that knows p	236
turtle that knows not p	65
turtle that discovers p (id,ranking,skeptic?)	26	1	true
turtle that discovers not p (id,ranking,skeptic?)	36	1	true
