model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	226
total timesteps	76
turtle that knows p	138
turtle that knows not p	163
turtle that discovers p (id,ranking,skeptic?)	207	1	false
turtle that discovers not p (id,ranking,skeptic?)	90	0.25	false
