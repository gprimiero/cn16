model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	108
total timesteps	23
turtle that knows p	24
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	41	1	true
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
