model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	114
total timesteps	26
turtle that knows p	8
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	14	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	11	0.5	true
