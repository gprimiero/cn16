model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	162
total timesteps	54
turtle that knows p	22
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	31	1	false
turtle that discovers not p (id,ranking,skeptic?)	36	0.5	false
