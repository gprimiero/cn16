model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	49
total timesteps	17
turtle that knows p	15
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	12	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	0.3333333333333333	false
