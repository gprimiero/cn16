model parameters
nodes	300
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	854
total timesteps	78
turtle that knows p	28
turtle that knows not p	273
turtle that discovers p (id,ranking,skeptic?)	232	1	false
turtle that discovers not p (id,ranking,skeptic?)	215	0.5	false
