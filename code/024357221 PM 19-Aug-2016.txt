model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	206
total timesteps	37
turtle that knows p	5
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	38	1	true
turtle that discovers not p (id,ranking,skeptic?)	5	0.3333333333333333	true
