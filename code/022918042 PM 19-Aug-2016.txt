model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	96
total timesteps	29
turtle that knows p	22
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	2	0.08333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	16	1	false
