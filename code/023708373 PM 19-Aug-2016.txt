model parameters
nodes	40
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	398
total timesteps	81
turtle that knows p	198
turtle that knows not p	103
turtle that discovers p (id,ranking,skeptic?)	154	1	true
turtle that discovers not p (id,ranking,skeptic?)	177	1	false
