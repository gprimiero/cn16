model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	396
total timesteps	54
turtle that knows p	183
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	35	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	5	0.125	true
