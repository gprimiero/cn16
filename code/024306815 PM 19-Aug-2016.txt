model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	95
total timesteps	18
turtle that knows p	12
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	3	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	11	1	true
