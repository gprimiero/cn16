model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	545
total timesteps	132
turtle that knows p	220
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	89	1	true
turtle that discovers not p (id,ranking,skeptic?)	244	1	false
