model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	575
total timesteps	89
turtle that knows p	39
turtle that knows not p	262
turtle that discovers p (id,ranking,skeptic?)	161	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	83	0.25	false
