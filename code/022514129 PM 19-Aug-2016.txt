model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	450
total timesteps	110
turtle that knows p	109
turtle that knows not p	192
turtle that discovers p (id,ranking,skeptic?)	179	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	298	1	false
