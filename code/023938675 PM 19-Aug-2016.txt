model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1378
total timesteps	137
turtle that knows p	155
turtle that knows not p	146
turtle that discovers p (id,ranking,skeptic?)	56	1	true
turtle that discovers not p (id,ranking,skeptic?)	46	0.5	true
