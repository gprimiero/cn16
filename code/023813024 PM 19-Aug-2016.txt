model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	77
total timesteps	21
turtle that knows p	10
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	4	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	1	false
