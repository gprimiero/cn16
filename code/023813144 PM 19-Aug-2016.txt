model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	39
total timesteps	13
turtle that knows p	17
turtle that knows not p	14
turtle that discovers p (id,ranking,skeptic?)	4	1	true
turtle that discovers not p (id,ranking,skeptic?)	2	0.1	false
