model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	458
total timesteps	67
turtle that knows p	82
turtle that knows not p	219
turtle that discovers p (id,ranking,skeptic?)	290	1	true
turtle that discovers not p (id,ranking,skeptic?)	86	0.3333333333333333	true
