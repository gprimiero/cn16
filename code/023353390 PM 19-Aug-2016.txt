model parameters
nodes	200
skeptics	104
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1032
total timesteps	98
turtle that knows p	52
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	189	1	true
turtle that discovers not p (id,ranking,skeptic?)	38	1	false
