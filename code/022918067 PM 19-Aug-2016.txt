model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	332
total timesteps	44
turtle that knows p	21
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	30	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
