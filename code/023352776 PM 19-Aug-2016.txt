model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	333
total timesteps	46
turtle that knows p	195
turtle that knows not p	6
turtle that discovers p (id,ranking,skeptic?)	10	0.125	true
turtle that discovers not p (id,ranking,skeptic?)	168	1	false
