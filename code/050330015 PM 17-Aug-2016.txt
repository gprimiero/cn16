model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	204
total timesteps	32
turtle that knows p	44
turtle that knows not p	7
turtle that discovers p (id,ranking,skeptic?)	33	1	false
turtle that discovers not p (id,ranking,skeptic?)	42	1	false
