model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	167
total timesteps	31
turtle that knows p	81
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	93	1	true
turtle that discovers not p (id,ranking,skeptic?)	33	0.5	true
