model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	65
total timesteps	20
turtle that knows p	21
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	28	1	false
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	true
