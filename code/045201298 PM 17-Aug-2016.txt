model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	107
total timesteps	24
turtle that knows p	27
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	12	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	27	1	false
