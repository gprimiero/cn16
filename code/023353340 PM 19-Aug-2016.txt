model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	334
total timesteps	40
turtle that knows p	167
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	46	1	true
turtle that discovers not p (id,ranking,skeptic?)	199	1	true
