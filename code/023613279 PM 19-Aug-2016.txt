model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	124
total timesteps	21
turtle that knows p	32
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	6	1	false
turtle that discovers not p (id,ranking,skeptic?)	8	0.2	true
