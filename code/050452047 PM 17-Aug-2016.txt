model parameters
nodes	300
skeptics	157
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	773
total timesteps	92
turtle that knows p	94
turtle that knows not p	207
turtle that discovers p (id,ranking,skeptic?)	195	1	true
turtle that discovers not p (id,ranking,skeptic?)	113	0.5	false
