model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	28
total timesteps	13
turtle that knows p	13
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	13	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	5	1	false
