model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	1068
total timesteps	85
turtle that knows p	224
turtle that knows not p	77
turtle that discovers p (id,ranking,skeptic?)	286	1	false
turtle that discovers not p (id,ranking,skeptic?)	208	1	true
model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	983
total timesteps	92
turtle that knows p	99
turtle that knows not p	202
turtle that discovers p (id,ranking,skeptic?)	132	1	false
turtle that discovers not p (id,ranking,skeptic?)	298	1	true
