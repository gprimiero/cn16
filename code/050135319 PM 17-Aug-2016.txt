model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	839
total timesteps	9
turtle that knows p	181
turtle that knows not p	119
turtle that discovers p (id,ranking,skeptic?)	229	0	false
turtle that discovers not p (id,ranking,skeptic?)	79	0	true
