model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	704
total timesteps	98
turtle that knows p	227
turtle that knows not p	74
turtle that discovers p (id,ranking,skeptic?)	63	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	224	1	true
