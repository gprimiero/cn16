model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	101
total timesteps	20
turtle that knows p	25
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	16	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
