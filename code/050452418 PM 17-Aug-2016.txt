model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	65
total timesteps	22
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	6	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	17	0.3333333333333333	true
model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	346
total timesteps	61
turtle that knows p	242
turtle that knows not p	59
turtle that discovers p (id,ranking,skeptic?)	3	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	252	1	true
