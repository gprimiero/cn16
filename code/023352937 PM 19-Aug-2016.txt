model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	433
total timesteps	44
turtle that knows p	184
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	84	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	27	0.25	false
