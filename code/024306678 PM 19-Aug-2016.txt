model parameters
nodes	50
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	195
total timesteps	32
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	45	1	true
turtle that discovers not p (id,ranking,skeptic?)	35	1	true
