model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1044
total timesteps	81
turtle that knows p	49
turtle that knows not p	252
turtle that discovers p (id,ranking,skeptic?)	214	1	true
turtle that discovers not p (id,ranking,skeptic?)	30	1	true
model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1092
total timesteps	106
turtle that knows p	174
turtle that knows not p	127
turtle that discovers p (id,ranking,skeptic?)	84	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	89	0.5	true
