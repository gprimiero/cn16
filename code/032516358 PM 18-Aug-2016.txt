model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	843
total timesteps	9
turtle that knows p	166
turtle that knows not p	134
turtle that discovers p (id,ranking,skeptic?)	48	0	false
turtle that discovers not p (id,ranking,skeptic?)	91	0	true
