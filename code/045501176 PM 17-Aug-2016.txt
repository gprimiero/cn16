model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1117
total timesteps	201
turtle that knows p	219
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	69	99	true
turtle that discovers not p (id,ranking,skeptic?)	44	60	true
