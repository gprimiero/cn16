model parameters
nodes	300
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	218
total timesteps	63
turtle that knows p	104
turtle that knows not p	197
turtle that discovers p (id,ranking,skeptic?)	269	1	false
turtle that discovers not p (id,ranking,skeptic?)	256	1	false
