model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	62
total timesteps	18
turtle that knows p	11
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	29	1	false
turtle that discovers not p (id,ranking,skeptic?)	1	0.2	false
