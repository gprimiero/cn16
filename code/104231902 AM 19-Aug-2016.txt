model parameters
nodes	300
skeptics	146
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1137
total timesteps	110
turtle that knows p	141
turtle that knows not p	160
turtle that discovers p (id,ranking,skeptic?)	70	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	51	0.16666666666666666	true
