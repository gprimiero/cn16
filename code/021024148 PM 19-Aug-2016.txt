model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	615
total timesteps	68
turtle that knows p	83
turtle that knows not p	218
turtle that discovers p (id,ranking,skeptic?)	103	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	162	1	true
