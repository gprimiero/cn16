model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	107
total timesteps	41
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	14	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	34	1	true
