model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	390
total timesteps	68
turtle that knows p	109
turtle that knows not p	192
turtle that discovers p (id,ranking,skeptic?)	4	0.030303030303030304	true
turtle that discovers not p (id,ranking,skeptic?)	195	1	true
