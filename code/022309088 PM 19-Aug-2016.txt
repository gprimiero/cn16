model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	404
total timesteps	99
turtle that knows p	99
turtle that knows not p	202
turtle that discovers p (id,ranking,skeptic?)	287	1	false
turtle that discovers not p (id,ranking,skeptic?)	129	0.3333333333333333	false
