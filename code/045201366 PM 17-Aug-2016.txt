model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	297
total timesteps	98
turtle that knows p	144
turtle that knows not p	156
turtle that discovers p (id,ranking,skeptic?)	227	214	false
turtle that discovers not p (id,ranking,skeptic?)	277	98	false
