model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	972
total timesteps	79
turtle that knows p	171
turtle that knows not p	130
turtle that discovers p (id,ranking,skeptic?)	91	1	true
turtle that discovers not p (id,ranking,skeptic?)	149	1	true
