model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	292
total timesteps	65
turtle that knows p	50
turtle that knows not p	251
turtle that discovers p (id,ranking,skeptic?)	191	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	41	1	false
