model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	175
total timesteps	29
turtle that knows p	31
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	34	1	true
turtle that discovers not p (id,ranking,skeptic?)	49	1	true
