model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	694
total timesteps	60
turtle that knows p	265
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	111	1	true
turtle that discovers not p (id,ranking,skeptic?)	278	1	true
