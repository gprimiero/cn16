model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	576
total timesteps	127
turtle that knows p	72
turtle that knows not p	229
turtle that discovers p (id,ranking,skeptic?)	202	1	false
turtle that discovers not p (id,ranking,skeptic?)	171	1	false
