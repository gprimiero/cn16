model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	205
total timesteps	36
turtle that knows p	41
turtle that knows not p	160
turtle that discovers p (id,ranking,skeptic?)	68	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.09090909090909091	true
