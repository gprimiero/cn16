model parameters
nodes	40
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	78
total timesteps	23
turtle that knows p	0
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	11	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	30	1	true
