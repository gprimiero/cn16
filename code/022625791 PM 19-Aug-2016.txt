model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	125
total timesteps	28
turtle that knows p	12
turtle that knows not p	89
turtle that discovers p (id,ranking,skeptic?)	80	1	false
turtle that discovers not p (id,ranking,skeptic?)	23	0.5	false
