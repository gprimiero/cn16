model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	769
total timesteps	9
turtle that knows p	110
turtle that knows not p	190
turtle that discovers p (id,ranking,skeptic?)	207	0	false
turtle that discovers not p (id,ranking,skeptic?)	212	0	true
