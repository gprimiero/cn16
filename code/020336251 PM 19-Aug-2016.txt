model parameters
nodes	300
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	268
total timesteps	60
turtle that knows p	87
turtle that knows not p	214
turtle that discovers p (id,ranking,skeptic?)	109	1	false
turtle that discovers not p (id,ranking,skeptic?)	68	0.3333333333333333	false
