model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1115
total timesteps	200
turtle that knows p	202
turtle that knows not p	98
turtle that discovers p (id,ranking,skeptic?)	190	200	false
turtle that discovers not p (id,ranking,skeptic?)	140	208	false
