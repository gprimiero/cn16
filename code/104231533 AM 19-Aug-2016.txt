model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1804
total timesteps	73
turtle that knows p	169
turtle that knows not p	132
turtle that discovers p (id,ranking,skeptic?)	120	1	false
turtle that discovers not p (id,ranking,skeptic?)	296	1	true
