model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	59
total timesteps	14
turtle that knows p	51
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	2	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	44	1	true
