model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	247
total timesteps	96
turtle that knows p	192
turtle that knows not p	109
turtle that discovers p (id,ranking,skeptic?)	11	0.06666666666666667	false
turtle that discovers not p (id,ranking,skeptic?)	257	0.5	false
