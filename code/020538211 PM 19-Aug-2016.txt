model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	289
total timesteps	100
turtle that knows p	85
turtle that knows not p	216
turtle that discovers p (id,ranking,skeptic?)	223	1	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.07142857142857142	false
