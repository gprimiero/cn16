model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	238
total timesteps	51
turtle that knows p	82
turtle that knows not p	219
turtle that discovers p (id,ranking,skeptic?)	287	1	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.25	true
