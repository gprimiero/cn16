model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	585
total timesteps	61
turtle that knows p	11
turtle that knows not p	290
turtle that discovers p (id,ranking,skeptic?)	212	1	false
turtle that discovers not p (id,ranking,skeptic?)	214	1	false
