model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	922
total timesteps	72
turtle that knows p	74
turtle that knows not p	127
turtle that discovers p (id,ranking,skeptic?)	192	1	true
turtle that discovers not p (id,ranking,skeptic?)	17	1	true
