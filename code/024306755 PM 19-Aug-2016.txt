model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	90
total timesteps	20
turtle that knows p	11
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	11	1	true
turtle that discovers not p (id,ranking,skeptic?)	4	0.5	true
