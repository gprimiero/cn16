model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	704
total timesteps	50
turtle that knows p	214
turtle that knows not p	87
turtle that discovers p (id,ranking,skeptic?)	60	1	false
turtle that discovers not p (id,ranking,skeptic?)	176	1	false
