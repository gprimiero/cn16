model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	447
total timesteps	68
turtle that knows p	176
turtle that knows not p	125
turtle that discovers p (id,ranking,skeptic?)	256	1	false
turtle that discovers not p (id,ranking,skeptic?)	288	1	true
