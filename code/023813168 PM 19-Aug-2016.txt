model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	34
total timesteps	15
turtle that knows p	8
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	10	1	true
turtle that discovers not p (id,ranking,skeptic?)	20	1	true
