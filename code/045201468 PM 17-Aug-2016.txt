model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	115
total timesteps	23
turtle that knows p	16
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	7	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
