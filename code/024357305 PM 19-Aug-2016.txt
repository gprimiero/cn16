model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	125
total timesteps	22
turtle that knows p	30
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	40	1	true
turtle that discovers not p (id,ranking,skeptic?)	16	0.25	false
model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	227
total timesteps	34
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	16	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	true
