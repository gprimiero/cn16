model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	201
total timesteps	45
turtle that knows p	284
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	270	1	false
turtle that discovers not p (id,ranking,skeptic?)	262	0.5	false
