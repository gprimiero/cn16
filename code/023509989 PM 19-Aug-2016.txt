model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	300
total timesteps	32
turtle that knows p	26
turtle that knows not p	75
turtle that discovers p (id,ranking,skeptic?)	0	0.09090909090909091	true
turtle that discovers not p (id,ranking,skeptic?)	74	1	false
