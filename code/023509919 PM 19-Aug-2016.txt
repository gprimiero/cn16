model parameters
nodes	100
skeptics	54
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	741
total timesteps	80
turtle that knows p	21
turtle that knows not p	80
turtle that discovers p (id,ranking,skeptic?)	7	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.16666666666666666	true
