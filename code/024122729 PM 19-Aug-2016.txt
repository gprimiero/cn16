model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	315
total timesteps	47
turtle that knows p	88
turtle that knows not p	113
turtle that discovers p (id,ranking,skeptic?)	124	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	198	1	true
