model parameters
nodes	30
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	220
total timesteps	49
turtle that knows p	20
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	20	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	0.5	true
