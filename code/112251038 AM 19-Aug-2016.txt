model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	502
total timesteps	49
turtle that knows p	35
turtle that knows not p	266
turtle that discovers p (id,ranking,skeptic?)	55	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	242	1	false
