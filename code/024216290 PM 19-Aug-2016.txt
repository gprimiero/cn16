model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	595
total timesteps	48
turtle that knows p	49
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	7	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	99	1	true
