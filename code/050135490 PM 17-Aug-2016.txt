model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	235
total timesteps	29
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	33	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
