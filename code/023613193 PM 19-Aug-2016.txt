model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	49
total timesteps	17
turtle that knows p	38
turtle that knows not p	13
turtle that discovers p (id,ranking,skeptic?)	15	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	37	0.3333333333333333	true
