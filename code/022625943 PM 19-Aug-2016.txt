model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	12
total timesteps	26
turtle that knows p	101
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	26	1	true
turtle that discovers not p (id,ranking,skeptic?)	70	1	false
