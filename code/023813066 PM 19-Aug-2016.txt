model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	51
total timesteps	14
turtle that knows p	14
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	25	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	7	1	false
