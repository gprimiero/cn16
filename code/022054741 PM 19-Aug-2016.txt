model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	390
total timesteps	70
turtle that knows p	123
turtle that knows not p	178
turtle that discovers p (id,ranking,skeptic?)	14	0.08333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	119	0.5	false
