model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	136
total timesteps	49
turtle that knows p	0
turtle that knows not p	201
turtle that discovers p (id,ranking,skeptic?)	164	1	true
turtle that discovers not p (id,ranking,skeptic?)	180	1	true
