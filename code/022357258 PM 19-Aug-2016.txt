model parameters
nodes	300
skeptics	33
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1255
total timesteps	99
turtle that knows p	189
turtle that knows not p	112
turtle that discovers p (id,ranking,skeptic?)	70	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	210	1	false
