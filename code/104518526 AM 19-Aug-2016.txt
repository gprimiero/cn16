model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	570
total timesteps	60
turtle that knows p	207
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	268	1	true
turtle that discovers not p (id,ranking,skeptic?)	47	0.125	true
