model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	563
total timesteps	54
turtle that knows p	239
turtle that knows not p	62
turtle that discovers p (id,ranking,skeptic?)	86	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	60	1	true
