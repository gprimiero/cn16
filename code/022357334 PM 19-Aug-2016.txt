model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	246
total timesteps	59
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	291	1	false
turtle that discovers not p (id,ranking,skeptic?)	93	1	false
