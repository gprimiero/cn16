model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1457
total timesteps	84
turtle that knows p	180
turtle that knows not p	121
turtle that discovers p (id,ranking,skeptic?)	236	1	true
turtle that discovers not p (id,ranking,skeptic?)	95	0.5	true
