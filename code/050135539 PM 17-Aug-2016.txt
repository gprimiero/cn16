model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	85
total timesteps	21
turtle that knows p	47
turtle that knows not p	4
turtle that discovers p (id,ranking,skeptic?)	23	1	false
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	829
total timesteps	9
turtle that knows p	164
turtle that knows not p	136
turtle that discovers p (id,ranking,skeptic?)	278	0	false
turtle that discovers not p (id,ranking,skeptic?)	264	0	true
