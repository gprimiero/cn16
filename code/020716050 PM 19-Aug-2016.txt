model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2082
total timesteps	110
turtle that knows p	242
turtle that knows not p	59
turtle that discovers p (id,ranking,skeptic?)	238	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	210	1	true
