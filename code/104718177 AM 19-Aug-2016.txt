model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	451
total timesteps	48
turtle that knows p	282
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	249	1	true
turtle that discovers not p (id,ranking,skeptic?)	280	1	true
