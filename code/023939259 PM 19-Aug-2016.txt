model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	687
total timesteps	76
turtle that knows p	45
turtle that knows not p	256
turtle that discovers p (id,ranking,skeptic?)	145	1	false
turtle that discovers not p (id,ranking,skeptic?)	287	1	true
