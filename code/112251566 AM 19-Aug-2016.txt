model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	296
total timesteps	61
turtle that knows p	220
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	86	1	false
turtle that discovers not p (id,ranking,skeptic?)	113	1	false
