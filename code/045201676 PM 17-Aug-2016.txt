model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	44
total timesteps	14
turtle that knows p	11
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	37	1	true
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
