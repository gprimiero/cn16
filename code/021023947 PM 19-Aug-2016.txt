model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	457
total timesteps	55
turtle that knows p	60
turtle that knows not p	241
turtle that discovers p (id,ranking,skeptic?)	54	1	true
turtle that discovers not p (id,ranking,skeptic?)	286	1	true
