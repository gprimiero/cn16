model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1085
total timesteps	10
turtle that knows p	109
turtle that knows not p	191
turtle that discovers p (id,ranking,skeptic?)	176	0	false
turtle that discovers not p (id,ranking,skeptic?)	82	0	true
