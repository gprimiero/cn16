model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	488
total timesteps	82
turtle that knows p	64
turtle that knows not p	237
turtle that discovers p (id,ranking,skeptic?)	96	1	false
turtle that discovers not p (id,ranking,skeptic?)	36	0.5	true
