model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	688
total timesteps	63
turtle that knows p	183
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	21	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	189	1	true
