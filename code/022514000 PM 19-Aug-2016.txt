model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	537
total timesteps	109
turtle that knows p	194
turtle that knows not p	107
turtle that discovers p (id,ranking,skeptic?)	274	1	false
turtle that discovers not p (id,ranking,skeptic?)	163	0.25	false
model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1034
total timesteps	119
turtle that knows p	241
turtle that knows not p	60
turtle that discovers p (id,ranking,skeptic?)	270	1	false
turtle that discovers not p (id,ranking,skeptic?)	198	1	true
