model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	511
total timesteps	70
turtle that knows p	85
turtle that knows not p	116
turtle that discovers p (id,ranking,skeptic?)	23	1	false
turtle that discovers not p (id,ranking,skeptic?)	156	1	false
