model parameters
nodes	100
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	227
total timesteps	42
turtle that knows p	47
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	59	1	false
turtle that discovers not p (id,ranking,skeptic?)	60	1	false
