model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	593
total timesteps	45
turtle that knows p	80
turtle that knows not p	221
turtle that discovers p (id,ranking,skeptic?)	152	1	true
turtle that discovers not p (id,ranking,skeptic?)	200	1	true
