model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	740
total timesteps	9
turtle that knows p	90
turtle that knows not p	210
turtle that discovers p (id,ranking,skeptic?)	214	0	false
turtle that discovers not p (id,ranking,skeptic?)	0	0	true
