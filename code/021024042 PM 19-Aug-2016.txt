model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	763
total timesteps	70
turtle that knows p	124
turtle that knows not p	177
turtle that discovers p (id,ranking,skeptic?)	245	1	true
turtle that discovers not p (id,ranking,skeptic?)	77	1	true
