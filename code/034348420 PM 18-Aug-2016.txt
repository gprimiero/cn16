model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	813
total timesteps	9
turtle that knows p	173
turtle that knows not p	127
turtle that discovers p (id,ranking,skeptic?)	125	0	false
turtle that discovers not p (id,ranking,skeptic?)	117	0	true
