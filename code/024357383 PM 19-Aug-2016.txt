model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	126
total timesteps	23
turtle that knows p	22
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	8	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
