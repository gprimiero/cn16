model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	40
total timesteps	13
turtle that knows p	22
turtle that knows not p	29
turtle that discovers p (id,ranking,skeptic?)	43	1	true
turtle that discovers not p (id,ranking,skeptic?)	1	1	true
