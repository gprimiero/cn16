model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	776
total timesteps	9
turtle that knows p	194
turtle that knows not p	106
turtle that discovers p (id,ranking,skeptic?)	255	0	false
turtle that discovers not p (id,ranking,skeptic?)	107	0	true
