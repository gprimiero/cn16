model parameters
nodes	300
skeptics	146
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	557
total timesteps	72
turtle that knows p	230
turtle that knows not p	71
turtle that discovers p (id,ranking,skeptic?)	150	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	213	1	false
