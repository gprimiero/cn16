model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	275
total timesteps	50
turtle that knows p	184
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	7	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	159	1	false
