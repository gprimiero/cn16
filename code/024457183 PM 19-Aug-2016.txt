model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	73
total timesteps	18
turtle that knows p	10
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	19	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	25	1	true
