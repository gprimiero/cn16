model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	102
total timesteps	24
turtle that knows p	16
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	19	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
