model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	450
total timesteps	53
turtle that knows p	212
turtle that knows not p	89
turtle that discovers p (id,ranking,skeptic?)	228	1	false
turtle that discovers not p (id,ranking,skeptic?)	297	1	true
