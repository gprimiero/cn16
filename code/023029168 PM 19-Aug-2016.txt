model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	38
total timesteps	16
turtle that knows p	14
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	28	1	false
turtle that discovers not p (id,ranking,skeptic?)	27	1	false
model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	109
total timesteps	26
turtle that knows p	11
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	15	1	false
turtle that discovers not p (id,ranking,skeptic?)	1	0.1111111111111111	false
