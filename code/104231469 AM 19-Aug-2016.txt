model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	651
total timesteps	75
turtle that knows p	160
turtle that knows not p	141
turtle that discovers p (id,ranking,skeptic?)	99	1	false
turtle that discovers not p (id,ranking,skeptic?)	206	1	true
