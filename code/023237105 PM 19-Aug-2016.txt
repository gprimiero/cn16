model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1110
total timesteps	111
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	121	1	false
turtle that discovers not p (id,ranking,skeptic?)	111	1	true
