model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	107
total timesteps	19
turtle that knows p	1
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	6	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	38	1	true
model parameters
nodes	40
skeptics	18
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	56
total timesteps	15
turtle that knows p	19
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	8	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	0	0.14285714285714285	true
