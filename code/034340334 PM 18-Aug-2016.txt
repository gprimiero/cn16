model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1304
total timesteps	10
turtle that knows p	116
turtle that knows not p	184
turtle that discovers p (id,ranking,skeptic?)	72	0	false
turtle that discovers not p (id,ranking,skeptic?)	122	0	true
