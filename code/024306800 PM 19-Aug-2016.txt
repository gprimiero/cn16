model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	207
total timesteps	29
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	29	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	25	1	true
