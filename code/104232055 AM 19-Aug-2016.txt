model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1051
total timesteps	98
turtle that knows p	176
turtle that knows not p	125
turtle that discovers p (id,ranking,skeptic?)	264	1	true
turtle that discovers not p (id,ranking,skeptic?)	44	0.2	false
