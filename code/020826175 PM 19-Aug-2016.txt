model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	651
total timesteps	98
turtle that knows p	143
turtle that knows not p	158
turtle that discovers p (id,ranking,skeptic?)	50	1	false
turtle that discovers not p (id,ranking,skeptic?)	118	1	true
