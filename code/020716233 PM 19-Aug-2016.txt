model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	213
total timesteps	41
turtle that knows p	192
turtle that knows not p	109
turtle that discovers p (id,ranking,skeptic?)	8	0.1111111111111111	false
turtle that discovers not p (id,ranking,skeptic?)	53	0.3333333333333333	true
