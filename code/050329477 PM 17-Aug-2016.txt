model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	106
total timesteps	22
turtle that knows p	17
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	43	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	true
