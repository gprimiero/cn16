model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	124
total timesteps	29
turtle that knows p	43
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	36	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	74	1	false
