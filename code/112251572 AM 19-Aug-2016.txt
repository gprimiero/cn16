model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	598
total timesteps	91
turtle that knows p	75
turtle that knows not p	226
turtle that discovers p (id,ranking,skeptic?)	294	1	false
turtle that discovers not p (id,ranking,skeptic?)	265	1	false
