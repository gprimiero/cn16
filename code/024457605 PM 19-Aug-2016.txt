model parameters
nodes	30
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	657
total timesteps	77
turtle that knows p	51
turtle that knows not p	250
turtle that discovers p (id,ranking,skeptic?)	139	1	true
turtle that discovers not p (id,ranking,skeptic?)	38	1	true
