model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	339
total timesteps	54
turtle that knows p	177
turtle that knows not p	124
turtle that discovers p (id,ranking,skeptic?)	40	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	57	1	false
