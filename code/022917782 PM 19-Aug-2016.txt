model parameters
nodes	40
skeptics	4
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	136
total timesteps	43
turtle that knows p	15
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	2	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	31	0.5	false
