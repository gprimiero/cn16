model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	346
total timesteps	60
turtle that knows p	184
turtle that knows not p	117
turtle that discovers p (id,ranking,skeptic?)	52	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	216	1	true
