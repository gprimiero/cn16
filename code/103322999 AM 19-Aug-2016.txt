model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	310
total timesteps	72
turtle that knows p	257
turtle that knows not p	44
turtle that discovers p (id,ranking,skeptic?)	210	1	false
turtle that discovers not p (id,ranking,skeptic?)	182	1	false
