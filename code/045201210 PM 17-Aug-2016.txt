model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	120
total timesteps	25
turtle that knows p	23
turtle that knows not p	28
turtle that discovers p (id,ranking,skeptic?)	38	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
