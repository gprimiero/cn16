model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	217
total timesteps	41
turtle that knows p	22
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	29	1	true
turtle that discovers not p (id,ranking,skeptic?)	4	0.3333333333333333	true
