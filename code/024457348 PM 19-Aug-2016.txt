model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	87
total timesteps	19
turtle that knows p	23
turtle that knows not p	8
turtle that discovers p (id,ranking,skeptic?)	25	1	true
turtle that discovers not p (id,ranking,skeptic?)	4	1	true
