model parameters
nodes	300
skeptics	146
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	272
total timesteps	53
turtle that knows p	185
turtle that knows not p	116
turtle that discovers p (id,ranking,skeptic?)	78	1	true
turtle that discovers not p (id,ranking,skeptic?)	103	1	true
