model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	500
total timesteps	74
turtle that knows p	188
turtle that knows not p	113
turtle that discovers p (id,ranking,skeptic?)	66	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	180	1	false
