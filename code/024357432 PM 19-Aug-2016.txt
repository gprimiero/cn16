model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	54
total timesteps	13
turtle that knows p	23
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	13	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
