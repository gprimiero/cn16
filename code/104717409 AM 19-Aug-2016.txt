model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	286
total timesteps	58
turtle that knows p	56
turtle that knows not p	245
turtle that discovers p (id,ranking,skeptic?)	95	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	202	0.5	true
