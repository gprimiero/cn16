model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1462
total timesteps	159
turtle that knows p	123
turtle that knows not p	178
turtle that discovers p (id,ranking,skeptic?)	239	1	false
turtle that discovers not p (id,ranking,skeptic?)	140	0.5	true
