model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	648
total timesteps	81
turtle that knows p	31
turtle that knows not p	270
turtle that discovers p (id,ranking,skeptic?)	124	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	153	1	true
