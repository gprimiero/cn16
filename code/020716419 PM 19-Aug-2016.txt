model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	617
total timesteps	49
turtle that knows p	273
turtle that knows not p	28
turtle that discovers p (id,ranking,skeptic?)	91	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	36	1	false
