model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	525
total timesteps	54
turtle that knows p	224
turtle that knows not p	77
turtle that discovers p (id,ranking,skeptic?)	76	1	true
turtle that discovers not p (id,ranking,skeptic?)	171	1	true
