model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	355
total timesteps	57
turtle that knows p	235
turtle that knows not p	66
turtle that discovers p (id,ranking,skeptic?)	147	1	false
turtle that discovers not p (id,ranking,skeptic?)	3	0.16666666666666666	true
