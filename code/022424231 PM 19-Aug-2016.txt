model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	163
total timesteps	46
turtle that knows p	59
turtle that knows not p	242
turtle that discovers p (id,ranking,skeptic?)	65	1	true
turtle that discovers not p (id,ranking,skeptic?)	45	1	false
