model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	443
total timesteps	74
turtle that knows p	71
turtle that knows not p	230
turtle that discovers p (id,ranking,skeptic?)	127	1	false
turtle that discovers not p (id,ranking,skeptic?)	114	0.5	false
