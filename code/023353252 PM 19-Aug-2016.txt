model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	428
total timesteps	70
turtle that knows p	104
turtle that knows not p	197
turtle that discovers p (id,ranking,skeptic?)	110	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	82	1	true
