model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1208
total timesteps	10
turtle that knows p	194
turtle that knows not p	106
turtle that discovers p (id,ranking,skeptic?)	161	0	false
turtle that discovers not p (id,ranking,skeptic?)	298	0	true
