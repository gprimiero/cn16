model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	77
total timesteps	23
turtle that knows p	30
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	44	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	1	true
