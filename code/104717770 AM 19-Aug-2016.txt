model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	573
total timesteps	64
turtle that knows p	76
turtle that knows not p	225
turtle that discovers p (id,ranking,skeptic?)	32	1	true
turtle that discovers not p (id,ranking,skeptic?)	180	0.5	true
