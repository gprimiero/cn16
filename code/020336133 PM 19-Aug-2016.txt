model parameters
nodes	300
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	151
total timesteps	40
turtle that knows p	218
turtle that knows not p	83
turtle that discovers p (id,ranking,skeptic?)	194	1	false
turtle that discovers not p (id,ranking,skeptic?)	222	1	false
