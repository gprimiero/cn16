model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	586
total timesteps	68
turtle that knows p	87
turtle that knows not p	214
turtle that discovers p (id,ranking,skeptic?)	207	1	true
turtle that discovers not p (id,ranking,skeptic?)	111	1	true
