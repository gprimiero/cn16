model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	297
total timesteps	49
turtle that knows p	11
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	31	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	true
