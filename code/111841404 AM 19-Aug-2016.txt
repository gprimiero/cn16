model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	426
total timesteps	72
turtle that knows p	70
turtle that knows not p	231
turtle that discovers p (id,ranking,skeptic?)	215	1	true
turtle that discovers not p (id,ranking,skeptic?)	263	1	true
model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	1061
total timesteps	132
turtle that knows p	116
turtle that knows not p	185
turtle that discovers p (id,ranking,skeptic?)	221	1	true
turtle that discovers not p (id,ranking,skeptic?)	282	1	true
