model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	101
total timesteps	32
turtle that knows p	21
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	23	1	false
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
