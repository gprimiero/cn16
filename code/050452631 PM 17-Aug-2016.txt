model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	752
total timesteps	69
turtle that knows p	12
turtle that knows not p	289
turtle that discovers p (id,ranking,skeptic?)	22	0.125	false
turtle that discovers not p (id,ranking,skeptic?)	169	0.5	false
