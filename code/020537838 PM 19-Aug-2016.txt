model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	103
total timesteps	45
turtle that knows p	160
turtle that knows not p	141
turtle that discovers p (id,ranking,skeptic?)	47	1	false
turtle that discovers not p (id,ranking,skeptic?)	199	1	false
