model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	711
total timesteps	67
turtle that knows p	223
turtle that knows not p	78
turtle that discovers p (id,ranking,skeptic?)	16	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	108	0.5	true
