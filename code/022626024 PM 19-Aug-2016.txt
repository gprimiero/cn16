model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	128
total timesteps	43
turtle that knows p	92
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	90	1	false
turtle that discovers not p (id,ranking,skeptic?)	52	1	false
