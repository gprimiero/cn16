model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	57
total timesteps	16
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	0	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	102
total timesteps	24
turtle that knows p	4
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	5	1	false
turtle that discovers not p (id,ranking,skeptic?)	10	0.5	false
