model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	808
total timesteps	126
turtle that knows p	105
turtle that knows not p	196
turtle that discovers p (id,ranking,skeptic?)	181	1	false
turtle that discovers not p (id,ranking,skeptic?)	195	1	false
