model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	801
total timesteps	9
turtle that knows p	122
turtle that knows not p	178
turtle that discovers p (id,ranking,skeptic?)	34	0	false
turtle that discovers not p (id,ranking,skeptic?)	46	0	true
