model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	484
total timesteps	60
turtle that knows p	156
turtle that knows not p	145
turtle that discovers p (id,ranking,skeptic?)	142	1	false
turtle that discovers not p (id,ranking,skeptic?)	63	0.5	false
