model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	227
total timesteps	41
turtle that knows p	45
turtle that knows not p	56
turtle that discovers p (id,ranking,skeptic?)	2	0.058823529411764705	false
turtle that discovers not p (id,ranking,skeptic?)	75	0.25	false
