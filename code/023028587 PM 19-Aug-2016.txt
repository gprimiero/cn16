model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	14
total timesteps	12
turtle that knows p	9
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	23	1	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	false
