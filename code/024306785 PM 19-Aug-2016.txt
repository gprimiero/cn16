model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	139
total timesteps	21
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	25	1	true
turtle that discovers not p (id,ranking,skeptic?)	20	1	false
