model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	75
total timesteps	16
turtle that knows p	41
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	19	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	46	1	false
