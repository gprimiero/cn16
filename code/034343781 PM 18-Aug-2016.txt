model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1252
total timesteps	10
turtle that knows p	124
turtle that knows not p	176
turtle that discovers p (id,ranking,skeptic?)	251	0	false
turtle that discovers not p (id,ranking,skeptic?)	32	0	true
