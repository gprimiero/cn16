model parameters
nodes	50
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1613
total timesteps	92
turtle that knows p	168
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	194	1	true
turtle that discovers not p (id,ranking,skeptic?)	216	1	false
