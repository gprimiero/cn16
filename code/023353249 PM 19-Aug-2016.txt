model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	161
total timesteps	33
turtle that knows p	138
turtle that knows not p	63
turtle that discovers p (id,ranking,skeptic?)	20	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	114	1	true
