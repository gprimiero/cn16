model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	41
total timesteps	20
turtle that knows p	28
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	43	1	true
turtle that discovers not p (id,ranking,skeptic?)	23	1	false
