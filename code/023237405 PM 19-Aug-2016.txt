model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	477
total timesteps	71
turtle that knows p	210
turtle that knows not p	91
turtle that discovers p (id,ranking,skeptic?)	300	1	true
turtle that discovers not p (id,ranking,skeptic?)	187	1	true
