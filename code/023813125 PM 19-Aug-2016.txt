model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	59
total timesteps	16
turtle that knows p	5
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	16	1	true
turtle that discovers not p (id,ranking,skeptic?)	5	1	false
