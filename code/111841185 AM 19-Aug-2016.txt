model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	702
total timesteps	54
turtle that knows p	225
turtle that knows not p	76
turtle that discovers p (id,ranking,skeptic?)	74	1	true
turtle that discovers not p (id,ranking,skeptic?)	266	1	true
