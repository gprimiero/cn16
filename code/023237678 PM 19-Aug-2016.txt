model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	445
total timesteps	50
turtle that knows p	258
turtle that knows not p	43
turtle that discovers p (id,ranking,skeptic?)	165	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	127	1	false
