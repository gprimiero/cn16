model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1021
total timesteps	77
turtle that knows p	177
turtle that knows not p	124
turtle that discovers p (id,ranking,skeptic?)	264	1	true
turtle that discovers not p (id,ranking,skeptic?)	76	1	true
