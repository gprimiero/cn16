model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	113
total timesteps	21
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	40	1	false
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	true
