model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	416
total timesteps	63
turtle that knows p	62
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	24	1	true
turtle that discovers not p (id,ranking,skeptic?)	19	0.5	true
