model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	138
total timesteps	24
turtle that knows p	30
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	13	1	true
turtle that discovers not p (id,ranking,skeptic?)	39	1	true
