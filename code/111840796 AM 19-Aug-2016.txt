model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	662
total timesteps	79
turtle that knows p	134
turtle that knows not p	167
turtle that discovers p (id,ranking,skeptic?)	76	1	true
turtle that discovers not p (id,ranking,skeptic?)	132	0.5	true
