model parameters
nodes	200
skeptics	104
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1124
total timesteps	101
turtle that knows p	51
turtle that knows not p	150
turtle that discovers p (id,ranking,skeptic?)	180	1	true
turtle that discovers not p (id,ranking,skeptic?)	147	1	true
