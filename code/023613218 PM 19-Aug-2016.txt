model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	80
total timesteps	29
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	11	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	50	1	true
