model parameters
nodes	300
skeptics	164
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	841
total timesteps	82
turtle that knows p	240
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	282	1	true
turtle that discovers not p (id,ranking,skeptic?)	39	0.5	true
