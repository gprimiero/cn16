model parameters
nodes	300
skeptics	244
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1120
total timesteps	72
turtle that knows p	178
turtle that knows not p	123
turtle that discovers p (id,ranking,skeptic?)	229	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.045454545454545456	true
