model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	60
total timesteps	17
turtle that knows p	29
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	8	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
