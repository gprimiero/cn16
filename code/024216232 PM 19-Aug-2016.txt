model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	915
total timesteps	60
turtle that knows p	52
turtle that knows not p	49
turtle that discovers p (id,ranking,skeptic?)	22	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	91	1	true
