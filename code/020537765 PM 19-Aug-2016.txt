model parameters
nodes	300
skeptics	32
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	404
total timesteps	74
turtle that knows p	133
turtle that knows not p	168
turtle that discovers p (id,ranking,skeptic?)	100	1	false
turtle that discovers not p (id,ranking,skeptic?)	158	1	false
model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	412
total timesteps	107
turtle that knows p	84
turtle that knows not p	217
turtle that discovers p (id,ranking,skeptic?)	15	0.045454545454545456	false
turtle that discovers not p (id,ranking,skeptic?)	149	1	false
