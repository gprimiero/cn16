model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	899
total timesteps	91
turtle that knows p	47
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	98	1	true
turtle that discovers not p (id,ranking,skeptic?)	67	1	true
