model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1115
total timesteps	111
turtle that knows p	207
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	102	1	true
turtle that discovers not p (id,ranking,skeptic?)	156	1	true
