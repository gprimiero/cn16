model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	341
total timesteps	79
turtle that knows p	90
turtle that knows not p	111
turtle that discovers p (id,ranking,skeptic?)	16	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	146	1	false
