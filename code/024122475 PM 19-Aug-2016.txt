model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	401
total timesteps	44
turtle that knows p	162
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	149	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	124	1	true
