model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	76
total timesteps	22
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	11	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
