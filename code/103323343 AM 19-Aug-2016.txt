model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	628
total timesteps	76
turtle that knows p	168
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	1	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	222	1	true
