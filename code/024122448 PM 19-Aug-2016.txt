model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	457
total timesteps	56
turtle that knows p	186
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	124	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	101	1	true
model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	596
total timesteps	51
turtle that knows p	95
turtle that knows not p	106
turtle that discovers p (id,ranking,skeptic?)	16	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	24	0.25	true
