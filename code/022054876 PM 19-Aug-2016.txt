model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	180
total timesteps	49
turtle that knows p	71
turtle that knows not p	130
turtle that discovers p (id,ranking,skeptic?)	188	1	false
turtle that discovers not p (id,ranking,skeptic?)	34	1	false
