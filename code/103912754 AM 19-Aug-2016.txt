model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1134
total timesteps	173
turtle that knows p	80
turtle that knows not p	221
turtle that discovers p (id,ranking,skeptic?)	249	1	false
turtle that discovers not p (id,ranking,skeptic?)	283	1	false
