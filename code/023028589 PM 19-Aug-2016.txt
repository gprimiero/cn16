model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	23
total timesteps	12
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	28	1	false
turtle that discovers not p (id,ranking,skeptic?)	24	1	false
