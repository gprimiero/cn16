model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1243
total timesteps	10
turtle that knows p	138
turtle that knows not p	162
turtle that discovers p (id,ranking,skeptic?)	209	0	false
turtle that discovers not p (id,ranking,skeptic?)	218	0	true
