model parameters
nodes	50
skeptics	5
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	119
total timesteps	25
turtle that knows p	32
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	42	1	false
turtle that discovers not p (id,ranking,skeptic?)	48	1	false
