model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	365
total timesteps	65
turtle that knows p	167
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	94	1	false
turtle that discovers not p (id,ranking,skeptic?)	45	0.3333333333333333	false
