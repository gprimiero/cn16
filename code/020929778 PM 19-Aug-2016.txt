model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	637
total timesteps	50
turtle that knows p	73
turtle that knows not p	228
turtle that discovers p (id,ranking,skeptic?)	204	1	true
turtle that discovers not p (id,ranking,skeptic?)	205	1	false
