model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	60
total timesteps	56
turtle that knows p	0
turtle that knows not p	301
turtle that discovers p (id,ranking,skeptic?)	206	1	false
turtle that discovers not p (id,ranking,skeptic?)	162	0.2	false
