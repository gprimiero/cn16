model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	207
total timesteps	40
turtle that knows p	26
turtle that knows not p	15
turtle that discovers p (id,ranking,skeptic?)	38	1	false
turtle that discovers not p (id,ranking,skeptic?)	26	1	false
