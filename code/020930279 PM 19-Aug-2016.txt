model parameters
nodes	300
skeptics	240
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1312
total timesteps	120
turtle that knows p	59
turtle that knows not p	242
turtle that discovers p (id,ranking,skeptic?)	145	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.018518518518518517	true
