model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	432
total timesteps	60
turtle that knows p	270
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	49	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	111	1	true
