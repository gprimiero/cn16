model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	370
total timesteps	39
turtle that knows p	37
turtle that knows not p	64
turtle that discovers p (id,ranking,skeptic?)	10	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	12	1	true
