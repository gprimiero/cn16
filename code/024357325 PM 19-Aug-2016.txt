model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	161
total timesteps	29
turtle that knows p	5
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	36	1	true
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
