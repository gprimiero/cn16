model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	487
total timesteps	65
turtle that knows p	153
turtle that knows not p	148
turtle that discovers p (id,ranking,skeptic?)	147	1	true
turtle that discovers not p (id,ranking,skeptic?)	19	0.1111111111111111	true
