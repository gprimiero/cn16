model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	87
total timesteps	24
turtle that knows p	12
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	13	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	6	0.3333333333333333	false
