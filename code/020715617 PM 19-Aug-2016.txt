model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	241
total timesteps	46
turtle that knows p	97
turtle that knows not p	204
turtle that discovers p (id,ranking,skeptic?)	85	1	true
turtle that discovers not p (id,ranking,skeptic?)	71	0.5	false
