model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	376
total timesteps	59
turtle that knows p	212
turtle that knows not p	89
turtle that discovers p (id,ranking,skeptic?)	130	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	295	1	false
