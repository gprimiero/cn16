model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	438
total timesteps	65
turtle that knows p	66
turtle that knows not p	235
turtle that discovers p (id,ranking,skeptic?)	89	1	false
turtle that discovers not p (id,ranking,skeptic?)	43	1	true
