model parameters
nodes	300
skeptics	144
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	534
total timesteps	56
turtle that knows p	199
turtle that knows not p	102
turtle that discovers p (id,ranking,skeptic?)	136	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	153	1	true
