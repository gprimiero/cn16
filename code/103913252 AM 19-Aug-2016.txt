model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	742
total timesteps	74
turtle that knows p	89
turtle that knows not p	212
turtle that discovers p (id,ranking,skeptic?)	265	1	false
turtle that discovers not p (id,ranking,skeptic?)	203	1	false
