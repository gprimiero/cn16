model parameters
nodes	300
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	136
total timesteps	54
turtle that knows p	215
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	110	1	false
turtle that discovers not p (id,ranking,skeptic?)	56	0.2	false
