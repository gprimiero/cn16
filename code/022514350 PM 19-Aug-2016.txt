model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1657
total timesteps	252
turtle that knows p	247
turtle that knows not p	54
turtle that discovers p (id,ranking,skeptic?)	48	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	222	1	false
