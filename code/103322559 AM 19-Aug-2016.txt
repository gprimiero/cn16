model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	520
total timesteps	78
turtle that knows p	113
turtle that knows not p	188
turtle that discovers p (id,ranking,skeptic?)	133	1	false
turtle that discovers not p (id,ranking,skeptic?)	1	0.16666666666666666	false
