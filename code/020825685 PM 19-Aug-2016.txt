model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	347
total timesteps	55
turtle that knows p	37
turtle that knows not p	264
turtle that discovers p (id,ranking,skeptic?)	236	1	true
turtle that discovers not p (id,ranking,skeptic?)	6	0.16666666666666666	true
