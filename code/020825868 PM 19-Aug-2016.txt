model parameters
nodes	300
skeptics	144
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	833
total timesteps	112
turtle that knows p	160
turtle that knows not p	141
turtle that discovers p (id,ranking,skeptic?)	202	1	true
turtle that discovers not p (id,ranking,skeptic?)	108	0.5	false
