model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	529
total timesteps	72
turtle that knows p	5
turtle that knows not p	296
turtle that discovers p (id,ranking,skeptic?)	238	1	true
turtle that discovers not p (id,ranking,skeptic?)	49	1	false
