model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	193
total timesteps	24
turtle that knows p	10
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	0	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
