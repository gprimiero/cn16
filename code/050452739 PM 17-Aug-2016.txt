model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	179
total timesteps	26
turtle that knows p	39
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	3	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
