model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	356
total timesteps	76
turtle that knows p	19
turtle that knows not p	182
turtle that discovers p (id,ranking,skeptic?)	131	1	true
turtle that discovers not p (id,ranking,skeptic?)	57	1	false
