model parameters
nodes	50
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	280
total timesteps	41
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	22	1	true
turtle that discovers not p (id,ranking,skeptic?)	27	1	true
