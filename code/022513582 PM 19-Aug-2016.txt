model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1247
total timesteps	109
turtle that knows p	188
turtle that knows not p	113
turtle that discovers p (id,ranking,skeptic?)	177	1	false
turtle that discovers not p (id,ranking,skeptic?)	65	0.16666666666666666	false
