model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	543
total timesteps	50
turtle that knows p	255
turtle that knows not p	46
turtle that discovers p (id,ranking,skeptic?)	52	0.2	true
turtle that discovers not p (id,ranking,skeptic?)	289	0.5	true
