model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	791
total timesteps	91
turtle that knows p	132
turtle that knows not p	169
turtle that discovers p (id,ranking,skeptic?)	22	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	225	1	false
