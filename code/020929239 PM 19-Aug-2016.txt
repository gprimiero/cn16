model parameters
nodes	300
skeptics	240
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	900
total timesteps	128
turtle that knows p	68
turtle that knows not p	233
turtle that discovers p (id,ranking,skeptic?)	152	1	false
turtle that discovers not p (id,ranking,skeptic?)	47	0.1	false
