model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	481
total timesteps	55
turtle that knows p	133
turtle that knows not p	168
turtle that discovers p (id,ranking,skeptic?)	150	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	190	1	true
