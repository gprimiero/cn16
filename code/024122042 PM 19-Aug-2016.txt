model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	724
total timesteps	110
turtle that knows p	133
turtle that knows not p	168
turtle that discovers p (id,ranking,skeptic?)	40	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	15	0.16666666666666666	true
