model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	586
total timesteps	124
turtle that knows p	121
turtle that knows not p	179
turtle that discovers p (id,ranking,skeptic?)	209	211	false
turtle that discovers not p (id,ranking,skeptic?)	186	124	false
