model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	1297
total timesteps	10
turtle that knows p	123
turtle that knows not p	177
turtle that discovers p (id,ranking,skeptic?)	35	0	true
turtle that discovers not p (id,ranking,skeptic?)	236	0	true
