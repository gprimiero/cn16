model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	309
total timesteps	55
turtle that knows p	173
turtle that knows not p	128
turtle that discovers p (id,ranking,skeptic?)	184	1	true
turtle that discovers not p (id,ranking,skeptic?)	225	1	true
model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	791
total timesteps	70
turtle that knows p	134
turtle that knows not p	167
turtle that discovers p (id,ranking,skeptic?)	217	1	false
turtle that discovers not p (id,ranking,skeptic?)	7	0.16666666666666666	false
