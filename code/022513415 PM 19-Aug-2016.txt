model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	438
total timesteps	82
turtle that knows p	249
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	157	1	false
turtle that discovers not p (id,ranking,skeptic?)	92	1	false
