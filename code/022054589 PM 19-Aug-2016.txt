model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	31
total timesteps	46
turtle that knows p	201
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	58	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	169	1	false
