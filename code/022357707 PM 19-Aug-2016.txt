model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	740
total timesteps	70
turtle that knows p	26
turtle that knows not p	275
turtle that discovers p (id,ranking,skeptic?)	121	1	false
turtle that discovers not p (id,ranking,skeptic?)	193	1	false
