model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	122
total timesteps	27
turtle that knows p	12
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	22	1	true
turtle that discovers not p (id,ranking,skeptic?)	0	0.07692307692307693	false
