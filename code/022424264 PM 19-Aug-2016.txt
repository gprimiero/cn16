model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1146
total timesteps	88
turtle that knows p	69
turtle that knows not p	232
turtle that discovers p (id,ranking,skeptic?)	9	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	87	0.5	false
