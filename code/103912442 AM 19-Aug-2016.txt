model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	589
total timesteps	79
turtle that knows p	154
turtle that knows not p	147
turtle that discovers p (id,ranking,skeptic?)	277	1	true
turtle that discovers not p (id,ranking,skeptic?)	298	1	true
