model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	325
total timesteps	39
turtle that knows p	35
turtle that knows not p	66
turtle that discovers p (id,ranking,skeptic?)	2	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
