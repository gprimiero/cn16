model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	396
total timesteps	85
turtle that knows p	129
turtle that knows not p	172
turtle that discovers p (id,ranking,skeptic?)	194	1	true
turtle that discovers not p (id,ranking,skeptic?)	189	1	true
