model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	286
total timesteps	54
turtle that knows p	275
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	206	1	true
turtle that discovers not p (id,ranking,skeptic?)	249	1	false
