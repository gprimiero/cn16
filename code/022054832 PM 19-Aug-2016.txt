model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	216
total timesteps	48
turtle that knows p	167
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	151	1	false
turtle that discovers not p (id,ranking,skeptic?)	194	1	false
