model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	377
total timesteps	45
turtle that knows p	95
turtle that knows not p	206
turtle that discovers p (id,ranking,skeptic?)	269	1	false
turtle that discovers not p (id,ranking,skeptic?)	35	0.3333333333333333	true
