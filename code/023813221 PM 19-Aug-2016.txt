model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	92
total timesteps	22
turtle that knows p	8
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	1	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
