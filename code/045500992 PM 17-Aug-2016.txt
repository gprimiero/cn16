model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	161
total timesteps	28
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	49	1	true
turtle that discovers not p (id,ranking,skeptic?)	5	1	false
