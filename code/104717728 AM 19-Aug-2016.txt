model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	655
total timesteps	82
turtle that knows p	257
turtle that knows not p	44
turtle that discovers p (id,ranking,skeptic?)	128	1	true
turtle that discovers not p (id,ranking,skeptic?)	95	1	true
