model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	316
total timesteps	63
turtle that knows p	22
turtle that knows not p	279
turtle that discovers p (id,ranking,skeptic?)	23	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	239	1	false
