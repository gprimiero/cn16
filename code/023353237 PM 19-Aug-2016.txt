model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	449
total timesteps	42
turtle that knows p	176
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	189	1	true
turtle that discovers not p (id,ranking,skeptic?)	54	0.5	false
