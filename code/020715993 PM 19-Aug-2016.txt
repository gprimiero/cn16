model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	458
total timesteps	50
turtle that knows p	156
turtle that knows not p	145
turtle that discovers p (id,ranking,skeptic?)	137	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	256	1	true
