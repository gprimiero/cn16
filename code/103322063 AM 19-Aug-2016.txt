model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	413
total timesteps	65
turtle that knows p	244
turtle that knows not p	57
turtle that discovers p (id,ranking,skeptic?)	262	1	true
turtle that discovers not p (id,ranking,skeptic?)	265	1	true
