model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	618
total timesteps	54
turtle that knows p	262
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	16	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	106	0.5	true
