model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	541
total timesteps	57
turtle that knows p	89
turtle that knows not p	212
turtle that discovers p (id,ranking,skeptic?)	158	1	false
turtle that discovers not p (id,ranking,skeptic?)	291	1	true
