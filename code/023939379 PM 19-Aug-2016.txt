model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	615
total timesteps	82
turtle that knows p	126
turtle that knows not p	175
turtle that discovers p (id,ranking,skeptic?)	83	1	true
turtle that discovers not p (id,ranking,skeptic?)	36	0.3333333333333333	true
