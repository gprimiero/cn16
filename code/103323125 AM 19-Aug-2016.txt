model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	576
total timesteps	132
turtle that knows p	70
turtle that knows not p	231
turtle that discovers p (id,ranking,skeptic?)	173	1	false
turtle that discovers not p (id,ranking,skeptic?)	150	1	false
