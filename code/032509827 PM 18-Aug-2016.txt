model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	887
total timesteps	9
turtle that knows p	157
turtle that knows not p	143
turtle that discovers p (id,ranking,skeptic?)	274	0	false
turtle that discovers not p (id,ranking,skeptic?)	115	0	true
