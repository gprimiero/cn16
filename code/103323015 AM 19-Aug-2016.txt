model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	943
total timesteps	79
turtle that knows p	87
turtle that knows not p	214
turtle that discovers p (id,ranking,skeptic?)	140	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	4	0.16666666666666666	false
