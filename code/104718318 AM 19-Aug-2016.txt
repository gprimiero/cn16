model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1017
total timesteps	73
turtle that knows p	213
turtle that knows not p	88
turtle that discovers p (id,ranking,skeptic?)	40	1	true
turtle that discovers not p (id,ranking,skeptic?)	182	0.5	true
