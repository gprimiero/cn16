model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	480
total timesteps	72
turtle that knows p	175
turtle that knows not p	126
turtle that discovers p (id,ranking,skeptic?)	121	1	false
turtle that discovers not p (id,ranking,skeptic?)	251	1	true
