model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	123
total timesteps	19
turtle that knows p	27
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	48	1	false
turtle that discovers not p (id,ranking,skeptic?)	19	0.3333333333333333	true
