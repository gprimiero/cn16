model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	824
total timesteps	63
turtle that knows p	207
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	71	1	true
turtle that discovers not p (id,ranking,skeptic?)	108	0.2	true
