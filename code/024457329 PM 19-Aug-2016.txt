model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	70
total timesteps	17
turtle that knows p	6
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	16	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.5	false
