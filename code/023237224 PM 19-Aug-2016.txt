model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	820
total timesteps	97
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	49	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	104	0.3333333333333333	true
