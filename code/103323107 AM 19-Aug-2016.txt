model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	317
total timesteps	59
turtle that knows p	141
turtle that knows not p	160
turtle that discovers p (id,ranking,skeptic?)	141	1	false
turtle that discovers not p (id,ranking,skeptic?)	122	1	true
