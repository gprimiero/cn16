model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	80
total timesteps	23
turtle that knows p	8
turtle that knows not p	43
turtle that discovers p (id,ranking,skeptic?)	35	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	0	1	false
