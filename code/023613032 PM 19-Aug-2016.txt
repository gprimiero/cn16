model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	71
total timesteps	19
turtle that knows p	26
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	10	1	true
turtle that discovers not p (id,ranking,skeptic?)	18	1	true
