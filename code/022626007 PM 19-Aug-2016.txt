model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	110
total timesteps	46
turtle that knows p	71
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	9	1	false
turtle that discovers not p (id,ranking,skeptic?)	32	0.2	false
model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	169
total timesteps	40
turtle that knows p	84
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	1	0.045454545454545456	false
turtle that discovers not p (id,ranking,skeptic?)	95	1	false
