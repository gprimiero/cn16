model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	121
total timesteps	30
turtle that knows p	39
turtle that knows not p	62
turtle that discovers p (id,ranking,skeptic?)	35	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	7	0.2	true
model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	444
total timesteps	41
turtle that knows p	67
turtle that knows not p	34
turtle that discovers p (id,ranking,skeptic?)	64	1	true
turtle that discovers not p (id,ranking,skeptic?)	56	1	true
