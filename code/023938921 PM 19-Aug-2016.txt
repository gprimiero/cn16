model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	731
total timesteps	64
turtle that knows p	63
turtle that knows not p	238
turtle that discovers p (id,ranking,skeptic?)	259	1	true
turtle that discovers not p (id,ranking,skeptic?)	262	1	true
