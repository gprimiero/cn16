model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	466
total timesteps	59
turtle that knows p	297
turtle that knows not p	4
turtle that discovers p (id,ranking,skeptic?)	25	0.14285714285714285	false
turtle that discovers not p (id,ranking,skeptic?)	111	1	true
