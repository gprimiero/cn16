model parameters
nodes	40
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	500
total timesteps	50
turtle that knows p	41
turtle that knows not p	260
turtle that discovers p (id,ranking,skeptic?)	89	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	288	1	true
