model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	248
total timesteps	42
turtle that knows p	24
turtle that knows not p	27
turtle that discovers p (id,ranking,skeptic?)	15	1	false
turtle that discovers not p (id,ranking,skeptic?)	47	1	true
model parameters
nodes	50
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	225
total timesteps	34
turtle that knows p	13
turtle that knows not p	38
turtle that discovers p (id,ranking,skeptic?)	17	1	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	false
