model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	220
total timesteps	28
turtle that knows p	16
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	49	1	true
turtle that discovers not p (id,ranking,skeptic?)	50	1	true
