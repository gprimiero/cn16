model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	10
total timesteps	21
turtle that knows p	0
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	8	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	false
