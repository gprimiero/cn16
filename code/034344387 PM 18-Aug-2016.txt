model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1283
total timesteps	10
turtle that knows p	161
turtle that knows not p	139
turtle that discovers p (id,ranking,skeptic?)	86	0	false
turtle that discovers not p (id,ranking,skeptic?)	100	0	true
