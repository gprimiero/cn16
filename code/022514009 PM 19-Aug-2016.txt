model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	74
total timesteps	49
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	206	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	0.08333333333333333	false
