model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	667
total timesteps	60
turtle that knows p	57
turtle that knows not p	244
turtle that discovers p (id,ranking,skeptic?)	94	1	false
turtle that discovers not p (id,ranking,skeptic?)	0	0.09090909090909091	false
