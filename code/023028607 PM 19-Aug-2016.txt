model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	49
total timesteps	20
turtle that knows p	31
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	16	1	false
turtle that discovers not p (id,ranking,skeptic?)	6	1	false
