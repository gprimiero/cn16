model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	194
total timesteps	28
turtle that knows p	91
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	73	1	true
turtle that discovers not p (id,ranking,skeptic?)	64	1	true
model parameters
nodes	100
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	109
total timesteps	28
turtle that knows p	42
turtle that knows not p	59
turtle that discovers p (id,ranking,skeptic?)	25	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	45	0.3333333333333333	true
