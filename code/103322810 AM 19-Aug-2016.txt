model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	356
total timesteps	60
turtle that knows p	201
turtle that knows not p	100
turtle that discovers p (id,ranking,skeptic?)	170	1	false
turtle that discovers not p (id,ranking,skeptic?)	82	1	true
