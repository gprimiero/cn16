model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	669
total timesteps	95
turtle that knows p	208
turtle that knows not p	93
turtle that discovers p (id,ranking,skeptic?)	113	1	true
turtle that discovers not p (id,ranking,skeptic?)	29	0.1111111111111111	true
