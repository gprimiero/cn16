model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	332
total timesteps	63
turtle that knows p	74
turtle that knows not p	227
turtle that discovers p (id,ranking,skeptic?)	149	1	true
turtle that discovers not p (id,ranking,skeptic?)	123	0.5	true
