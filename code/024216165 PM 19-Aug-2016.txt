model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	190
total timesteps	33
turtle that knows p	16
turtle that knows not p	85
turtle that discovers p (id,ranking,skeptic?)	53	1	false
turtle that discovers not p (id,ranking,skeptic?)	20	1	false
