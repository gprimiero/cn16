model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	930
total timesteps	99
turtle that knows p	76
turtle that knows not p	225
turtle that discovers p (id,ranking,skeptic?)	284	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	257	1	true
