model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	85
total timesteps	39
turtle that knows p	23
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	32	1	false
turtle that discovers not p (id,ranking,skeptic?)	6	1	false
model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	83
total timesteps	21
turtle that knows p	17
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	14	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	30	1	false
