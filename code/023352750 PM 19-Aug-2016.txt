model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	324
total timesteps	43
turtle that knows p	184
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	92	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	128	1	true
