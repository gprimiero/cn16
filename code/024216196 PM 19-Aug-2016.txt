model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	319
total timesteps	43
turtle that knows p	51
turtle that knows not p	50
turtle that discovers p (id,ranking,skeptic?)	30	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	36	1	true
