model parameters
nodes	300
skeptics	33
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1142
total timesteps	154
turtle that knows p	99
turtle that knows not p	202
turtle that discovers p (id,ranking,skeptic?)	265	1	false
turtle that discovers not p (id,ranking,skeptic?)	200	1	true
