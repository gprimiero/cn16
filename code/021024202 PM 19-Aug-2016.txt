model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	595
total timesteps	63
turtle that knows p	11
turtle that knows not p	290
turtle that discovers p (id,ranking,skeptic?)	53	0.16666666666666666	true
turtle that discovers not p (id,ranking,skeptic?)	155	1	true
