model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	360
total timesteps	49
turtle that knows p	119
turtle that knows not p	182
turtle that discovers p (id,ranking,skeptic?)	9	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	106	0.3333333333333333	true
