model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	384
total timesteps	51
turtle that knows p	175
turtle that knows not p	126
turtle that discovers p (id,ranking,skeptic?)	12	1	true
turtle that discovers not p (id,ranking,skeptic?)	139	0.25	true
