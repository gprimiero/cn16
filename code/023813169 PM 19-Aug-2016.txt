model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	60
total timesteps	14
turtle that knows p	20
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	26	1	false
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
