model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	438
total timesteps	47
turtle that knows p	255
turtle that knows not p	46
turtle that discovers p (id,ranking,skeptic?)	183	1	false
turtle that discovers not p (id,ranking,skeptic?)	295	1	false
