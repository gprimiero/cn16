model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	89
total timesteps	31
turtle that knows p	50
turtle that knows not p	151
turtle that discovers p (id,ranking,skeptic?)	133	1	false
turtle that discovers not p (id,ranking,skeptic?)	52	1	false
