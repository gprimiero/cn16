model parameters
nodes	300
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	778
total timesteps	114
turtle that knows p	145
turtle that knows not p	156
turtle that discovers p (id,ranking,skeptic?)	82	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	100	0.5	false
