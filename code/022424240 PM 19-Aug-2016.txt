model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1259
total timesteps	156
turtle that knows p	59
turtle that knows not p	242
turtle that discovers p (id,ranking,skeptic?)	50	0.1	false
turtle that discovers not p (id,ranking,skeptic?)	47	1	false
