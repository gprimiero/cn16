model parameters
nodes	300
skeptics	240
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1288
total timesteps	147
turtle that knows p	130
turtle that knows not p	171
turtle that discovers p (id,ranking,skeptic?)	161	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	296	1	true
