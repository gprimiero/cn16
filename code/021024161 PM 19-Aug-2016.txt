model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	977
total timesteps	108
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	142	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	157	1	true
