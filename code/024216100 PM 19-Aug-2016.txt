model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	696
total timesteps	106
turtle that knows p	64
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	91	1	true
turtle that discovers not p (id,ranking,skeptic?)	37	1	true
