model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	241
total timesteps	30
turtle that knows p	78
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	34	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	36	0.2	true
