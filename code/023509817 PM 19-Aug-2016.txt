model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	78
total timesteps	18
turtle that knows p	33
turtle that knows not p	68
turtle that discovers p (id,ranking,skeptic?)	60	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	31	1	false
