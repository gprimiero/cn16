model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1269
total timesteps	10
turtle that knows p	165
turtle that knows not p	135
turtle that discovers p (id,ranking,skeptic?)	80	0	false
turtle that discovers not p (id,ranking,skeptic?)	174	0	true
