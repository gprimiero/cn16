model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	439
total timesteps	127
turtle that knows p	102
turtle that knows not p	198
turtle that discovers p (id,ranking,skeptic?)	242	250	false
turtle that discovers not p (id,ranking,skeptic?)	243	126	false
