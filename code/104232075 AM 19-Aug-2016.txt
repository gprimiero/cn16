model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	278
total timesteps	37
turtle that knows p	220
turtle that knows not p	81
turtle that discovers p (id,ranking,skeptic?)	50	1	true
turtle that discovers not p (id,ranking,skeptic?)	73	1	false
