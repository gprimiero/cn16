model parameters
nodes	300
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	778
total timesteps	67
turtle that knows p	218
turtle that knows not p	83
turtle that discovers p (id,ranking,skeptic?)	0	0.037037037037037035	false
turtle that discovers not p (id,ranking,skeptic?)	283	1	false
