model parameters
nodes	200
skeptics	23
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	296
total timesteps	37
turtle that knows p	143
turtle that knows not p	58
turtle that discovers p (id,ranking,skeptic?)	1	0.05555555555555555	false
turtle that discovers not p (id,ranking,skeptic?)	113	1	false
