model parameters
nodes	300
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	993
total timesteps	91
turtle that knows p	254
turtle that knows not p	47
turtle that discovers p (id,ranking,skeptic?)	169	1	false
turtle that discovers not p (id,ranking,skeptic?)	167	0.5	false
