model parameters
nodes	40
skeptics	18
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	44
total timesteps	17
turtle that knows p	16
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	37	1	true
turtle that discovers not p (id,ranking,skeptic?)	14	0.5	false
