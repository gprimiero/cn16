model parameters
nodes	300
skeptics	32
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	310
total timesteps	72
turtle that knows p	238
turtle that knows not p	63
turtle that discovers p (id,ranking,skeptic?)	249	1	false
turtle that discovers not p (id,ranking,skeptic?)	248	1	false
