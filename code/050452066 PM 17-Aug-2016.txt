model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	756
total timesteps	83
turtle that knows p	214
turtle that knows not p	87
turtle that discovers p (id,ranking,skeptic?)	3	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	52	0.2	false
