model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	469
total timesteps	77
turtle that knows p	189
turtle that knows not p	112
turtle that discovers p (id,ranking,skeptic?)	298	1	false
turtle that discovers not p (id,ranking,skeptic?)	297	1	true
