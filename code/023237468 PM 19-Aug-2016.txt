model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	554
total timesteps	64
turtle that knows p	73
turtle that knows not p	228
turtle that discovers p (id,ranking,skeptic?)	177	1	true
turtle that discovers not p (id,ranking,skeptic?)	209	0.3333333333333333	true
