model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	582
total timesteps	47
turtle that knows p	248
turtle that knows not p	53
turtle that discovers p (id,ranking,skeptic?)	59	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	194	1	true
