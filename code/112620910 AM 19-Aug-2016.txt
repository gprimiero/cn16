model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	411
total timesteps	67
turtle that knows p	289
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	134	1	false
turtle that discovers not p (id,ranking,skeptic?)	150	1	true
