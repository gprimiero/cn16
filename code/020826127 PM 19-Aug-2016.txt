model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	426
total timesteps	91
turtle that knows p	152
turtle that knows not p	149
turtle that discovers p (id,ranking,skeptic?)	150	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	253	1	false
