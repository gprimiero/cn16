model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	406
total timesteps	71
turtle that knows p	122
turtle that knows not p	179
turtle that discovers p (id,ranking,skeptic?)	243	1	true
turtle that discovers not p (id,ranking,skeptic?)	114	1	true
