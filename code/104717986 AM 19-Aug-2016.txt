model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	591
total timesteps	60
turtle that knows p	225
turtle that knows not p	76
turtle that discovers p (id,ranking,skeptic?)	22	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	8	0.5	true
