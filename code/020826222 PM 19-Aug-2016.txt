model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1052
total timesteps	98
turtle that knows p	201
turtle that knows not p	100
turtle that discovers p (id,ranking,skeptic?)	171	1	false
turtle that discovers not p (id,ranking,skeptic?)	127	0.125	false
