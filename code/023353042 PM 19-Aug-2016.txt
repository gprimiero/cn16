model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	449
total timesteps	51
turtle that knows p	175
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	46	1	true
turtle that discovers not p (id,ranking,skeptic?)	67	0.5	false
