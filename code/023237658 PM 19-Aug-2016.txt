model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	370
total timesteps	48
turtle that knows p	91
turtle that knows not p	210
turtle that discovers p (id,ranking,skeptic?)	176	1	true
turtle that discovers not p (id,ranking,skeptic?)	146	1	false
