model parameters
nodes	100
skeptics	14
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	55
total timesteps	22
turtle that knows p	77
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	57	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	70	1	false
