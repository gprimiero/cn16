model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	183
total timesteps	53
turtle that knows p	173
turtle that knows not p	128
turtle that discovers p (id,ranking,skeptic?)	64	1	false
turtle that discovers not p (id,ranking,skeptic?)	99	0.25	false
