model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	107
total timesteps	24
turtle that knows p	21
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	24	1	false
turtle that discovers not p (id,ranking,skeptic?)	13	0.5	true
