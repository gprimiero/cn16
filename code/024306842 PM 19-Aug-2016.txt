model parameters
nodes	50
skeptics	45
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	130
total timesteps	26
turtle that knows p	14
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	43	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.3333333333333333	true
