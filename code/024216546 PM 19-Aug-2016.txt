model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	448
total timesteps	69
turtle that knows p	66
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	21	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	74	1	true
