model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	960
total timesteps	85
turtle that knows p	236
turtle that knows not p	65
turtle that discovers p (id,ranking,skeptic?)	271	1	true
turtle that discovers not p (id,ranking,skeptic?)	80	0.3333333333333333	true
