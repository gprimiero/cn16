model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	725
total timesteps	73
turtle that knows p	194
turtle that knows not p	107
turtle that discovers p (id,ranking,skeptic?)	86	1	false
turtle that discovers not p (id,ranking,skeptic?)	296	1	false
