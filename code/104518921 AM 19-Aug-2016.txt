model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	666
total timesteps	86
turtle that knows p	192
turtle that knows not p	109
turtle that discovers p (id,ranking,skeptic?)	249	1	true
turtle that discovers not p (id,ranking,skeptic?)	243	1	true
