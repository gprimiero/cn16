model parameters
nodes	40
skeptics	18
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	29
total timesteps	8
turtle that knows p	30
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	30	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	0.5	false
