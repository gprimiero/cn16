model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	951
total timesteps	91
turtle that knows p	212
turtle that knows not p	89
turtle that discovers p (id,ranking,skeptic?)	110	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	18	0.3333333333333333	false
