model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	503
total timesteps	9
turtle that knows p	50
turtle that knows not p	250
turtle that discovers p (id,ranking,skeptic?)	108	0	false
turtle that discovers not p (id,ranking,skeptic?)	225	0	true
