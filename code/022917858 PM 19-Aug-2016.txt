model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	32
total timesteps	14
turtle that knows p	2
turtle that knows not p	39
turtle that discovers p (id,ranking,skeptic?)	22	1	false
turtle that discovers not p (id,ranking,skeptic?)	0	0.25	true
