model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	336
total timesteps	56
turtle that knows p	102
turtle that knows not p	199
turtle that discovers p (id,ranking,skeptic?)	300	1	false
turtle that discovers not p (id,ranking,skeptic?)	119	1	false
