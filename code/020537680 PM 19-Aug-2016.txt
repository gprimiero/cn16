model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	336
total timesteps	81
turtle that knows p	252
turtle that knows not p	49
turtle that discovers p (id,ranking,skeptic?)	122	1	false
turtle that discovers not p (id,ranking,skeptic?)	38	1	false
