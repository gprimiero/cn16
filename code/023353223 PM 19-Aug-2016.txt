model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	501
total timesteps	38
turtle that knows p	185
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	34	1	false
turtle that discovers not p (id,ranking,skeptic?)	164	0.5	false
