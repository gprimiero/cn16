model parameters
nodes	30
skeptics	28
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	153
total timesteps	35
turtle that knows p	24
turtle that knows not p	7
turtle that discovers p (id,ranking,skeptic?)	29	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	0.5	true
