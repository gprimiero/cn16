model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	374
total timesteps	37
turtle that knows p	87
turtle that knows not p	214
turtle that discovers p (id,ranking,skeptic?)	99	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	103	0.5	true
