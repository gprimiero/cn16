model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	741
total timesteps	69
turtle that knows p	60
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	41	1	true
turtle that discovers not p (id,ranking,skeptic?)	97	1	true
model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	801
total timesteps	62
turtle that knows p	22
turtle that knows not p	79
turtle that discovers p (id,ranking,skeptic?)	91	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.3333333333333333	true
