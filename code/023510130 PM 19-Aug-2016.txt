model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	165
total timesteps	26
turtle that knows p	73
turtle that knows not p	28
turtle that discovers p (id,ranking,skeptic?)	95	1	false
turtle that discovers not p (id,ranking,skeptic?)	7	0.2	true
