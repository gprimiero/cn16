model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	181
total timesteps	31
turtle that knows p	42
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	12	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	6	0.1111111111111111	true
