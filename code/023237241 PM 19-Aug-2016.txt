model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	326
total timesteps	45
turtle that knows p	154
turtle that knows not p	147
turtle that discovers p (id,ranking,skeptic?)	74	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	23	1	true
