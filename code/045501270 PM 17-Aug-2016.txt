model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	143
total timesteps	103
turtle that knows p	109
turtle that knows not p	191
turtle that discovers p (id,ranking,skeptic?)	138	294	true
turtle that discovers not p (id,ranking,skeptic?)	159	87	true
