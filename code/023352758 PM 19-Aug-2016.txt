model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	182
total timesteps	37
turtle that knows p	140
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	44	1	true
turtle that discovers not p (id,ranking,skeptic?)	140	1	false
