model parameters
nodes	40
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	53
total timesteps	12
turtle that knows p	22
turtle that knows not p	19
turtle that discovers p (id,ranking,skeptic?)	3	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	12	0.5	true
