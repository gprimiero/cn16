model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	388
total timesteps	62
turtle that knows p	34
turtle that knows not p	267
turtle that discovers p (id,ranking,skeptic?)	225	1	true
turtle that discovers not p (id,ranking,skeptic?)	26	0.3333333333333333	true
