model parameters
nodes	300
skeptics	20
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	195
total timesteps	66
turtle that knows p	248
turtle that knows not p	53
turtle that discovers p (id,ranking,skeptic?)	59	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	0.045454545454545456	false
