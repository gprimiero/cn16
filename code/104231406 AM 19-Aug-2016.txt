model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	635
total timesteps	92
turtle that knows p	94
turtle that knows not p	207
turtle that discovers p (id,ranking,skeptic?)	79	1	true
turtle that discovers not p (id,ranking,skeptic?)	62	0.14285714285714285	false
