model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	1503
total timesteps	117
turtle that knows p	215
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	165	1	false
turtle that discovers not p (id,ranking,skeptic?)	10	1	true
