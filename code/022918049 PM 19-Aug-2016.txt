model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	79
total timesteps	26
turtle that knows p	24
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	11	1	false
turtle that discovers not p (id,ranking,skeptic?)	34	1	false
