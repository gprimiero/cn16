model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	695
total timesteps	64
turtle that knows p	24
turtle that knows not p	177
turtle that discovers p (id,ranking,skeptic?)	98	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	120	1	true
