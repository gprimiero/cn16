model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	382
total timesteps	62
turtle that knows p	71
turtle that knows not p	30
turtle that discovers p (id,ranking,skeptic?)	51	1	true
turtle that discovers not p (id,ranking,skeptic?)	86	1	true
