model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	78
total timesteps	24
turtle that knows p	21
turtle that knows not p	10
turtle that discovers p (id,ranking,skeptic?)	22	1	false
turtle that discovers not p (id,ranking,skeptic?)	21	0.3333333333333333	false
model parameters
nodes	30
skeptics	17
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	60
total timesteps	15
turtle that knows p	15
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	4	1	true
turtle that discovers not p (id,ranking,skeptic?)	6	1	false
