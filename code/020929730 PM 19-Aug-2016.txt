model parameters
nodes	300
skeptics	233
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	448
total timesteps	55
turtle that knows p	186
turtle that knows not p	115
turtle that discovers p (id,ranking,skeptic?)	74	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	177	1	false
