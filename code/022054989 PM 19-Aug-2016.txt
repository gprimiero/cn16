model parameters
nodes	200
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	366
total timesteps	53
turtle that knows p	178
turtle that knows not p	123
turtle that discovers p (id,ranking,skeptic?)	222	1	true
turtle that discovers not p (id,ranking,skeptic?)	178	1	false
