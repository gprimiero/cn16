model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	301
total timesteps	56
turtle that knows p	186
turtle that knows not p	115
turtle that discovers p (id,ranking,skeptic?)	80	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	176	1	false
