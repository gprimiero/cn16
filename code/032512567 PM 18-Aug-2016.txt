model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	851
total timesteps	9
turtle that knows p	126
turtle that knows not p	174
turtle that discovers p (id,ranking,skeptic?)	32	0	false
turtle that discovers not p (id,ranking,skeptic?)	200	0	true
