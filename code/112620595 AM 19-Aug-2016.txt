model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	314
total timesteps	49
turtle that knows p	83
turtle that knows not p	218
turtle that discovers p (id,ranking,skeptic?)	25	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	235	1	true
