model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	484
total timesteps	75
turtle that knows p	222
turtle that knows not p	79
turtle that discovers p (id,ranking,skeptic?)	264	1	true
turtle that discovers not p (id,ranking,skeptic?)	230	1	false
