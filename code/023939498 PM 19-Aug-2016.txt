model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	839
total timesteps	91
turtle that knows p	173
turtle that knows not p	128
turtle that discovers p (id,ranking,skeptic?)	110	1	true
turtle that discovers not p (id,ranking,skeptic?)	159	1	true
