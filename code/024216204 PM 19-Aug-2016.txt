model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	193
total timesteps	34
turtle that knows p	80
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	28	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	53	1	false
