model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	662
total timesteps	70
turtle that knows p	39
turtle that knows not p	262
turtle that discovers p (id,ranking,skeptic?)	171	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	206	1	false
