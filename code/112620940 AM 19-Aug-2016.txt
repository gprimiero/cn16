model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	366
total timesteps	47
turtle that knows p	86
turtle that knows not p	215
turtle that discovers p (id,ranking,skeptic?)	77	1	false
turtle that discovers not p (id,ranking,skeptic?)	229	1	true
