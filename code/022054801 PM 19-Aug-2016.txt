model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	240
total timesteps	55
turtle that knows p	164
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	181	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	0.5	false
