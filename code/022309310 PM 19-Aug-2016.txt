model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1055
total timesteps	130
turtle that knows p	198
turtle that knows not p	103
turtle that discovers p (id,ranking,skeptic?)	147	1	false
turtle that discovers not p (id,ranking,skeptic?)	186	1	false
