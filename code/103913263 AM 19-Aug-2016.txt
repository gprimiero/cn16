model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2556
total timesteps	109
turtle that knows p	184
turtle that knows not p	117
turtle that discovers p (id,ranking,skeptic?)	82	1	true
turtle that discovers not p (id,ranking,skeptic?)	79	0.2	false
