model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	485
total timesteps	63
turtle that knows p	234
turtle that knows not p	67
turtle that discovers p (id,ranking,skeptic?)	106	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	198	1	true
