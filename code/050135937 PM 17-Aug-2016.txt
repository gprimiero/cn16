model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	897
total timesteps	9
turtle that knows p	128
turtle that knows not p	172
turtle that discovers p (id,ranking,skeptic?)	87	0	true
turtle that discovers not p (id,ranking,skeptic?)	45	0	true
