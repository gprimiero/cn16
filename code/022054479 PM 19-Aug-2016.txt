model parameters
nodes	200
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	290
total timesteps	67
turtle that knows p	151
turtle that knows not p	50
turtle that discovers p (id,ranking,skeptic?)	183	1	false
turtle that discovers not p (id,ranking,skeptic?)	168	1	false
