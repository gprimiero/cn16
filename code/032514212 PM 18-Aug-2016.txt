model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1302
total timesteps	10
turtle that knows p	186
turtle that knows not p	114
turtle that discovers p (id,ranking,skeptic?)	66	0	false
turtle that discovers not p (id,ranking,skeptic?)	18	0	true
