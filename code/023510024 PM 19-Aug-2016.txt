model parameters
nodes	100
skeptics	53
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	295
total timesteps	34
turtle that knows p	76
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	87	1	true
turtle that discovers not p (id,ranking,skeptic?)	91	1	true
