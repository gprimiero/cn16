model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	140
total timesteps	29
turtle that knows p	5
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	22	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	33	1	true
