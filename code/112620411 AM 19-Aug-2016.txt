model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	340
total timesteps	38
turtle that knows p	33
turtle that knows not p	268
turtle that discovers p (id,ranking,skeptic?)	245	1	false
turtle that discovers not p (id,ranking,skeptic?)	18	0.3333333333333333	true
