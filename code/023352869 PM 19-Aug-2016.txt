model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	278
total timesteps	38
turtle that knows p	68
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	141	1	true
turtle that discovers not p (id,ranking,skeptic?)	7	0.16666666666666666	false
