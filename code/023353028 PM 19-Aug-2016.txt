model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	311
total timesteps	58
turtle that knows p	62
turtle that knows not p	139
turtle that discovers p (id,ranking,skeptic?)	92	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	170	1	true
