model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	527
total timesteps	77
turtle that knows p	189
turtle that knows not p	112
turtle that discovers p (id,ranking,skeptic?)	33	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	218	1	true
