model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	527
total timesteps	65
turtle that knows p	60
turtle that knows not p	241
turtle that discovers p (id,ranking,skeptic?)	286	1	true
turtle that discovers not p (id,ranking,skeptic?)	219	1	true
