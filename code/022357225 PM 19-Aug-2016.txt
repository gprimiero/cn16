model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	185
total timesteps	55
turtle that knows p	244
turtle that knows not p	57
turtle that discovers p (id,ranking,skeptic?)	67	1	false
turtle that discovers not p (id,ranking,skeptic?)	31	0.2	false
