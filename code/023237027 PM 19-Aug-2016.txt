model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	398
total timesteps	54
turtle that knows p	34
turtle that knows not p	267
turtle that discovers p (id,ranking,skeptic?)	134	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	291	1	true
