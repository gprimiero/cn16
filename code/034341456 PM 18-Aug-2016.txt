model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1372
total timesteps	10
turtle that knows p	148
turtle that knows not p	152
turtle that discovers p (id,ranking,skeptic?)	6	0	false
turtle that discovers not p (id,ranking,skeptic?)	160	0	true
