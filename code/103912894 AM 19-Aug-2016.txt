model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1145
total timesteps	84
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	68	1	false
turtle that discovers not p (id,ranking,skeptic?)	24	0.2	false
