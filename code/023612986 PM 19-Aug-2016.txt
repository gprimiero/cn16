model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	60
total timesteps	23
turtle that knows p	26
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	44	1	true
turtle that discovers not p (id,ranking,skeptic?)	43	1	true
