model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	114
total timesteps	23
turtle that knows p	13
turtle that knows not p	38
turtle that discovers p (id,ranking,skeptic?)	37	1	false
turtle that discovers not p (id,ranking,skeptic?)	19	1	false
