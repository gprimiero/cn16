model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	97
total timesteps	19
turtle that knows p	35
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	2	0.1	false
turtle that discovers not p (id,ranking,skeptic?)	23	1	false
