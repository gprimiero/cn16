model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	47
total timesteps	67
turtle that knows p	301
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	83	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	97	1	false
