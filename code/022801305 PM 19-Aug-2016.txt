model parameters
nodes	50
skeptics	11
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	157
total timesteps	28
turtle that knows p	15
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	48	1	false
turtle that discovers not p (id,ranking,skeptic?)	46	1	false
