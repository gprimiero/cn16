model parameters
nodes	300
skeptics	32
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	459
total timesteps	60
turtle that knows p	145
turtle that knows not p	156
turtle that discovers p (id,ranking,skeptic?)	151	1	true
turtle that discovers not p (id,ranking,skeptic?)	104	0.2	false
