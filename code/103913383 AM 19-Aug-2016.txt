model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	564
total timesteps	121
turtle that knows p	165
turtle that knows not p	136
turtle that discovers p (id,ranking,skeptic?)	268	1	false
turtle that discovers not p (id,ranking,skeptic?)	284	0.5	false
