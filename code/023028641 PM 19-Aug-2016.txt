model parameters
nodes	30
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	54
total timesteps	15
turtle that knows p	26
turtle that knows not p	5
turtle that discovers p (id,ranking,skeptic?)	16	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	12	1	false
