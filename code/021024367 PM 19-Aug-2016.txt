model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	483
total timesteps	54
turtle that knows p	164
turtle that knows not p	137
turtle that discovers p (id,ranking,skeptic?)	225	1	true
turtle that discovers not p (id,ranking,skeptic?)	257	1	true
