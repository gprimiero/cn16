model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	67
total timesteps	20
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	0	1	false
turtle that discovers not p (id,ranking,skeptic?)	30	1	true
