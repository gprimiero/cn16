model parameters
nodes	100
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	728
total timesteps	86
turtle that knows p	40
turtle that knows not p	261
turtle that discovers p (id,ranking,skeptic?)	154	1	true
turtle that discovers not p (id,ranking,skeptic?)	140	1	true
