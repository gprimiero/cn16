model parameters
nodes	300
skeptics	141
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	719
total timesteps	163
turtle that knows p	209
turtle that knows not p	91
turtle that discovers p (id,ranking,skeptic?)	231	136	true
turtle that discovers not p (id,ranking,skeptic?)	4	3	false
