model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	66
total timesteps	21
turtle that knows p	29
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	17	1	false
turtle that discovers not p (id,ranking,skeptic?)	15	1	false
