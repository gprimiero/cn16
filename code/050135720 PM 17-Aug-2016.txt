model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	total
discovery_type	random

model output
total costs	96
total timesteps	18
turtle that knows p	5
turtle that knows not p	46
turtle that discovers p (id,ranking,skeptic?)	38	1	true
turtle that discovers not p (id,ranking,skeptic?)	50	1	true
