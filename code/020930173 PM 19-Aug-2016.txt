model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	555
total timesteps	56
turtle that knows p	53
turtle that knows not p	248
turtle that discovers p (id,ranking,skeptic?)	134	1	true
turtle that discovers not p (id,ranking,skeptic?)	188	0.5	true
