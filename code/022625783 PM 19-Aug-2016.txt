model parameters
nodes	100
skeptics	7
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	90
total timesteps	34
turtle that knows p	34
turtle that knows not p	67
turtle that discovers p (id,ranking,skeptic?)	75	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	20	1	false
