model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1527
total timesteps	101
turtle that knows p	181
turtle that knows not p	120
turtle that discovers p (id,ranking,skeptic?)	151	1	true
turtle that discovers not p (id,ranking,skeptic?)	235	1	true
