model parameters
nodes	200
skeptics	27
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1115
total timesteps	93
turtle that knows p	137
turtle that knows not p	64
turtle that discovers p (id,ranking,skeptic?)	127	1	false
turtle that discovers not p (id,ranking,skeptic?)	139	1	false
