model parameters
nodes	100
skeptics	54
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	857
total timesteps	116
turtle that knows p	60
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	11	0.14285714285714285	false
turtle that discovers not p (id,ranking,skeptic?)	50	0.3333333333333333	true
