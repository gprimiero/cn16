model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	277
total timesteps	50
turtle that knows p	120
turtle that knows not p	181
turtle that discovers p (id,ranking,skeptic?)	56	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	179	1	false
