model parameters
nodes	50
skeptics	5
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	41
total timesteps	14
turtle that knows p	4
turtle that knows not p	47
turtle that discovers p (id,ranking,skeptic?)	33	1	false
turtle that discovers not p (id,ranking,skeptic?)	0	0.3333333333333333	false
