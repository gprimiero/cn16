model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	137
total timesteps	26
turtle that knows p	35
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	7	1	true
turtle that discovers not p (id,ranking,skeptic?)	32	0.5	true
