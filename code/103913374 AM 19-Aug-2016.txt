model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	366
total timesteps	77
turtle that knows p	78
turtle that knows not p	223
turtle that discovers p (id,ranking,skeptic?)	161	1	false
turtle that discovers not p (id,ranking,skeptic?)	227	0.5	false
