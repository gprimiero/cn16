model parameters
nodes	300
skeptics	153
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	704
total timesteps	75
turtle that knows p	240
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	274	1	false
turtle that discovers not p (id,ranking,skeptic?)	150	1	true
