model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	382
total timesteps	69
turtle that knows p	49
turtle that knows not p	52
turtle that discovers p (id,ranking,skeptic?)	72	1	true
turtle that discovers not p (id,ranking,skeptic?)	6	0.25	false
model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	159
total timesteps	22
turtle that knows p	76
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	69	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	54	1	true
