model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	381
total timesteps	60
turtle that knows p	169
turtle that knows not p	32
turtle that discovers p (id,ranking,skeptic?)	41	1	false
turtle that discovers not p (id,ranking,skeptic?)	122	1	true
