model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	337
total timesteps	32
turtle that knows p	98
turtle that knows not p	103
turtle that discovers p (id,ranking,skeptic?)	12	0.1	true
turtle that discovers not p (id,ranking,skeptic?)	167	1	true
