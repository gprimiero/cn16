model parameters
nodes	300
skeptics	30
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	425
total timesteps	41
turtle that knows p	269
turtle that knows not p	32
turtle that discovers p (id,ranking,skeptic?)	172	1	false
turtle that discovers not p (id,ranking,skeptic?)	144	1	false
