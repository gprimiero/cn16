model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	613
total timesteps	74
turtle that knows p	144
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	297	1	true
turtle that discovers not p (id,ranking,skeptic?)	13	0.5	true
