model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	85
total timesteps	21
turtle that knows p	40
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	40	1	false
turtle that discovers not p (id,ranking,skeptic?)	35	0.5	true
