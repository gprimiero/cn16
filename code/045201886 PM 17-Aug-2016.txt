model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	74
total timesteps	19
turtle that knows p	39
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	46	1	false
turtle that discovers not p (id,ranking,skeptic?)	44	1	false
