model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	601
total timesteps	61
turtle that knows p	19
turtle that knows not p	282
turtle that discovers p (id,ranking,skeptic?)	297	1	false
turtle that discovers not p (id,ranking,skeptic?)	76	1	false
