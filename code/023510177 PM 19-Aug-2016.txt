model parameters
nodes	100
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	796
total timesteps	119
turtle that knows p	175
turtle that knows not p	126
turtle that discovers p (id,ranking,skeptic?)	250	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	67	1	false
