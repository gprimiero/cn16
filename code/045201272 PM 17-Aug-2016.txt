model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	149
total timesteps	19
turtle that knows p	8
turtle that knows not p	43
turtle that discovers p (id,ranking,skeptic?)	42	1	false
turtle that discovers not p (id,ranking,skeptic?)	9	1	false
