model parameters
nodes	50
skeptics	5
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	49
total timesteps	19
turtle that knows p	33
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	11	1	false
turtle that discovers not p (id,ranking,skeptic?)	23	0.3333333333333333	false
