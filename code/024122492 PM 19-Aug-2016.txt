model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	707
total timesteps	61
turtle that knows p	161
turtle that knows not p	40
turtle that discovers p (id,ranking,skeptic?)	74	1	true
turtle that discovers not p (id,ranking,skeptic?)	34	1	true
