model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	1594
total timesteps	93
turtle that knows p	101
turtle that knows not p	200
turtle that discovers p (id,ranking,skeptic?)	186	1	false
turtle that discovers not p (id,ranking,skeptic?)	12	0.16666666666666666	false
