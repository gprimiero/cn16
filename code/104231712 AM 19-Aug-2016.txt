model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1018
total timesteps	89
turtle that knows p	24
turtle that knows not p	277
turtle that discovers p (id,ranking,skeptic?)	246	1	true
turtle that discovers not p (id,ranking,skeptic?)	233	1	true
