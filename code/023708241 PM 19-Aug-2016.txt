model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	87
total timesteps	13
turtle that knows p	10
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	26	1	true
turtle that discovers not p (id,ranking,skeptic?)	5	1	true
