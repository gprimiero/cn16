model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1173
total timesteps	117
turtle that knows p	155
turtle that knows not p	146
turtle that discovers p (id,ranking,skeptic?)	169	1	true
turtle that discovers not p (id,ranking,skeptic?)	250	1	false
model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1725
total timesteps	139
turtle that knows p	102
turtle that knows not p	199
turtle that discovers p (id,ranking,skeptic?)	52	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	158	0.5	true
