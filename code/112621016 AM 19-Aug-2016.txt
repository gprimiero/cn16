model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	690
total timesteps	48
turtle that knows p	268
turtle that knows not p	33
turtle that discovers p (id,ranking,skeptic?)	154	1	false
turtle that discovers not p (id,ranking,skeptic?)	260	1	true
