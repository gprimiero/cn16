model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	566
total timesteps	63
turtle that knows p	118
turtle that knows not p	183
turtle that discovers p (id,ranking,skeptic?)	280	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	174	1	true
