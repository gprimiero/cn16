model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	815
total timesteps	96
turtle that knows p	110
turtle that knows not p	191
turtle that discovers p (id,ranking,skeptic?)	254	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	78	1	true
