model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	181
total timesteps	28
turtle that knows p	11
turtle that knows not p	20
turtle that discovers p (id,ranking,skeptic?)	1	0.09090909090909091	true
turtle that discovers not p (id,ranking,skeptic?)	11	0.3333333333333333	true
