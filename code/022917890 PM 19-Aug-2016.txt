model parameters
nodes	40
skeptics	2
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	384
total timesteps	66
turtle that knows p	20
turtle that knows not p	21
turtle that discovers p (id,ranking,skeptic?)	30	1	false
turtle that discovers not p (id,ranking,skeptic?)	16	1	false
