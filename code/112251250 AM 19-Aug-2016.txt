model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	655
total timesteps	86
turtle that knows p	69
turtle that knows not p	232
turtle that discovers p (id,ranking,skeptic?)	157	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	76	0.5	false
