model parameters
nodes	300
skeptics	159
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	833
total timesteps	9
turtle that knows p	190
turtle that knows not p	110
turtle that discovers p (id,ranking,skeptic?)	138	0	false
turtle that discovers not p (id,ranking,skeptic?)	249	0	true
