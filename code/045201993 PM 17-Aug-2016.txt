model parameters
nodes	300
skeptics	152
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	1456
total timesteps	262
turtle that knows p	250
turtle that knows not p	50
turtle that discovers p (id,ranking,skeptic?)	144	262	false
turtle that discovers not p (id,ranking,skeptic?)	178	285	false
