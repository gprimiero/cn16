model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2057
total timesteps	209
turtle that knows p	173
turtle that knows not p	128
turtle that discovers p (id,ranking,skeptic?)	81	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	249	1	true
