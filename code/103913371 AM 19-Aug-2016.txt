model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	556
total timesteps	81
turtle that knows p	257
turtle that knows not p	44
turtle that discovers p (id,ranking,skeptic?)	170	1	false
turtle that discovers not p (id,ranking,skeptic?)	212	1	true
