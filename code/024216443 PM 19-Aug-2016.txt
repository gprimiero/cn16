model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	372
total timesteps	69
turtle that knows p	78
turtle that knows not p	23
turtle that discovers p (id,ranking,skeptic?)	65	1	true
turtle that discovers not p (id,ranking,skeptic?)	63	1	true
