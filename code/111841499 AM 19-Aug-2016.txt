model parameters
nodes	300
skeptics	162
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	506
total timesteps	59
turtle that knows p	35
turtle that knows not p	266
turtle that discovers p (id,ranking,skeptic?)	219	1	true
turtle that discovers not p (id,ranking,skeptic?)	285	1	true
