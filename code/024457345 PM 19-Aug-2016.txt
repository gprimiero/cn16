model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	120
total timesteps	23
turtle that knows p	15
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	14	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	21	1	true
