model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	55
total timesteps	23
turtle that knows p	51
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	8	0.1111111111111111	true
turtle that discovers not p (id,ranking,skeptic?)	46	1	false
