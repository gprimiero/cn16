model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	344
total timesteps	65
turtle that knows p	50
turtle that knows not p	251
turtle that discovers p (id,ranking,skeptic?)	6	0.125	false
turtle that discovers not p (id,ranking,skeptic?)	265	1	false
