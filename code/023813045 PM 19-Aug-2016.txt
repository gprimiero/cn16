model parameters
nodes	30
skeptics	15
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	64
total timesteps	18
turtle that knows p	19
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	16	1	true
turtle that discovers not p (id,ranking,skeptic?)	9	0.2	true
