model parameters
nodes	300
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	494
total timesteps	77
turtle that knows p	27
turtle that knows not p	274
turtle that discovers p (id,ranking,skeptic?)	182	1	false
turtle that discovers not p (id,ranking,skeptic?)	69	1	false
