model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	191
total timesteps	51
turtle that knows p	301
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	121	1	false
turtle that discovers not p (id,ranking,skeptic?)	274	1	false
