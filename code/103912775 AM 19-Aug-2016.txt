model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	457
total timesteps	83
turtle that knows p	98
turtle that knows not p	203
turtle that discovers p (id,ranking,skeptic?)	50	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	33	0.3333333333333333	true
