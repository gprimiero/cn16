model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	50
total timesteps	12
turtle that knows p	24
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	30	1	true
turtle that discovers not p (id,ranking,skeptic?)	26	0.5	true
