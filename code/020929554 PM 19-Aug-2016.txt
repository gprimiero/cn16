model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	525
total timesteps	100
turtle that knows p	166
turtle that knows not p	135
turtle that discovers p (id,ranking,skeptic?)	273	1	false
turtle that discovers not p (id,ranking,skeptic?)	297	1	true
