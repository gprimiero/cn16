model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	168
total timesteps	31
turtle that knows p	68
turtle that knows not p	133
turtle that discovers p (id,ranking,skeptic?)	180	1	true
turtle that discovers not p (id,ranking,skeptic?)	92	0.5	true
