model parameters
nodes	40
skeptics	38
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	84
total timesteps	16
turtle that knows p	19
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	31	1	true
turtle that discovers not p (id,ranking,skeptic?)	33	1	true
model parameters
nodes	40
skeptics	36
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	122
total timesteps	23
turtle that knows p	5
turtle that knows not p	36
turtle that discovers p (id,ranking,skeptic?)	3	1	true
turtle that discovers not p (id,ranking,skeptic?)	24	1	true
