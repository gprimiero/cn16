model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	127
total timesteps	30
turtle that knows p	15
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	18	1	true
turtle that discovers not p (id,ranking,skeptic?)	25	1	false
