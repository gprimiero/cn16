model parameters
nodes	30
skeptics	0
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	101
total timesteps	24
turtle that knows p	6
turtle that knows not p	25
turtle that discovers p (id,ranking,skeptic?)	22	1	false
turtle that discovers not p (id,ranking,skeptic?)	29	1	false
