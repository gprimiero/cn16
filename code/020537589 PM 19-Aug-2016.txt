model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	485
total timesteps	78
turtle that knows p	244
turtle that knows not p	57
turtle that discovers p (id,ranking,skeptic?)	34	1	false
turtle that discovers not p (id,ranking,skeptic?)	284	0.5	true
