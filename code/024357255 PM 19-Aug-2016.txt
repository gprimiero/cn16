model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	87
total timesteps	17
turtle that knows p	0
turtle that knows not p	41
turtle that discovers p (id,ranking,skeptic?)	9	1	true
turtle that discovers not p (id,ranking,skeptic?)	7	1	true
