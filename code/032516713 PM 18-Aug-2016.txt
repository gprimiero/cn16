model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1346
total timesteps	10
turtle that knows p	143
turtle that knows not p	157
turtle that discovers p (id,ranking,skeptic?)	261	0	false
turtle that discovers not p (id,ranking,skeptic?)	106	0	true
