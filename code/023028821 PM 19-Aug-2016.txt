model parameters
nodes	30
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	697
total timesteps	111
turtle that knows p	218
turtle that knows not p	83
turtle that discovers p (id,ranking,skeptic?)	296	1	false
turtle that discovers not p (id,ranking,skeptic?)	116	1	false
