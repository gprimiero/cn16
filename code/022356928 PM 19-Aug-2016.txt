model parameters
nodes	300
skeptics	42
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	76
total timesteps	37
turtle that knows p	165
turtle that knows not p	136
turtle that discovers p (id,ranking,skeptic?)	143	0.3333333333333333	false
turtle that discovers not p (id,ranking,skeptic?)	96	1	false
