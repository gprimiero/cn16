model parameters
nodes	100
skeptics	90
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	853
total timesteps	119
turtle that knows p	25
turtle that knows not p	76
turtle that discovers p (id,ranking,skeptic?)	47	1	false
turtle that discovers not p (id,ranking,skeptic?)	42	0.5	true
model parameters
nodes	100
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	257
total timesteps	55
turtle that knows p	64
turtle that knows not p	37
turtle that discovers p (id,ranking,skeptic?)	63	1	true
turtle that discovers not p (id,ranking,skeptic?)	46	1	true
