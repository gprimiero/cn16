model parameters
nodes	300
skeptics	160
% of confirmation for skeptics' control	95
network type	total
discovery_type	lazyP-skepticNotP

model output
total costs	1357
total timesteps	10
turtle that knows p	134
turtle that knows not p	166
turtle that discovers p (id,ranking,skeptic?)	269	0	false
turtle that discovers not p (id,ranking,skeptic?)	104	0	true
