model parameters
nodes	300
skeptics	137
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	726
total timesteps	102
turtle that knows p	195
turtle that knows not p	106
turtle that discovers p (id,ranking,skeptic?)	20	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	270	1	false
