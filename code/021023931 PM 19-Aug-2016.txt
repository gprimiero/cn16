model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	553
total timesteps	56
turtle that knows p	277
turtle that knows not p	24
turtle that discovers p (id,ranking,skeptic?)	232	1	true
turtle that discovers not p (id,ranking,skeptic?)	120	0.5	true
