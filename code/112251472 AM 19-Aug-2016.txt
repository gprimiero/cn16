model parameters
nodes	300
skeptics	151
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	292
total timesteps	44
turtle that knows p	283
turtle that knows not p	18
turtle that discovers p (id,ranking,skeptic?)	56	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	107	0.5	false
