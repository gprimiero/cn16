model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	569
total timesteps	67
turtle that knows p	25
turtle that knows not p	276
turtle that discovers p (id,ranking,skeptic?)	134	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	214	1	true
