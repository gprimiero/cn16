model parameters
nodes	300
skeptics	92
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	524
total timesteps	59
turtle that knows p	166
turtle that knows not p	135
turtle that discovers p (id,ranking,skeptic?)	198	1	false
turtle that discovers not p (id,ranking,skeptic?)	59	0.3333333333333333	true
