model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	220
total timesteps	22
turtle that knows p	40
turtle that knows not p	11
turtle that discovers p (id,ranking,skeptic?)	46	1	true
turtle that discovers not p (id,ranking,skeptic?)	28	1	true
