model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	365
total timesteps	77
turtle that knows p	72
turtle that knows not p	229
turtle that discovers p (id,ranking,skeptic?)	94	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	135	0.5	false
