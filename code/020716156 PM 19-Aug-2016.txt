model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	365
total timesteps	48
turtle that knows p	74
turtle that knows not p	227
turtle that discovers p (id,ranking,skeptic?)	16	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	24	0.5	true
