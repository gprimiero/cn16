model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	817
total timesteps	70
turtle that knows p	236
turtle that knows not p	65
turtle that discovers p (id,ranking,skeptic?)	182	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	266	1	true
