model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	52
total timesteps	17
turtle that knows p	17
turtle that knows not p	14
turtle that discovers p (id,ranking,skeptic?)	26	1	true
turtle that discovers not p (id,ranking,skeptic?)	12	1	false
