model parameters
nodes	40
skeptics	6
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	41
total timesteps	16
turtle that knows p	10
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	13	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	8	0.3333333333333333	false
