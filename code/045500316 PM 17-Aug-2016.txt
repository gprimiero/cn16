model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	61
total timesteps	17
turtle that knows p	29
turtle that knows not p	22
turtle that discovers p (id,ranking,skeptic?)	33	1	false
turtle that discovers not p (id,ranking,skeptic?)	28	1	false
