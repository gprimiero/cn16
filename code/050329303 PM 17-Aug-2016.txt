model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	108
total timesteps	21
turtle that knows p	39
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	47	1	true
turtle that discovers not p (id,ranking,skeptic?)	30	1	true
