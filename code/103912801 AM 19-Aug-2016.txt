model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	785
total timesteps	86
turtle that knows p	245
turtle that knows not p	56
turtle that discovers p (id,ranking,skeptic?)	29	0.16666666666666666	false
turtle that discovers not p (id,ranking,skeptic?)	4	0.5	false
