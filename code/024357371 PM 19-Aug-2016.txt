model parameters
nodes	40
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	152
total timesteps	32
turtle that knows p	24
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	8	1	true
turtle that discovers not p (id,ranking,skeptic?)	0	0.07692307692307693	true
