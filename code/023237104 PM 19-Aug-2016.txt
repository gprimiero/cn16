model parameters
nodes	300
skeptics	155
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	747
total timesteps	94
turtle that knows p	57
turtle that knows not p	244
turtle that discovers p (id,ranking,skeptic?)	118	1	false
turtle that discovers not p (id,ranking,skeptic?)	190	0.3333333333333333	true
