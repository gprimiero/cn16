model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	381
total timesteps	51
turtle that knows p	126
turtle that knows not p	175
turtle that discovers p (id,ranking,skeptic?)	275	1	false
turtle that discovers not p (id,ranking,skeptic?)	219	1	true
