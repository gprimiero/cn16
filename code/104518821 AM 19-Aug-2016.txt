model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	399
total timesteps	52
turtle that knows p	217
turtle that knows not p	84
turtle that discovers p (id,ranking,skeptic?)	72	1	true
turtle that discovers not p (id,ranking,skeptic?)	41	0.5	false
