model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	306
total timesteps	56
turtle that knows p	191
turtle that knows not p	110
turtle that discovers p (id,ranking,skeptic?)	206	1	false
turtle that discovers not p (id,ranking,skeptic?)	96	1	false
