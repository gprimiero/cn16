model parameters
nodes	50
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	74
total timesteps	22
turtle that knows p	25
turtle that knows not p	26
turtle that discovers p (id,ranking,skeptic?)	49	1	false
turtle that discovers not p (id,ranking,skeptic?)	8	0.25	true
