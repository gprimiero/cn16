model parameters
nodes	300
skeptics	268
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	502
total timesteps	59
turtle that knows p	164
turtle that knows not p	137
turtle that discovers p (id,ranking,skeptic?)	63	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	51	1	true
