model parameters
nodes	100
skeptics	89
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	287
total timesteps	43
turtle that knows p	89
turtle that knows not p	12
turtle that discovers p (id,ranking,skeptic?)	41	1	true
turtle that discovers not p (id,ranking,skeptic?)	46	1	true
