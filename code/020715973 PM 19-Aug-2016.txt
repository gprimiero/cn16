model parameters
nodes	300
skeptics	85
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	588
total timesteps	53
turtle that knows p	204
turtle that knows not p	97
turtle that discovers p (id,ranking,skeptic?)	291	1	false
turtle that discovers not p (id,ranking,skeptic?)	82	0.25	true
