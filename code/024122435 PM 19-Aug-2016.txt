model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	614
total timesteps	67
turtle that knows p	135
turtle that knows not p	66
turtle that discovers p (id,ranking,skeptic?)	171	1	false
turtle that discovers not p (id,ranking,skeptic?)	123	1	true
