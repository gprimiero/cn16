model parameters
nodes	300
skeptics	240
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1199
total timesteps	130
turtle that knows p	147
turtle that knows not p	154
turtle that discovers p (id,ranking,skeptic?)	86	1	true
turtle that discovers not p (id,ranking,skeptic?)	20	0.3333333333333333	true
