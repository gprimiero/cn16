model parameters
nodes	40
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	59
total timesteps	16
turtle that knows p	24
turtle that knows not p	17
turtle that discovers p (id,ranking,skeptic?)	24	1	true
turtle that discovers not p (id,ranking,skeptic?)	6	0.25	true
