model parameters
nodes	300
skeptics	154
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	476
total timesteps	70
turtle that knows p	26
turtle that knows not p	275
turtle that discovers p (id,ranking,skeptic?)	195	1	true
turtle that discovers not p (id,ranking,skeptic?)	81	1	true
model parameters
nodes	300
skeptics	147
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	376
total timesteps	51
turtle that knows p	28
turtle that knows not p	273
turtle that discovers p (id,ranking,skeptic?)	217	1	true
turtle that discovers not p (id,ranking,skeptic?)	157	1	true
