model parameters
nodes	30
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	111
total timesteps	21
turtle that knows p	26
turtle that knows not p	5
turtle that discovers p (id,ranking,skeptic?)	14	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	18	0.5	true
