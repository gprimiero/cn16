model parameters
nodes	300
skeptics	39
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	2087
total timesteps	98
turtle that knows p	149
turtle that knows not p	152
turtle that discovers p (id,ranking,skeptic?)	251	1	false
turtle that discovers not p (id,ranking,skeptic?)	226	1	false
