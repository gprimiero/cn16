model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	339
total timesteps	51
turtle that knows p	20
turtle that knows not p	31
turtle that discovers p (id,ranking,skeptic?)	44	1	false
turtle that discovers not p (id,ranking,skeptic?)	33	1	false
