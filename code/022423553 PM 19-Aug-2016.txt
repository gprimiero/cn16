model parameters
nodes	300
skeptics	31
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	572
total timesteps	64
turtle that knows p	157
turtle that knows not p	144
turtle that discovers p (id,ranking,skeptic?)	219	1	false
turtle that discovers not p (id,ranking,skeptic?)	253	1	false
