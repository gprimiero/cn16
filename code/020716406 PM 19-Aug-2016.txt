model parameters
nodes	300
skeptics	102
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	230
total timesteps	33
turtle that knows p	69
turtle that knows not p	232
turtle that discovers p (id,ranking,skeptic?)	117	1	false
turtle that discovers not p (id,ranking,skeptic?)	278	1	true
