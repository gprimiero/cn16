model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	194
total timesteps	26
turtle that knows p	51
turtle that knows not p	0
turtle that discovers p (id,ranking,skeptic?)	19	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	22	1	true
