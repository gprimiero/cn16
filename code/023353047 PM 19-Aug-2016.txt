model parameters
nodes	200
skeptics	104
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1501
total timesteps	152
turtle that knows p	93
turtle that knows not p	108
turtle that discovers p (id,ranking,skeptic?)	72	1	true
turtle that discovers not p (id,ranking,skeptic?)	93	0.3333333333333333	false
