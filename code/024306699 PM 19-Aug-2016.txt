model parameters
nodes	50
skeptics	46
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	88
total timesteps	15
turtle that knows p	16
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	20	1	false
turtle that discovers not p (id,ranking,skeptic?)	11	1	true
