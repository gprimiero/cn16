model parameters
nodes	200
skeptics	110
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	165
total timesteps	44
turtle that knows p	0
turtle that knows not p	201
turtle that discovers p (id,ranking,skeptic?)	128	1	true
turtle that discovers not p (id,ranking,skeptic?)	183	1	false
