model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	534
total timesteps	100
turtle that knows p	209
turtle that knows not p	92
turtle that discovers p (id,ranking,skeptic?)	166	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	294	1	false
