model parameters
nodes	300
skeptics	149
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	943
total timesteps	99
turtle that knows p	185
turtle that knows not p	116
turtle that discovers p (id,ranking,skeptic?)	250	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	93	1	true
