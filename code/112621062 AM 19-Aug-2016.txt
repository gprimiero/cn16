model parameters
nodes	300
skeptics	145
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-skepticNotP

model output
total costs	570
total timesteps	70
turtle that knows p	240
turtle that knows not p	61
turtle that discovers p (id,ranking,skeptic?)	129	1	false
turtle that discovers not p (id,ranking,skeptic?)	272	1	true
