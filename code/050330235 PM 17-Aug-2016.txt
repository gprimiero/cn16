model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	random
discovery_type	random

model output
total costs	43
total timesteps	15
turtle that knows p	37
turtle that knows not p	14
turtle that discovers p (id,ranking,skeptic?)	9	1	false
turtle that discovers not p (id,ranking,skeptic?)	7	1	true
