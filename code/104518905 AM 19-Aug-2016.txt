model parameters
nodes	300
skeptics	238
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	385
total timesteps	54
turtle that knows p	205
turtle that knows not p	96
turtle that discovers p (id,ranking,skeptic?)	268	1	true
turtle that discovers not p (id,ranking,skeptic?)	79	1	false
