model parameters
nodes	300
skeptics	34
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	949
total timesteps	139
turtle that knows p	184
turtle that knows not p	117
turtle that discovers p (id,ranking,skeptic?)	24	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	222	1	true
