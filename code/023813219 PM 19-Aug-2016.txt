model parameters
nodes	30
skeptics	13
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	168
total timesteps	39
turtle that knows p	22
turtle that knows not p	9
turtle that discovers p (id,ranking,skeptic?)	27	1	false
turtle that discovers not p (id,ranking,skeptic?)	2	0.2	false
