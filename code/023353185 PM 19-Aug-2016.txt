model parameters
nodes	200
skeptics	99
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	247
total timesteps	40
turtle that knows p	166
turtle that knows not p	35
turtle that discovers p (id,ranking,skeptic?)	100	1	false
turtle that discovers not p (id,ranking,skeptic?)	188	1	false
