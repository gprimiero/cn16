model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	379
total timesteps	39
turtle that knows p	29
turtle that knows not p	272
turtle that discovers p (id,ranking,skeptic?)	206	1	true
turtle that discovers not p (id,ranking,skeptic?)	96	1	true
