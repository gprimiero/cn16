model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1037
total timesteps	95
turtle that knows p	207
turtle that knows not p	94
turtle that discovers p (id,ranking,skeptic?)	59	0.5	false
turtle that discovers not p (id,ranking,skeptic?)	262	1	true
