model parameters
nodes	200
skeptics	180
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	624
total timesteps	63
turtle that knows p	39
turtle that knows not p	162
turtle that discovers p (id,ranking,skeptic?)	89	0.25	true
turtle that discovers not p (id,ranking,skeptic?)	147	0.5	true
