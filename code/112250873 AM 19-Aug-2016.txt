model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	lazyP-lazyNotP

model output
total costs	819
total timesteps	102
turtle that knows p	149
turtle that knows not p	152
turtle that discovers p (id,ranking,skeptic?)	186	1	false
turtle that discovers not p (id,ranking,skeptic?)	157	1	false
