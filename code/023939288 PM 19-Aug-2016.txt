model parameters
nodes	300
skeptics	267
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	683
total timesteps	88
turtle that knows p	139
turtle that knows not p	162
turtle that discovers p (id,ranking,skeptic?)	24	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	211	1	true
