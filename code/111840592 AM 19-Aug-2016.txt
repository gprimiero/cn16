model parameters
nodes	300
skeptics	143
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	skepticP-skepticNotP

model output
total costs	320
total timesteps	74
turtle that knows p	181
turtle that knows not p	120
turtle that discovers p (id,ranking,skeptic?)	189	1	true
turtle that discovers not p (id,ranking,skeptic?)	199	1	true
