model parameters
nodes	300
skeptics	25
% of confirmation for skeptics' control	95
network type	linear
discovery_type	random

model output
total costs	88
total timesteps	21
turtle that knows p	38
turtle that knows not p	13
turtle that discovers p (id,ranking,skeptic?)	17	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	34	0.5	true
