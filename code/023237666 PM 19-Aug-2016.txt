model parameters
nodes	300
skeptics	166
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	369
total timesteps	61
turtle that knows p	162
turtle that knows not p	139
turtle that discovers p (id,ranking,skeptic?)	26	0.25	false
turtle that discovers not p (id,ranking,skeptic?)	233	1	true
