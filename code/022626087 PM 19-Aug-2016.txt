model parameters
nodes	100
skeptics	9
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	193
total timesteps	61
turtle that knows p	72
turtle that knows not p	29
turtle that discovers p (id,ranking,skeptic?)	76	1	false
turtle that discovers not p (id,ranking,skeptic?)	37	1	false
