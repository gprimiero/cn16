model parameters
nodes	300
skeptics	301
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	393
total timesteps	39
turtle that knows p	84
turtle that knows not p	217
turtle that discovers p (id,ranking,skeptic?)	124	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	177	0.5	true
