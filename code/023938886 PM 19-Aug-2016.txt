model parameters
nodes	300
skeptics	279
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1737
total timesteps	105
turtle that knows p	215
turtle that knows not p	86
turtle that discovers p (id,ranking,skeptic?)	207	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	30	0.3333333333333333	true
