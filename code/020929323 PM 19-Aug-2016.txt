model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	379
total timesteps	82
turtle that knows p	217
turtle that knows not p	84
turtle that discovers p (id,ranking,skeptic?)	116	1	false
turtle that discovers not p (id,ranking,skeptic?)	94	0.3333333333333333	true
