model parameters
nodes	300
skeptics	32
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	393
total timesteps	48
turtle that knows p	242
turtle that knows not p	59
turtle that discovers p (id,ranking,skeptic?)	146	1	false
turtle that discovers not p (id,ranking,skeptic?)	186	0.3333333333333333	false
