model parameters
nodes	50
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	142
total timesteps	28
turtle that knows p	23
turtle that knows not p	28
turtle that discovers p (id,ranking,skeptic?)	49	1	true
turtle that discovers not p (id,ranking,skeptic?)	1	0.08333333333333333	true
