model parameters
nodes	300
skeptics	26
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	1520
total timesteps	144
turtle that knows p	285
turtle that knows not p	16
turtle that discovers p (id,ranking,skeptic?)	9	0.2	false
turtle that discovers not p (id,ranking,skeptic?)	36	0.5	false
