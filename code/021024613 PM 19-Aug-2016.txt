model parameters
nodes	300
skeptics	161
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	529
total timesteps	69
turtle that knows p	226
turtle that knows not p	75
turtle that discovers p (id,ranking,skeptic?)	36	0.5	true
turtle that discovers not p (id,ranking,skeptic?)	90	0.5	false
