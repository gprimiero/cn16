model parameters
nodes	300
skeptics	91
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	272
total timesteps	53
turtle that knows p	235
turtle that knows not p	66
turtle that discovers p (id,ranking,skeptic?)	46	0.3333333333333333	true
turtle that discovers not p (id,ranking,skeptic?)	227	0.5	false
