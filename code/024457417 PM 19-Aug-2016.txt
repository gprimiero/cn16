model parameters
nodes	30
skeptics	29
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	84
total timesteps	20
turtle that knows p	17
turtle that knows not p	14
turtle that discovers p (id,ranking,skeptic?)	23	1	true
turtle that discovers not p (id,ranking,skeptic?)	3	0.5	true
