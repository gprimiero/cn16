model parameters
nodes	300
skeptics	241
% of confirmation for skeptics' control	95
network type	small-world
discovery_type	random

model output
total costs	316
total timesteps	40
turtle that knows p	130
turtle that knows not p	171
turtle that discovers p (id,ranking,skeptic?)	74	1	true
turtle that discovers not p (id,ranking,skeptic?)	300	1	true
